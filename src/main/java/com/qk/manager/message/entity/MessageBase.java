package com.qk.manager.message.entity;

/**
 * 
 * <p>Description: 消息库类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年8月13日   
 * @version 1.0
 */
public class MessageBase {
    /**
     *	id
     */
    private Integer id;

    /**
     *	消息
     */
    private String message;
    
    /**
     * 	管理员ID
     */
    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message == null ? null : message.trim();
    }

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}