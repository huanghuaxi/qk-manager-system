package com.qk.manager.message.controller;

import java.util.Arrays;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.qk.manager.business.controller.PageUtil;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.ExcelUtil;
import com.qk.manager.common.ResultMessage;
import com.qk.manager.message.entity.MessageBase;
import com.qk.manager.message.service.MessageBaseService;

/**
 * 
 * <p>Description: 消息库控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年8月13日   
 * @version 1.0
 */

@Controller
@RequestMapping("/messageBase")
public class MessageBaseController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MessageBaseService msgBaseService;
	
	/**
	 * 
	 * Description: 跳转到消息库列表页面 
	 * @return 
	 * date 2019年8月13日
	 */
	@RequestMapping("/toList")
	public String toList() {
		logger.info("跳转到消息库列表页面");
		return "messageBase/list.html";
	}
	
	/**
	 * 
	 * Description: 获取消息库列表 
	 * @param pageUtil
	 * @return 
	 * date 2019年8月13日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil<MessageBase> getList(PageUtil<MessageBase> pageUtil){
		try {
			List<MessageBase> list = msgBaseService.selectByPage(pageUtil.getCurrIndex(), pageUtil.getLimit());
			int count = msgBaseService.countMessage();
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setData(list);
			pageUtil.setMsg("成功");
			logger.info("获取消息库列表");
		}catch (Exception e) {
			logger.error("获取消息库列表失败", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 新增消息 
	 * @param msgBase
	 * @return 
	 * date 2019年8月13日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage save(MessageBase msgBase) {
		try {
			msgBaseService.saveMessage(msgBase);
			logger.info("新增消息");
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("新增消息失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 编辑消息 
	 * @param msgBase
	 * @return 
	 * date 2019年8月13日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(MessageBase msgBase) {
		try {
			msgBaseService.editMessage(msgBase);
			logger.info("编辑消息");
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("编辑消息失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 删除消息 
	 * @param ids
	 * @return 
	 * date 2019年8月13日
	 */
	@ResponseBody
	@RequestMapping("/del")
	public ResultMessage del(String ids) {
		try {
			Assert.hasText(ids, "消息库ID数据不存在");
			msgBaseService.delMessage(Arrays.asList(ids.trim().split(",")));;
			logger.info("删除消息");
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("删除消息失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 消息批量导入 
	 * @param file
	 * @return 
	 * date 2019年8月13日
	 */
	@ResponseBody
	@RequestMapping("/impExcel")
	public ResultMessage impExcel(@RequestParam("file") MultipartFile file) {
		try {
			//读取Excel数据内容
			Workbook workbook = ExcelUtil.getWorkBook(file);
			String msg = msgBaseService.impExcel(workbook);
			logger.info("消息批量导入 ");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("活动会员批量导入失败", e);
			return ResultMessage.getFail();
		}
	}
	
}
