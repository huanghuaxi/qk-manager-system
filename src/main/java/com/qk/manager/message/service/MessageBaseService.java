package com.qk.manager.message.service;

import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.qk.manager.message.entity.MessageBase;

/**
 * 
 * <p>Description: 加载消息库类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年8月13日   
 * @version 1.0
 */
public interface MessageBaseService {

	/**
	 * 
	 * Description: 分页查询 
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年8月13日
	 */
	public List<MessageBase> selectByPage(Integer start, Integer pageSize);
	
	/**
	 * 
	 * Description: 统计数量 
	 * @return 
	 * date 2019年8月13日
	 */
	public Integer countMessage();
	
	/**
	 * 
	 * Description: 新增消息 
	 * @param msgBase 
	 * date 2019年8月13日
	 */
	public void saveMessage(MessageBase msgBase);
	
	/**
	 * 
	 * Description: 编辑消息 
	 * @param msgBase 
	 * date 2019年8月13日
	 */
	public void editMessage(MessageBase msgBase);
	
	/**
	 * 
	 * Description: 删除消息 
	 * @param ids 
	 * date 2019年8月13日
	 */
	public void delMessage(List<String> ids);
	
	/**
	 * 
	 * Description: 批量导入 
	 * @param workbook
	 * @return 
	 * date 2019年8月13日
	 */
	public String impExcel(Workbook workbook);
}
