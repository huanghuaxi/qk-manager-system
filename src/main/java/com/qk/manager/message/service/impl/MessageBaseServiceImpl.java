package com.qk.manager.message.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.qk.manager.common.CurrentParamUtil;
import com.qk.manager.common.ExcelUtil;
import com.qk.manager.message.entity.MessageBase;
import com.qk.manager.message.mapper.MessageBaseMapper;
import com.qk.manager.message.service.MessageBaseService;
import com.qk.manager.security.entity.SysUser;
import com.qk.manager.security.service.UserService;

/**
 * 
 * <p>Description: 消息库更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年8月13日   
 * @version 1.0
 */

@Service
public class MessageBaseServiceImpl implements MessageBaseService{

	@Autowired
	private MessageBaseMapper msgBaseMapper;
	
	@Autowired
	private UserService userService;
	
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByPage</p>   
	 * <p>Description: 分页查询</p>   
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.qk.manager.message.service.MessageBaseService#selectByPage(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<MessageBase> selectByPage(Integer start, Integer pageSize) {
		List<Integer> userIds = userService.getRoleUser();
		return msgBaseMapper.selectByPage(start, pageSize, userIds);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: countMessage</p>   
	 * <p>Description: 统计数量</p>   
	 * @return   
	 * @see com.qk.manager.message.service.MessageBaseService#countMessage()
	 */
	@Override
	public Integer countMessage() {
		List<Integer> userIds = userService.getRoleUser();
		return msgBaseMapper.countMessage(userIds);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: saveMessage</p>   
	 * <p>Description: 新增消息</p>   
	 * @param msgBase   
	 * @see com.qk.manager.message.service.MessageBaseService#saveMessage(com.qk.manager.message.entity.MessageBase)
	 */
	@Override
	public void saveMessage(MessageBase msgBase) {
		Assert.hasText(msgBase.getMessage(), "消息数据不存在");
		SysUser user = CurrentParamUtil.getCurrentUser();
		msgBase.setUserId(user.getId());
		msgBaseMapper.insert(msgBase);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editMessage</p>   
	 * <p>Description: 编辑消息</p>   
	 * @param msgBase   
	 * @see com.qk.manager.message.service.MessageBaseService#editMessage(com.qk.manager.message.entity.MessageBase)
	 */
	@Override
	public void editMessage(MessageBase msgBase) {
		Assert.notNull(msgBase.getId(), "消息库ID数据不存在");
		Assert.hasText(msgBase.getMessage(), "消息数据不存在");
		MessageBase msg = msgBaseMapper.selectByPrimaryKey(msgBase.getId());
		msgBase.setUserId(msg.getUserId());
		msgBaseMapper.updateByPrimaryKey(msgBase);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: delMessage</p>   
	 * <p>Description: 删除消息</p>   
	 * @param ids   
	 * @see com.qk.manager.message.service.MessageBaseService#delMessage(java.util.List)
	 */
	@Override
	public void delMessage(List<String> ids) {
		msgBaseMapper.delByIds(ids);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: impExcel</p>   
	 * <p>Description: 批量导入</p>   
	 * @param workbook
	 * @return   
	 * @see com.qk.manager.message.service.MessageBaseService#impExcel(org.apache.poi.ss.usermodel.Workbook)
	 */
	@Override
	@Transactional
	public String impExcel(Workbook workbook) {
		if(workbook != null) {
			//获得当前sheet工作表
            Sheet sheet = workbook.getSheetAt(0);
            //获得当前sheet的开始行
            int firstRowNum  = sheet.getFirstRowNum();
            //获得当前sheet的结束行
            int lastRowNum = sheet.getLastRowNum();
            MessageBase msg = new MessageBase();
            SysUser user = CurrentParamUtil.getCurrentUser();
            //循环除了第一行的所有行
            for(int rowNum = firstRowNum+1;rowNum <= lastRowNum;rowNum++){ //为了过滤到第一行因为我的第一行是数据库的列
                //获得当前行
                Row row = sheet.getRow(rowNum);
                if(row == null){
                    continue;
                }
                //获取每列数据
                Cell cell = row.getCell(0);
                String message = ExcelUtil.getCellValue(cell).trim();
                if(message == null || "".equals(message)) {
    				//消息为空不做插入
    				continue;
    			}
                msg = new MessageBase();
    			msg.setMessage(message);
    			msg.setUserId(user.getId());
    			msgBaseMapper.insert(msg);
            }
		}
		return null;
	}
	
}
