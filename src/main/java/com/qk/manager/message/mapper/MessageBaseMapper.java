package com.qk.manager.message.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.qk.manager.message.entity.MessageBase;

/**
 * 
 * <p>Description: 加载消息库类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年8月13日   
 * @version 1.0
 */

@Mapper
public interface MessageBaseMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(MessageBase record);

    MessageBase selectByPrimaryKey(Integer id);

    List<MessageBase> selectAll();

    int updateByPrimaryKey(MessageBase record);
    
    /**
     * 
     * Description: 分页查询 
     * @param start
     * @param pageSize
     * @return 
     * date 2019年8月13日
     */
    List<MessageBase> selectByPage(Integer start, Integer pageSize, @Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 统计数量 
     * @return 
     * date 2019年8月13日
     */
    Integer countMessage(@Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 批量删除 
     * @param ids 
     * date 2019年8月13日
     */
    void delByIds(@Param("ids") List<String> ids);
    
    /**
     * 
     * Description:    
     * @param start
     * @param pageSize
     * @return 
     * date 2019年8月14日
     */
    List<String> selectByPageForString(Integer start, Integer pageSize,@Param("userIds") List<Integer> userIds);
}