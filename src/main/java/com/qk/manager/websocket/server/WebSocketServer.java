package com.qk.manager.websocket.server;

import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.DataUtil;

/**
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2019
 * </p>
 * 
 * @author huanghx
 * @date 2019年6月11日
 * @version 1.0
 */
@ServerEndpoint(value = "/websocket/{sid}")
@Component
public class WebSocketServer {

	private static final Logger log = LoggerFactory.getLogger(WebSocketServer.class);
	// 静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
	private static int onlineCount = 0;
	// concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
	private static CopyOnWriteArraySet<WebSocketServer> webSocketSet = new CopyOnWriteArraySet<WebSocketServer>();

	// 与某个客户端的连接会话，需要通过它来给客户端发送数据
	private Session session;

	// 接收sid
	private String sid = "";

	private HttpSession httpSession;

	/**
	 * 连接建立成功调用的方法
	 */
	@OnOpen
	public void onOpen(Session session, EndpointConfig config, @PathParam("sid") String sid) {
		this.session = session;
		this.httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class.getName());
		webSocketSet.add(this); // 加入set中
		addOnlineCount(); // 在线数加1
		log.info("有新窗口开始监听:" + sid + ",当前在线人数为" + getOnlineCount());
		this.sid = sid;
		try {
			sendMessage(DataUtil.setBaseData(CodeUtil.SUCCESS_CODE, "连接成功", 0, 1, null));
		} catch (IOException e) {
			log.error("websocket IO异常");
		}
	}

	/**
	 * 连接关闭调用的方法
	 */
	@OnClose
	public void onClose() {
		String sid = this.sid;
//		String[] data = sid.split("&");
//		String deviceNum = data[1].substring(0, 15);
//		String wechatAccount = data[1].substring(15);
		// 修改设备和账号状态
		DataUtil.offline(sid);
		webSocketSet.remove(this);
		log.info(""+webSocketSet.contains(this));
		// 在线数减1
		subOnlineCount();
		log.info("有一连接关闭，设备账号下线："+sid+"，当前在线人数为" + getOnlineCount());
	}

	/**
	 * 收到客户端消息后调用的方法
	 *
	 * @param message 客户端发送过来的消息
	 */
	@OnMessage
	public void onMessage(String message, Session session) {
		log.info("收到来自窗口" + sid + "的信息:" + message);
		DataUtil.getData(message);
		// 群发消息
//        for (WebSocketServer item : webSocketSet) {
//            try {
//                item.sendMessage(message);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
	}

	/**
	 * 
	 * @param session
	 * @param error
	 */
	@OnError
	public void onError(Session session, Throwable error) {
		log.error("发生错误");
		error.printStackTrace();
	}

	/**
	 * 实现服务器主动推送
	 */
	public void sendMessage(String message) throws IOException {

		this.session.getBasicRemote().sendText(message);
	}

	/**
	 * Description: 向客户端发送消息
	 * 
	 * @param message 向客户端发送的消息
	 * @param sid     每个客户端的标识
	 * @throws IOException date 2019年6月11日
	 */
	public static void sendInfo(String message, String sid) throws IOException {
		log.info("推送消息到窗口" + sid + "，推送内容:" + message);
		for (WebSocketServer item : webSocketSet) {
			try {
				log.info("服务号:"+item.sid+",要推送的服务号："+sid+",发送的smg："+message);
				// 这里可以设定只推送给这个sid的
				if (item.sid.equals(sid)) {
					log.info("查找到服务号:"+sid+",正准备发送消息");
					item.sendMessage(message);
					log.info("像服务号发送消息成功:"+sid+",正准备发送消息");
				}
			} catch (IOException e) {
			   log.error("信息错误", e);
				continue;
			}
		}
	}

	/**
	 * Description: 判断session是否存在    
	 * @param sid
	 * @return 
	 * date 2019年8月1日
	 */
	public static boolean isExist(String sid) {
		boolean isExist = false;
		for (WebSocketServer item : webSocketSet) {
			// 这里可以设定只推送给这个sid的
			if (item.sid.equals(sid)) {
				isExist = true;
				break;
			}
		}
        return isExist;
	}

	public static synchronized int getOnlineCount() {
		return onlineCount;
	}

	public static synchronized void addOnlineCount() {
		WebSocketServer.onlineCount++;
	}

	public static synchronized void subOnlineCount() {
		WebSocketServer.onlineCount--;
		log.info("内部输出"+onlineCount);
	}
}
