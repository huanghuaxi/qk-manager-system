/**
 * 
 */
/**    
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月30日   
 * @version 1.0   
 */
package com.qk.manager.taskintf;

import com.qk.manager.task.entity.Task;

/**
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月31日   
 * @version 1.0
 */
public interface DataExtend {
	
	/**
	 * Description: 任务操作方法    
	 * @param deviceNum
	 * @param account
	 * @param task 
	 * date 2019年7月31日
	 */
	public void operate(String deviceNum, String account,Task task) ;
}