package com.qk.manager.security.Scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 
 * <p>Description: 定时器类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年5月20日   
 * @version 1.0
 */
@Component
public class Scheduler {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 
	 * Description: 每天0点定时清零配套活动活动会员抽奖次数
	 * date 2019年5月20日
	 */
    @Scheduled(cron = "0 0 0 ? * *")
    public void testTasks() {
    	
    }
	
}
