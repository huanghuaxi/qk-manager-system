package com.qk.manager.security.validatecode.filter;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.filter.OncePerRequestFilter;
import org.thymeleaf.util.StringUtils;

import com.qk.manager.common.CommonProperties;
import com.qk.manager.common.Constants;
import com.qk.manager.security.exception.ValidateCodeException;
import com.qk.manager.security.validatecode.common.ImageCode;

/**
 * Description:   验证码 过滤器
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年4月2日   
 * @version 1.0
 */
@Component
public class ValidateCodeFilter  extends OncePerRequestFilter  {

    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;
    
    @Autowired
    private MessageSource messageSource;

    private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();
    
    @Autowired 
    private CommonProperties validateCodeProperties;
    
    /**
     * 权限验证异常类处理
     */
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        if (StringUtils.equalsIgnoreCase(validateCodeProperties.getUrl(), httpServletRequest.getRequestURI())
                && StringUtils.equalsIgnoreCase(httpServletRequest.getMethod(), Constants.POST)) {
            try {
                validateCode(new ServletWebRequest(httpServletRequest));
            } catch (ValidateCodeException e) {
                authenticationFailureHandler.onAuthenticationFailure(httpServletRequest, httpServletResponse, e);
                return;
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
	/**
	 * 登陆页验证出错处理
	 * @param servletWebRequest
	 * @throws ServletRequestBindingException
	 */
    private void validateCode(ServletWebRequest servletWebRequest) throws ServletRequestBindingException {
        ImageCode codeInSession = (ImageCode) sessionStrategy.getAttribute(servletWebRequest, Constants.SESSION_KEY);
        String codeInRequest = ServletRequestUtils.getStringParameter(servletWebRequest.getRequest(), Constants.IMAGE_CODE);

        Locale locale = LocaleContextHolder.getLocale();
        
        if (StringUtils.isEmpty(codeInRequest)) {
            throw new ValidateCodeException(messageSource.getMessage(Constants.AUTH_MESSAGE_VALIDATECODE_NULL,null, locale));
        }
        if (codeInSession == null) {
            throw new ValidateCodeException(messageSource.getMessage(Constants.AUTH_MESSAGE_VALIDATECODE_NOTEXIST,null, locale));
        }
        if (codeInSession.isExpire()) {
            sessionStrategy.removeAttribute(servletWebRequest, Constants.SESSION_KEY);
            throw new ValidateCodeException(messageSource.getMessage(Constants.AUTH_MESSAGE_VALIDATECODE_EXPIRED,null, locale));
        }
        if (!StringUtils.equalsIgnoreCase(codeInSession.getCode(), codeInRequest)) {
            throw new ValidateCodeException(messageSource.getMessage(Constants.AUTH_MESSAGE_VALIDATECODE_INCORRECT,null, locale));
        }
        sessionStrategy.removeAttribute(servletWebRequest, Constants.SESSION_KEY);

    }
}
