package com.qk.manager.security.validatecode.service;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.qk.manager.common.CommonProperties;
import com.qk.manager.common.Constants;

/**
 * Description: 验证过程对错误码的处理
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年4月2日   
 * @version 1.0
 */
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler  {
	
	@Autowired
    private MessageSource messageSource;
	@Autowired
    private CommonProperties commonProperties;
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {

		//获取本地的语言环境
		Locale locale = LocaleContextHolder.getLocale();
		//设置默认的跳转页
		setDefaultFailureUrl("/login-error");
		setUseForward(true);
		String errorMessage = exception.getMessage();
		if(exception.getMessage().contains(Constants.BAD_CREDENTIALS)) {
			String[] str = exception.getMessage().split(":");
			Object[] args = {str[1]};
			if((Constants.NUM_TYPE_0+"").equals(str[1])) {
				args[0] = commonProperties.getFailCount();
				errorMessage = messageSource.getMessage(Constants.AUTH_MESSAGE_ACCOUNT_IS_LOCKED,args, locale); 
			} else {
				errorMessage = messageSource.getMessage(Constants.AUTH_MESSAGE_CREDENTIALS,args, locale);
			}
		}
		
		if(exception.getMessage().equalsIgnoreCase(Constants.USER_IS_DISABLED)) {
			errorMessage = messageSource.getMessage(Constants.AUTH_MESSAGE_DISABLE, null, locale);
		}
		
		if(exception.getMessage().equalsIgnoreCase(Constants.USER_CREDENTIALS_HAVE_EXPIRED)) {
			errorMessage = messageSource.getMessage(Constants.AUTH_MESSAGE_ACCOUNT_EXPIRED, null, locale);
		}
		
		if(exception.getMessage().equalsIgnoreCase(Constants.USER_CREDENTIALS_HAVE_EXPIRED)) {
			errorMessage = messageSource.getMessage(Constants.AUTH_MESSAGE_PWD_EXPIRED, null, locale);
		}
		
		if(exception.getMessage().equalsIgnoreCase(Constants.USER_ACCOUNT_IS_LOCKED)) {
			errorMessage = messageSource.getMessage(Constants.AUTH_MESSAGE_ACCOUNT_LOCKED, null, locale);
		}
		if(exception.getMessage().equalsIgnoreCase(Constants.USER_NOT_EXIST)) {
			errorMessage = messageSource.getMessage(Constants.AUTH_MESSAGE_ACCOUNT_NOTEXIST, null, locale);
		}
		request.getSession().setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION,errorMessage);
		super.onAuthenticationFailure(request, response, exception);
	}
}
