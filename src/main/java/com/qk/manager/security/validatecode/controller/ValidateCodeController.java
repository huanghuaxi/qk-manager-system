package com.qk.manager.security.validatecode.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import com.qk.manager.common.CommonProperties;
import com.qk.manager.common.Constants;
import com.qk.manager.security.validatecode.common.ImageCode;

/**
 * 
 * <p>Description: 生成验证码类 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author hhx   
 * @date 2019年4月24日   
 * @version 1.0
 */
@RestController
public class ValidateCodeController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
    private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();
    
    @Autowired 
    private CommonProperties validateCodeProperties;

    @RequestMapping("/image/code")
    public void createCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ImageCode imageCode = createImageCode();
        sessionStrategy.setAttribute(new ServletWebRequest(request), Constants.SESSION_KEY, imageCode);
        logger.info("io输出");
        ImageIO.write(imageCode.getImage(), Constants.IMAGE_FORMAT, response.getOutputStream());
    }

    /**
     * Description: 创建图片验证码    
     * @return 
     * date 2019年4月24日
     */
    private ImageCode createImageCode() {

    	int fontHeight =  validateCodeProperties.getHeight() - 5;// 字体的高度 
    	int codeY = validateCodeProperties.getHeight() - 8; 
        BufferedImage image = new BufferedImage(validateCodeProperties.getWidth(), validateCodeProperties.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();
        Random random = new Random();
        g.setColor(getRandColor(Constants.NUM_TYPE_200, Constants.NUM_TYPE_250));
        g.fillRect(Constants.NUM_TYPE_0, Constants.NUM_TYPE_0, validateCodeProperties.getWidth(),  validateCodeProperties.getHeight());
        g.setFont(new Font(Constants.FONT_TIMES_NEW_ROMAN, Font.ITALIC, fontHeight));
        g.setColor(getRandColor(Constants.NUM_TYPE_160, Constants.NUM_TYPE_200));
        logger.info("干扰线装配");
        //干扰线
        for (int i = Constants.NUM_TYPE_0; i < Constants.NUM_TYPE_20; i++) {
            int x = random.nextInt(validateCodeProperties.getWidth());
            int y = random.nextInt( validateCodeProperties.getHeight());
            int xl = random.nextInt(validateCodeProperties.getWidth());
            int yl = random.nextInt(validateCodeProperties.getHeight());
            g.drawLine(x, y, x + xl, y + yl);
        }
        logger.info("噪点装配");
        // 添加噪点 
        float yawpRate = 0.01f;// 噪声率 
        int area = (int) (yawpRate * validateCodeProperties.getWidth() * validateCodeProperties.getHeight()); 
        for (int i = 0; i < area; i++) { 
            int x = random.nextInt(validateCodeProperties.getWidth()); 
            int y = random.nextInt(validateCodeProperties.getHeight()); 
            image.setRGB(x, y, random.nextInt(Constants.NUM_TYPE_255)); 
        }
        
        logger.info("随机字装配");
        //随机字
        StringBuilder sRand = new StringBuilder();
        for (int i = Constants.NUM_TYPE_0; i <  validateCodeProperties.getLength(); i++) {
            int index = random.nextInt(validateCodeProperties.getCode().length());
            char rand = validateCodeProperties.getCode().charAt(index);
            sRand.append(validateCodeProperties.getCode().charAt(index));
            g.setColor(new Color(Constants.NUM_TYPE_20 + random.nextInt(Constants.NUM_TYPE_110), Constants.NUM_TYPE_20 + random.nextInt(Constants.NUM_TYPE_110),Constants.NUM_TYPE_20 + random.nextInt(Constants.NUM_TYPE_110)));
            g.drawString(String.valueOf(rand), Constants.NUM_TYPE_20 * i + Constants.NUM_TYPE_6, codeY);
        }
        g.dispose();
        return new ImageCode(image, sRand.toString(),  validateCodeProperties.getExpireIn());
    }

    /**
     * Description:获取随机色彩    
     * @param fc
     * @param bc
     * @return 
     * date 2019年4月24日
     */
    private Color getRandColor(int fc, int bc) {
        Random random = new Random();
        if (fc > Constants.NUM_TYPE_255) {
            fc = Constants.NUM_TYPE_255;
        }
        if (bc > Constants.NUM_TYPE_255) {
            bc = Constants.NUM_TYPE_255;
        }
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }
}