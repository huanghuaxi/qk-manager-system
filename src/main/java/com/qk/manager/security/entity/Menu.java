package com.qk.manager.security.entity;

import java.util.List;

public class Menu {
	
	private int id;
	
	/**
	 * 菜单名
	 */
	public String label;
	
	/**
	 * 图标
	 */
	public String icon;
	
	/**
	 * 链接
	 */
	public String url;
	
	/**
	 * 是否展开
	 */
	public boolean leaf;
	
	/**
	 * 子菜单
	 */
	public List<Menu> children;
	
	/**
	 * 父节点
	 */
	public int parentId;
	
	/**
	 * 排序
	 */
	public String sequence;
	
	/**
	 * 是否被选中
	 */
	public boolean checked;
	
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public List<Menu> getChildren() {
		return children;
	}
	public void setChildren(List<Menu> children) {
		this.children = children;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean getLeaf() {
		return leaf;
	}
	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}
}
