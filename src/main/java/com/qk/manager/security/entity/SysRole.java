package com.qk.manager.security.entity;

import java.util.Date;
import java.util.List;

/**
 * 
 * Description:角色类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年3月31日   
 * @version 1.0
 */
public class SysRole {
	
	public SysRole () {
		
	}
	
	public SysRole(String roleName,String desc) {
		this.roleName = roleName;
		this.desc = desc;
	}
	
	/**
	 * 主键id
	 */
	private int id;

	/**
	 * 角色名称
	 */
	private String roleName;
	
	/**
	 * 角色描述
	 */
	private String desc;
	
	/**
	 * 权限列表
	 */
	private List<SysPermission> permisList;
	
	/**
	 * 是否有效
	 */
	private boolean state;
	
	/**
	 * 是否有角色（用于用户管理中该用户是否有该角色）
	 */
	private boolean have;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 权限id
	 */
	private String permIds;

	public String getPermIds() {
		return permIds;
	}

	public void setPermIds(String permIds) {
		this.permIds = permIds;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public boolean isHave() {
		return have;
	}

	public void setHave(boolean have) {
		this.have = have;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public List<SysPermission> getPermisList() {
		return permisList;
	}
	public void setPermisList(List<SysPermission> permisList) {
		this.permisList = permisList;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return this.roleName+","+ this.desc;
	}
	
}
