package com.qk.manager.security.entity;

/**
 * 
 * <p>Description: 站点键值配置类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0
 */
public class SiteKv {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	站点键值中文名称
     */
    private String keyCnName;

    /**
     *	站点键值英文名称
     */
    private String keyEnName;

    /**
     *	站点键值描述
     */
    private String descr;

    /**
     *	创建时间
     */
    private String createTime;

    /**
     *	创建人
     */
    private Integer createPerson;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKeyCnName() {
		return keyCnName;
	}

	public void setKeyCnName(String keyCnName) {
		this.keyCnName = keyCnName == null ? null : keyCnName.trim();
	}

	public String getKeyEnName() {
		return keyEnName;
	}

	public void setKeyEnName(String keyEnName) {
		this.keyEnName = keyEnName == null ? null : keyEnName.trim();
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr == null ? null : descr.trim();
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getCreatePerson() {
		return createPerson;
	}

	public void setCreatePerson(Integer createPerson) {
		this.createPerson = createPerson;
	}

}