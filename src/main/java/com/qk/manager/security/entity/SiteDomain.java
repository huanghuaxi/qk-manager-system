package com.qk.manager.security.entity;

/**
 * 
 * <p>Description: 站点域名配置类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0
 */

public class SiteDomain {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	站点域名中文
     */
    private String domainCnName;

    /**
     *	站点域名英文
     */
    private String domainEnName;

    /**
     *	站点域名描述
     */
    private String descr;

    /**
     *	创建时间
     */
    private String createTime;

    /**
     *	创建人
     */
    private Integer createPerson;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDomainCnName() {
		return domainCnName;
	}

	public void setDomainCnName(String domainCnName) {
		this.domainCnName = domainCnName == null ? null : domainCnName.trim();
	}

	public String getDomainEnName() {
		return domainEnName;
	}

	public void setDomainEnName(String domainEnName) {
		this.domainEnName = domainEnName == null ? null : domainEnName.trim();
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr == null ? null : descr.trim();
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getCreatePerson() {
		return createPerson;
	}

	public void setCreatePerson(Integer createPerson) {
		this.createPerson = createPerson;
	}

}