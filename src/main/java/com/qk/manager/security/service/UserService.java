package com.qk.manager.security.service;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.qk.manager.security.entity.Menu;
import com.qk.manager.security.entity.SysRole;
import com.qk.manager.security.entity.SysUser;

/**
 * 
 * Description: 加载用户类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年3月31日   
 * @version 1.0
 */
public interface UserService {
	
   /**
    * Description:根据用户名获取用户信息
    * @date 2019年3月31日    
    * @param userName
    * @return
    */
   public SysUser getUserByName(String userName); 

   /**
    * Description: 加载菜单
    * @date 2019年3月31日    
    * @param roleLst 角色id
    * @return
    */
   public List<Menu> loadMenu(List<SysRole> roleLst);
   
   /**
	 * Description: 更新失败次数
	 * @date 2019年4月1日    
	 * @param uid
	 * @param failCount
	 */
	void updateFailCount(Integer uid,int failCount);
	
	/**
	 * Description: 查询所有用户
	 * @date 2019年4月6日    
	 * @param userName
	 * @return
	 */
	List<SysUser> queryUser(String userName,int start ,int pageSize);
	
	/**
	 * Description: 统计所有用户数
	 * @date 2019年4月6日    
	 * @param userName
	 * @return
	 */
	Integer userCount(String userName);
	
	/**
	 * Description: 删除用户
	 * @date 2019年4月6日    
	 * @param ids
	 * @return
	 */
	void delUser(List<String> ids);
	
	/**
	 * Description: 删除用户与角色的关联关系
	 * @date 2019年4月6日    
	 * @param ids
	 * @return
	 */
	void delUserRoleRef(List<String> ids);
	
	/**
	 * 删除用户数据
	 * @param ids
	 */
	public void delUserRefData(List<String> ids);
	
	/**
	 * Description: 删除用户与角色的关联关系
	 * @date 2019年4月6日    
	 * @param ids
	 * @return
	 */
	List<SysRole> selectAllBySate(String userId);
   
	/**
	 * Description: 增加用户信息   
	 * @param sysuser 
	 * date 2019年4月10日
	 */
	void addUser(SysUser sysuser);
	
	/**
	 * Description: 插入用户角色关联关系表
	 * @param userId
	 * @param roleId 
	 * date 2019年4月10日
	 */
	void insertUserRoleRef(Integer userId,String roleId);
	
	/**
	 * Description:  根据id查询用户 
	 * @param uid
	 * @return 
	 * date 2019年4月11日
	 */
	SysUser queryUserById(@Param("uid") String uid);

	/**
	 * Description:删除角色和用户关联关系    
	 * @param uid
	 * @param rid 
	 * date 2019年4月11日
	 */
    void delUserRoleRef(Integer uid ,String rid);
    
    /**
     * Description: 更新用户信息    
     * @param sysUser 用户信息
     * date 2019年4月11日
     */
    void updateUserConf(SysUser sysUser);
    
    /**
	 * Description: 更新用户信息
	 * @param sysUser 
	 * date 2019年4月11日
	 */
	void updateUserById(SysUser sysUser);
	
	/**
	 * Description:根据状态改值    
	 * @param uid 用户id
	 * @param value 值
	 * @param type 类型
	 * date 2019年4月12日
	 */
	void updateStateById(String uid,boolean value,String type);
	
	/**
	 * Description: 更改状态
	 * @param uid 用户id
	 * @param value 值
	 * @param type 修改类型
	 * date 2019年4月12日
	 */
	public void updateStateBusiness(String uid, boolean value, String type);
	
	/**
	 * Description: 查找用户是否唯一
	 * @param userName
	 * @return 
	 * date 2019年4月18日
	 */
	Integer userUnique(String userName);
	
	/**
	 * 
	 * Description: 修改密码   
	 * @param oldPasswrod
	 * @param newPasswrod
	 * @param confirmPasswrod
	 * @return 
	 * date 2019年4月25日
	 */
	public Integer changePassword(String oldPassword, String newPassword, String confirmPassword);
	
	/**
	 * 
	 * Description: 修改最后登录时间和IP    
	 * @param id
	 * @param lastLoginTime
	 * @param lastLoginIp 
	 * date 2019年4月25日
	 */
	void updateLastTime(Integer id, Date lastLoginTime, String lastLoginIp);
	
	/**
	 * 
	 * Description: 获取角色下的所有用户 
	 * @return 
	 * date 2019年9月11日
	 */
	public List<Integer> getRoleUser();
}