/**
 * 
 */
package com.qk.manager.security.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.qk.manager.security.entity.SiteDomainKv;

/**    
 * <p>Description: 加载域名键值绑定类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0   
 */
public interface SiteDomainKvService {
	
	/**
     * 
     * Description: 查询所有域名键值绑定  
     * @param siteDomainKv
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月17日
     */
    public List<SiteDomainKv> querySiteDomainKv(SiteDomainKv siteDomainKv, Integer start, Integer pageSize);
    
    /**
     * 
     * Description: 统计所有域名键值绑定    
     * @param siteDomainKv
     * @return 
     * date 2019年4月17日
     */
    public Integer siteDomainKvCount(SiteDomainKv siteDomainKv);
    
    /**
     * 
     * Description: 新增域名键值绑定
     * @param siteDomainKv 
     * date 2019年4月17日
     */
    public void saveSiteDomainKv(SiteDomainKv siteDomainKv);
    
    /**
     * 
     * Description: 编辑域名键值绑定   
     * @param siteDomainKv 
     * date 2019年4月17日
     */
    public void editSiteDomainKv(SiteDomainKv siteDomainKv);
    
    /**
     * 
     * Description: 删除域名键值绑定 
     * @param ids 
     * date 2019年4月17日
     */
    public void delSiteDomainKv(@Param("ids") List<String> ids);
	
}
