/**
 * 
 */
package com.qk.manager.security.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.qk.manager.common.CurrentParamUtil;
import com.qk.manager.security.entity.SiteKv;
import com.qk.manager.security.entity.SysUser;
import com.qk.manager.security.mapper.SiteKvMapper;
import com.qk.manager.security.service.SiteKvService;

/**    
 * <p>Description: 站点键值配置更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0   
 */

@Service
public class SiteKvServiceImpl implements SiteKvService{
	
	@Autowired
	private SiteKvMapper siteKvMapper;
	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: querySiteKv</p>   
	 * <p>Description: 查询所有站点键值配置</p>   
	 * @param siteKv
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.security.service.SiteKvService#querySiteKv(com.activity.manager.security.entity.SiteKv, java.lang.Integer, java.lang.Integer)   
	 */
	@Override
	public List<SiteKv> querySiteKv(SiteKv siteKv, Integer start, Integer pageSize) {
		return siteKvMapper.querySiteKv(siteKv, start, pageSize);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: siteKvCount</p>   
	 * <p>Description: 统计所有站点键值配置</p>   
	 * @param siteKv
	 * @return   
	 * @see com.activity.manager.security.service.SiteKvService#siteKvCount(com.activity.manager.security.entity.SiteKv)   
	 */
	@Override
	public Integer siteKvCount(SiteKv siteKv) {
		return siteKvMapper.siteKvCount(siteKv);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: saveSiteKv</p>   
	 * <p>Description: 新增站点键值配置 </p>   
	 * @param siteKv   
	 * @see com.activity.manager.security.service.SiteKvService#saveSiteKv(com.activity.manager.security.entity.SiteKv)   
	 */
	@Override
	public void saveSiteKv(SiteKv siteKv) {
		Assert.hasText(siteKv.getKeyEnName(), "站点键值英文名称数据不存在");
		Assert.hasText(siteKv.getKeyCnName(), "站点键值中文名称数据不存在");
		//查询该站点键值英文名是否存在
		SiteKv siteKey = siteKvMapper.selectByEnName(siteKv.getKeyEnName());
		Assert.isNull(siteKey, "站点键值英文不能重复");
		SysUser user = CurrentParamUtil.getCurrentUser();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		siteKv.setCreatePerson(user.getId());
		siteKv.setCreateTime(format.format(new Date()));
		siteKvMapper.insert(siteKv);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: eidtSiteKv</p>   
	 * <p>Description: 编辑站点键值配置</p>   
	 * @param siteKv   
	 * @see com.activity.manager.security.service.SiteKvService#eidtSiteKv(com.activity.manager.security.entity.SiteKv)   
	 */
	@Override
	public void editSiteKv(SiteKv siteKv) {
		Assert.notNull(siteKv.getId(), "站点键值ID数据不存在");
		Assert.hasText(siteKv.getKeyEnName(), "站点键值英文名称数据不存在");
		Assert.hasText(siteKv.getKeyCnName(), "站点键值中文名称数据不存在");
		SiteKv kv = siteKvMapper.selectByPrimaryKey(siteKv.getId());
		Assert.notNull(kv, "站点键值数据不存在");
		kv.setKeyCnName(siteKv.getKeyCnName());
		kv.setKeyEnName(siteKv.getKeyEnName());
		kv.setDescr(siteKv.getDescr());
		siteKvMapper.updateByPrimaryKey(kv);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: delSiteKv</p>   
	 * <p>Description: 删除站点键值配置</p>   
	 * @param ids   
	 * @see com.activity.manager.security.service.SiteKvService#delSiteKv(java.util.List)   
	 */
	@Override
	public Boolean delSiteKv(List<String> ids) {
		//查询要删除的键是否已经绑定域名
		Integer count = siteKvMapper.siteDomainKvByIdsCount(ids);
		if(count == 0) {
			siteKvMapper.delSiteKv(ids);
			return true;
		}
		return false;
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: selectAll</p>   
	 * <p>Description: 查询所有站点键值配置</p>   
	 * @return   
	 * @see com.activity.manager.security.service.SiteKvService#selectAll()   
	 */
	@Override
	public List<SiteKv> selectAll() {
		return siteKvMapper.selectAll();
	}

}
