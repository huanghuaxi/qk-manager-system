/**
 * 
 */
package com.qk.manager.security.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.qk.manager.common.CurrentParamUtil;
import com.qk.manager.security.entity.SysUser;
import com.qk.manager.security.entity.SysWhiteList;
import com.qk.manager.security.mapper.SysWhiteListMapper;
import com.qk.manager.security.service.WhiteListService;

/**    
 * <p>Description: 白名单更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0   
 */

@Service
public class WhiteListServiceImpl implements WhiteListService{

	@Autowired
	private SysWhiteListMapper whiteListMapper;
	
	/* 
	 *(non-Javadoc)   
	 * <p>Title: queryWhiteList</p>   
	 * <p>Description: 查询所有白名单</p>   
	 * @param whiteList
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.activity.manager.security.service.WhiteListService#queryWhiteList(com.activity.manager.security.entity.SysWhiteList, java.lang.Integer, java.lang.Integer)   
	 */
	@Override
	public List<SysWhiteList> queryWhiteList(SysWhiteList whiteList, Integer start, Integer pageSize) {
		return whiteListMapper.queryWhiteList(whiteList, start, pageSize);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: whiteListCount</p>   
	 * <p>Description: 统计所有白名单</p>   
	 * @param whiteList
	 * @return   
	 * @see com.activity.manager.security.service.WhiteListService#whiteListCount(com.activity.manager.security.entity.SysWhiteList)   
	 */
	@Override
	public Integer whiteListCount(SysWhiteList whiteList) {
		return whiteListMapper.whiteListCount(whiteList);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: saveWhiteList</p>   
	 * <p>Description: 新增白名单</p>   
	 * @param whiteList   
	 * @see com.activity.manager.security.service.WhiteListService#saveWhiteList(com.activity.manager.security.entity.SysWhiteList)   
	 */
	@Override
	public void saveWhiteList(SysWhiteList whiteList) {
		Assert.hasText(whiteList.getIpAddr(), "IP地址数据不存在");
		Assert.notNull(whiteList.getType(), "名单类型数据不存在");
		SysUser user = CurrentParamUtil.getCurrentUser();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		whiteList.setCreatePersion(user.getId());
		whiteList.setCreateTime(format.format(new Date()));
		whiteListMapper.insert(whiteList);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: editWhiteList</p>   
	 * <p>Description: 编辑白名单</p>   
	 * @param whiteList   
	 * @see com.activity.manager.security.service.WhiteListService#editWhiteList(com.activity.manager.security.entity.SysWhiteList)   
	 */
	@Override
	public void editWhiteList(SysWhiteList whiteList) {
		Assert.notNull(whiteList.getId(), "ID数据不存在");
		Assert.hasText(whiteList.getIpAddr(), "IP地址数据不存在");
		Assert.notNull(whiteList.getType(), "名单类型数据不存在");
		SysWhiteList wList = whiteListMapper.selectByPrimaryKey(whiteList.getId());
		Assert.notNull(wList, "ID：" + whiteList.getId() + "的白名单数据不存在");
		wList.setIpAddr(whiteList.getIpAddr());
		wList.setType(whiteList.getType());
		whiteListMapper.updateByPrimaryKey(wList);
	}

	/* 
	 *(non-Javadoc)   
	 * <p>Title: delWhiteList</p>   
	 * <p>Description: 删除白名单</p>   
	 * @param ids   
	 * @see com.activity.manager.security.service.WhiteListService#delWhiteList(java.util.List)   
	 */
	@Override
	public void delWhiteList(List<String> ids) {
		whiteListMapper.delWhiteList(ids);
	}

}
