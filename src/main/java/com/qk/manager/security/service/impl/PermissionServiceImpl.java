package com.qk.manager.security.service.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qk.manager.security.entity.SysPermission;
import com.qk.manager.security.mapper.PermissionMapper;
import com.qk.manager.security.service.PermissionService;
/**
 * <p>Description: 权限操作业务类 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年4月14日   
 * @version 1.0
 */
@Service
public class PermissionServiceImpl implements PermissionService {
	
	@Autowired
	private PermissionMapper permissionMapper;

	@Override
	public List<SysPermission> queryPermission(String permName, int start, int pageSize) {
		return permissionMapper.queryPermission(permName,start, pageSize);
	}

	@Override
	public Integer permCount(String permName) {
		return permissionMapper.permCount(permName);
	}

	@Override
	public void delPermission(List<String> ids) {
		permissionMapper.delPerm(ids);
	}

	@Transactional
	@Override
	public void delPerm(List<String> ids) {
		for(String id :ids) {
			SysPermission perm = queryPermissionById(id);
			int count = 0;
			if(perm != null) {
				if(!perm.isLeaf()) {
					throw new RuntimeException("该节点为父节点，请删除子节点");
				} else {
					count = countChild(String.valueOf(perm.getParentId()),false);
				}
				delPermission(Arrays.asList(id));
				delPermAndRoleRef(Arrays.asList(id));
				if(count == 1) {
					updatePermLeafState(true, perm.getParentId());
				}
			}
		}
	}

	@Override
	public void delPermAndRoleRef(List<String> ids) {
		permissionMapper.delPermAndRoleRef(ids);		
	}
	
	@Transactional
	@Override
	public void savePerm(SysPermission perm, int uid) {
		perm.setUid(uid);
		//不是操作项则把父节点改为不是叶子节点
		int count = countChild(String.valueOf(perm.getParentId()),false);
		insertPerm(perm,uid);
		if(count==0 && !perm.getIsAction()) {
			updatePermLeafState(false,perm.getParentId());
		}
	}
	
	@Override
	public void insertPerm(SysPermission perm, int uid) {
		perm.setLeaf(true);
		permissionMapper.insertPerm(perm);
	}

	@Override
	public void updatePermLeafState(boolean isLeaf, int id) {
		permissionMapper.updatePermLeafState(isLeaf, id);
	}

	@Override
	public SysPermission queryPermissionById(String id) {
		return permissionMapper.queryPermissionById(id);
	}

	@Transactional
	@Override
	public void updatePerm(SysPermission perm) {
		//不是操作项则把父节点改为不是叶子节点
		permissionMapper.updatePerm(perm);
		boolean isLeaf = perm.getIsAction();
		updatePermLeafState(isLeaf,perm.getParentId());
	}

	@Override
	public void updateStateById(String uid, boolean value, String type) {
		permissionMapper.updateStateById(uid, value, type);
	}

	@Override
	public Integer countChild(String parentId,Boolean isAction) {
		return permissionMapper.countChild(parentId, isAction);
	}
}
