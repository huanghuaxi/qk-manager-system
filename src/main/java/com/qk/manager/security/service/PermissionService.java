package com.qk.manager.security.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.qk.manager.security.entity.SysPermission;

/**
 * <p>Description: 权限业务类 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author binghe  
 * @date 2019年4月14日   
 * @version 1.0
 */
@Service
public interface PermissionService {
	
	/**
	 * Description: 查询所有权限
	 * @date 2019年4月6日    
	 * @param permName
	 * @return
	 */
	List<SysPermission> queryPermission(String permName,int start ,int pageSize);
	
	/**
	 * Description: 统计所有权限树
	 * @date 2019年4月6日    
	 * @param permName
	 * @return
	 */
	Integer permCount(String permName);
	
	/**
	 * Description: 删除权限
	 * @date 2019年4月6日    
	 * @param ids
	 * @return
	 */
	void delPermission(List<String> ids);

	/**
	 * Description: 删除权限角色关联关系  
	 * @param ids 
	 * date 2019年4月14日
	 */
	void delPerm(List<String> ids);
	
	/**
	 * Description: 删除角色跟权限的关联关系    
	 * @param ids 
	 * date 2019年4月14日
	 */
	void delPermAndRoleRef(List<String> ids);
	
	/**
	 * Description: 插入权限数据
	 * @param perm 权限对象
	 * @param uid 用户id
	 * date 2019年4月14日
	 */
	void insertPerm(SysPermission perm,int uid);
	
	/**
	 * Description: 插入权限数据
	 * @param perm 权限对象
	 * @param uid 用户id
	 * date 2019年4月14日
	 */
	void savePerm(SysPermission perm,int uid);
	
	/**
	 * Description: 更新菜单叶子节点字段
	 * @param isLeaf
	 * @param id 
	 * date 2019年4月14日
	 */
	void updatePermLeafState(boolean isLeaf,int id);
	
	/**
	 * Description: 查询权限 
	 * @param id
	 * @return 
	 * date 2019年4月14日
	 */
	SysPermission queryPermissionById(String id);
	
	/**
	 * Description: 更新权限   
	 * @param perm 权限对象
	 * date 2019年4月15日
	 */
	void updatePerm(SysPermission perm);
	
	/**
	 * Description:根据状态改值    
	 * @param uid 用户id
	 * @param value 值
	 * @param type 类型
	 * date 2019年4月12日
	 */
	void updateStateById(String uid,boolean value,String type);
	
	/**
	 * Description:  计算子节点  
	 * @param parentId 父节点
	 * @return 
	 * date 2019年5月1日
	 */
	Integer countChild(String parentId,Boolean isAction);
	
}
