package com.qk.manager.security.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * 自定义 验证码异常类
 * 
 */
public class ValidateCodeException extends AuthenticationException {
 
    private static final long serialVersionUID = 5022575393500654458L;

	public ValidateCodeException(String msg) {
        super(msg);
    }
}
