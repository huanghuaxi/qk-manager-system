package com.qk.manager.security.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qk.manager.business.controller.PageUtil;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.CurrentParamUtil;
import com.qk.manager.common.ResultMessage;
import com.qk.manager.security.entity.SysRole;
import com.qk.manager.security.entity.SysUser;
import com.qk.manager.security.entity.UserDTO;
import com.qk.manager.security.service.UserService;

/**
 * Description: 用户管理控制类
 * Copyright: Copyright (c) 2019    
 * @author binghe   
 * @date 2019年4月4日   
 * @version 1.0   
 */
@Controller
@RequestMapping("/user")
public class UserController {	

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private UserService userService;
	
	/**
	 * Description: 页面跳转    
	 * @return 
	 * date 2019年4月12日
	 */
	@RequestMapping("/list")
	public String toUserList() {
		return "sysmanage/user/usermanage.html";
	}
	
	@RequestMapping("/grid")
	@ResponseBody
	public  PageUtil<UserDTO> getList(PageUtil<UserDTO> pageUtil,String userName){
	    List<SysUser> ulist = userService.queryUser(userName,pageUtil.getCurrIndex(),pageUtil.getLimit());
	    List<UserDTO> uLst =  formate(ulist);
		Integer count = userService.userCount(userName);
		pageUtil.setCode("0");
		pageUtil.setCount(count);
		pageUtil.setMsg("成功");
		pageUtil.setData(uLst);
		return pageUtil;
	}
	
	/**
	 * 用户删除
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public ResultMessage del (String ids){
        try {
		 Assert.hasText(ids, "数据不存在");
		 String str = ids.substring(0, ids.length()-1);
		 userService.delUserRefData(Arrays.asList(str.split(",")));
        }catch(Exception e) {
        	logger.error("删除异常",e);
        	return  ResultMessage.getFail();
		}
		return  ResultMessage.getSuccess();
	}
	
	/**
	 * 新增用户
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public ResultMessage addUser(SysUser sysUser) {
		try {
	        Assert.notNull(sysUser,"该对象为空");		
			Integer coun = userService.userUnique(sysUser.getUsername());
			if(coun>0) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE_USER_ISEXIST, CodeUtil.FAIL_CODE_USER_ISEXIST_MSG, null);
			}
			userService.addUser(sysUser);	
		}catch(Exception e) {
			logger.error("用户增加失败",e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 将数据转化成dto形式
	 * @param list
	 * @return
	 */
	private List<UserDTO> formate(List<SysUser> list){
		List<UserDTO> lst = new ArrayList<UserDTO>();
		list.forEach(sysUser -> {
			UserDTO userDTO = new UserDTO();
			userDTO.setId(sysUser.getId());
			userDTO.setUserName(sysUser.getUserName());
			userDTO.setMail(sysUser.getMail());
			userDTO.setCreateTime(sysUser.getCreateTime());
			userDTO.setDescr(sysUser.getDescr());
			userDTO.setIsLock(sysUser.getIsLock());
			userDTO.setIsValide(sysUser.getIsValide());
			lst.add(userDTO);
		});
		return lst;
	}

	/**
	 * Description: 修改用户信息    
	 * @param sysUser
	 * @return 
	 * date 2019年4月12日
	 */
	@RequestMapping("/update")
	@ResponseBody
	public ResultMessage update(SysUser sysUser) {
		try {
			Assert.hasText(sysUser.getRoleStr(), "空指针异常");
		    userService.updateUserConf(sysUser);
		}catch(Exception e) {
			logger.error("修改客户信息报错",e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	@RequestMapping("/role")
	@ResponseBody
	public List<SysRole> selectRole(String id) {
		return userService.selectAllBySate(id);
	}
	
	/**
	 * 重定向新增页面
	 * @return
	 */
	@GetMapping("/redirect")
	public String addRedirect(String userId,Model model) {
		model.addAttribute("id", userId);
		return "sysmanage/user/saveOrEdit.html";
	}
	
	/**
	 * Description: 查询用户    
	 * @param id 用户id
	 * @return  用户对象
	 * date 2019年4月12日
	 */
	@RequestMapping("/getUserConf")
	@ResponseBody
	public SysUser getUserConf(String id) {
		return userService.queryUserById(id);
	}
	
	/**
	 * Description:更改用户状态    
	 * @param type 类型
	 * @param id 用户id
	 * @param value 值
	 * @return 结果对象
	 * date 2019年4月12日
	 */
	@RequestMapping("/updateState")
	@ResponseBody
	public ResultMessage updateState(String type, String id, boolean value) {
		try {
		   userService.updateStateById(id, value, type);
		}catch(Exception e) {
		   logger.error("修改状态失败", e);
	       return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	
	@RequestMapping("/unique")
	@ResponseBody
	public ResultMessage unique(String userName) {
	 try {
		   Integer coun = userService.userUnique(userName);
		   return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, coun);
	    }catch(Exception e) {
		   logger.error("修改状态失败", e);
		   return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description:修改密码
	 * @param oldPassword
	 * @param newPassword
	 * @param confirmPassword
	 * @return
	 * date 2019年4月25日
	 */
	@ResponseBody
	@RequestMapping("/changePassword")
	public ResultMessage changePassword(String oldPassword, String newPassword, 
				String confirmPassword) {
		try {
			Integer result = userService.changePassword(oldPassword.trim(), 
						newPassword.trim(), confirmPassword.trim());
			logger.info("修改用户密码");
			if(result == 0) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "修改密码失败，密码不正确", null);
			}else if(result == 1) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "修改密码失败，新密码不正确", null);
			}else {
				//获取当前用户
				SysUser user = CurrentParamUtil.getCurrentUser();
				return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, 
							user.getUserName());
			}
		}catch(Exception e) {
			logger.error("修改密码异常", e);
			return ResultMessage.getFail();
		}
	}
}
