/**
 * 
 */
package com.qk.manager.security.controller;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qk.manager.business.controller.PageUtil;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.ResultMessage;
import com.qk.manager.security.entity.SiteDomain;
import com.qk.manager.security.service.SiteDomainService;

/**    
 * <p>Description: 站点域名配置控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/siteDomain")
public class SiteDomainController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private SiteDomainService siteDomainService;
	
	/**
	 * 
	 * Description: 跳转到站点域名配置页面    
	 * @return 
	 * date 2019年4月17日
	 */
	@RequestMapping("/list")
	public String toSiteDomainList() {
		logger.info("跳转到站点域名配置页面");
		return "sysmanage/site/siteDomain.html";
	}
	
	/**
	 * 
	 * Description: 获取站点域名配置列表   
	 * @param pageUtil
	 * @param siteDomain
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil<SiteDomain> getList(PageUtil<SiteDomain> pageUtil, SiteDomain siteDomain) {
		try {
			List<SiteDomain> kList = siteDomainService.querySiteDomain(siteDomain, pageUtil.getCurrIndex(), pageUtil.getLimit());
			Integer count = siteDomainService.siteDomainCount(siteDomain);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(kList);
			logger.info("获取站点域名配置列表");
		}catch(Exception e) {
			logger.error("获取站点域名配置列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 新增站点域名配置    
	 * @param siteDomain
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage save(SiteDomain siteDomain) {
		try {
			siteDomainService.saveSiteDomain(siteDomain);
			logger.info("新增站点域名配置");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增站点域名配置异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 编辑站点域名配置   
	 * @param siteDomain
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(SiteDomain siteDomain) {
		try {
			siteDomainService.editSiteDomain(siteDomain);
			logger.info("编辑站点域名配置");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("编辑站点域名配置异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 删除站点域名配置   
	 * @param ids
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultMessage delete(String ids) {
		try {
			Assert.hasText(ids, "站点域名配置ID数据不存在");
			Boolean b =siteDomainService.delSiteDomain(Arrays.asList(ids.split(",")));
			logger.info("删除站点域名配置");
			if(b) {
				return ResultMessage.getSuccess();
			}else {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "要删除的域名已经绑定键值", null);
			}
		}catch(Exception e) {
			logger.error("删除站点域名配置异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取站点域名列表    
	 * @return 
	 * date 2019年4月17日
	 */
	@ResponseBody
	@RequestMapping("/getSiteDomainList")
	public ResultMessage getSiteDomainList() {
		try {
			List<SiteDomain> siteDomainList = siteDomainService.selectAll();
			logger.info("获取站点域名列表");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, siteDomainList);
		}catch(Exception e) {
			logger.error("获取站点域名列表异常", e);
			return ResultMessage.getFail();
		}
	}
}
