/**
 * 
 */
package com.qk.manager.security.controller;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qk.manager.business.controller.PageUtil;
import com.qk.manager.common.ResultMessage;
import com.qk.manager.security.entity.SysWhiteList;
import com.qk.manager.security.service.WhiteListService;

/**    
 * <p>Description: 白名单控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月16日   
 * @version 1.0   
 */

@Controller
@RequestMapping("/whiteList")
public class WhiteListController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private WhiteListService whiteListService;
	
	/**
	 * 
	 * Description: 跳转到白名单配置页面   
	 * @return 
	 * date 2019年4月16日
	 */
	@RequestMapping("/list")
	public String toList() {
		logger.info("跳转到白名单配置页面");
		return "sysmanage/whiteList/whiteListConf.html";
	}
	
	/**
	 * 
	 * Description: 获取白名单列表   
	 * @param pageUtil
	 * @param whiteList
	 * @return 
	 * date 2019年4月16日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil<SysWhiteList> getList(PageUtil<SysWhiteList> pageUtil, SysWhiteList whiteList) {
		try {
			List<SysWhiteList> wList = whiteListService.queryWhiteList(whiteList, pageUtil.getCurrIndex(), pageUtil.getLimit());
			Integer count = whiteListService.whiteListCount(whiteList);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(wList);
			logger.info("获取白名单列表");
		}catch(Exception e) {
			logger.error("获取白名单列表异常", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 新增白名单   
	 * @param whiteList
	 * @return 
	 * date 2019年4月16日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage save(SysWhiteList whiteList) {
		try {
			whiteListService.saveWhiteList(whiteList);
			logger.info("新增白名单");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增白名单异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 编辑白名单   
	 * @param whiteList
	 * @return 
	 * date 2019年4月16日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(SysWhiteList whiteList) {
		try {
			whiteListService.editWhiteList(whiteList);
			logger.info("编辑白名单");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("编辑白名单异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 删除白名单   
	 * @param ids
	 * @return 
	 * date 2019年4月16日
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultMessage delete(String ids) {
		try {
			Assert.hasText(ids, "白名单ID数据不存在");
			whiteListService.delWhiteList(Arrays.asList(ids.split(",")));
			logger.info("删除白名单");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("删除白名单异常", e);
			return ResultMessage.getFail();
		}
	}
}
