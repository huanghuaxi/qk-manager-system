package com.qk.manager.security.controller;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qk.manager.business.controller.PageUtil;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.CurrentParamUtil;
import com.qk.manager.common.ResultMessage;
import com.qk.manager.security.entity.SysPermission;
import com.qk.manager.security.service.PermissionService;

/**
 * 
 * Description:    
 * Copyright: Copyright (c) 2019
 * @author huanghx   
 * @date 2019年4月14日   
 * @version 1.0
 */
@Controller
@RequestMapping("/perm")
public class PermissionController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private PermissionService permissionService;
	
	/**
	 * Description: 页面跳转    
	 * @return 
	 * date 2019年4月12日
	 */
	@RequestMapping("/list")
	public String toUserList() {
		return "sysmanage/perm/permission.html";
	}
	
	@RequestMapping("/grid")
	@ResponseBody
	public  PageUtil<SysPermission> getList(PageUtil<SysPermission> pageUtil,String permName){
	    List<SysPermission> plist = permissionService.queryPermission(permName,pageUtil.getCurrIndex(),pageUtil.getLimit());
		Integer count = permissionService.permCount(permName);
		pageUtil.setCode("0");
		pageUtil.setCount(count);
		pageUtil.setMsg("成功");
		pageUtil.setData(plist);
		return pageUtil;
	}
	
	/**
	 * 重定向新增页面
	 * @return
	 */
	@GetMapping("/redirect")
	public String addRedirect(String permId,Model model) {
		model.addAttribute("id", permId);
		return "sysmanage/perm/saveOrEdit.html";
	}
	
	/**
	 * 用户删除
	 * @return
	 */
	@RequestMapping("/del")
	@ResponseBody
	public ResultMessage del (String ids){
        try {
         Assert.hasText(ids, "权限ids数据不存在");
         logger.info("用户：【"+CurrentParamUtil.getCurrentUser().getUsername()+"】，删除数据，ids【"+ids+"】");	
		 String str = ids.substring(0, ids.length()-1);
		 permissionService.delPerm(Arrays.asList(str.split(",")));
        }catch(Exception e) {
        	logger.error("删除权限数据异常：",e);
        	return  ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, e.getMessage()!=null?e.getMessage():CodeUtil.FAIL_CODE_MSG, null);
		}
		return  ResultMessage.getSuccess();
	}
	
	/**
	 * 新增权限
	 * @return
	 */
	@RequestMapping("/add")
	@ResponseBody
	public ResultMessage addPerm(SysPermission perm) {
	 try {	
		  Assert.notNull(perm, "权限对象为空");
		  logger.info("用户：【"+CurrentParamUtil.getCurrentUser().getUsername()+"】，新增数据【"+perm+"】");	
		  permissionService.savePerm(perm, CurrentParamUtil.getCurrentUser().getId()); 
	 }catch(Exception e) {
		 logger.error("删除权限数据异常：",e);
		 return  ResultMessage.getFail();
	 }
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 新增权限
	 * @return
	 */
	@RequestMapping("/queryById")
	@ResponseBody
	public ResultMessage queryById(String id) {
	 try {	
		  Assert.hasText(id, "权限对象为空");
		  logger.info("用户：【"+CurrentParamUtil.getCurrentUser().getUsername()+"】，修改前查询：【"+id+"】");	
		  SysPermission perm = permissionService.queryPermissionById(id); 
		  Assert.notNull(perm, "查询权限对象为空");
		  return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, perm);
	 }catch(Exception e) {
		 logger.error("删除权限数据异常：",e);
		 return  ResultMessage.getFail();
	 }
	}
	
	/**
	 * 新增权限
	 * @return
	 */
	@RequestMapping("/updatePerm")
	@ResponseBody
	public ResultMessage updatePerm(SysPermission perm) {
	 try {	
		  Assert.notNull(perm, "权限对象为空");
		  logger.info("用户：【"+CurrentParamUtil.getCurrentUser().getUsername()+"】，修改前查询：【"+perm+"】");	
		  permissionService.updatePerm(perm);
		  return ResultMessage.getSuccess();
	 }catch(Exception e) {
		 logger.error("修改权限数据异常：",e);
		 return  ResultMessage.getFail();
	 }
	}
	
	/**
	 * Description:更改用户状态    
	 * @param type 类型
	 * @param id 用户id
	 * @param value 值
	 * @return 结果对象
	 * date 2019年4月12日
	 */
	@RequestMapping("/updateState")
	@ResponseBody
	public ResultMessage updateState(String type, String id, boolean value) {
		try {
			permissionService.updateStateById(id, value, type);
		}catch(Exception e) {
		   logger.error("修改状态失败", e);
	       return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
}
