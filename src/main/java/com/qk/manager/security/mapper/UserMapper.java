package com.qk.manager.security.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.qk.manager.security.entity.SysUser;

/**
 * Description:   
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年4月1日   
 * @version 1.0
 */
@Mapper 
public interface UserMapper {
	
	/**
	 * Description:
	 * @date 2019年4月1日    
	 * @param userName
	 * @return
	 */
	SysUser selectByUserName(String userName);
	
	/**
	 * Description: 更新失败次数
	 * @date 2019年4月1日    
	 * @param uid
	 * @param failCount
	 */
	void updateFailCount(Integer uid,int failCount);
	
	/**
	 * Description: 用户解锁，锁方法
	 * @date 2019年4月2日    
	 * @param uid
	 * @param isLock
	 */
	void updateUserLockAndUnLock(Integer uid,int isLock);
	
	/**
	 * Description: 查询所有用户
	 * @date 2019年4月6日    
	 * @param userName
	 * @return
	 */
	List<SysUser> queryUser(@Param("userName") String userName,int start ,int pageSize);
	
	/**
	 * Description: 统计所有用户数
	 * @date 2019年4月6日    
	 * @param userName
	 * @return
	 */
	Integer userCount(@Param("userName") String userName);
	
	/**
	 * Description: 删除用户
	 * @date 2019年4月6日    
	 * @param ids
	 * @return
	 */
	void delUser(@Param("ids") List<String> ids);
	
	/**
	 * Description: 删除用户与角色的关联关系
	 * @date 2019年4月6日    
	 * @param ids
	 * @return
	 */
	void delUserRoleRef(@Param("ids") List<String> ids);
	
	/**
	 * 
	 * Description: 增加用户信息
	 * @param user 
	 * date 2019年4月10日
	 */
	Integer addUser(SysUser user);
	
	/**
	 * Description: 插入用户跟角色的关联关系  
	 * @param userId
	 * @param roleId 
	 * date 2019年4月10日
	 */
	void insertUserRoleRef(@Param("userId") Integer userId,@Param("roleId") String roleId);
	
	/**
	 * Description:  根据id查询用户 
	 * @param uid
	 * @return 
	 * date 2019年4月11日
	 */
	SysUser queryUserById(@Param("uid") String uid);
	
	/**
	 * Description: 更新用户信息
	 * @param sysUser 
	 * date 2019年4月11日
	 */
	void updateUserById(SysUser sysUser);
	
	/**
	 * Description:根据状态改值    
	 * @param uid
	 * @param value
	 * @param type 
	 * date 2019年4月12日
	 */
	void updateStateById(@Param("id") String uid,@Param("value") boolean value,@Param("type") String type);
	
	/**
	 * Description: 查看用户是否唯一   
	 * @param userName
	 * @return 
	 * date 2019年4月18日
	 */
	Integer userUnique(@Param("userName") String userName);
	
	/**
	 * 
	 * Description: 修改密码    
	 * @param id
	 * @param pwd 
	 * date 2019年4月25日
	 */
	void updatePwdById(@Param("id") Integer id, @Param("pwd") String pwd);
	
	/**
	 * 
	 * Description: 修改最后登录时间和IP    
	 * @param id
	 * @param lastLoginTime
	 * @param lastLoginIp 
	 * date 2019年4月25日
	 */
	void updateLastTime(@Param("id") Integer id, @Param("lastLoginTime") Date lastLoginTime, @Param("lastLoginIp") String lastLoginIp);
	
	/**
	 * 
	 * Description: 获取角色下的所有用户 
	 * @param userId
	 * @return 
	 * date 2019年9月11日
	 */
	List<Integer> selectIdByRole(@Param("userId") Integer userId);
}
