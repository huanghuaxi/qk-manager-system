package com.qk.manager.security.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.qk.manager.security.entity.SysRole;


@Mapper
public interface SysRoleMapper {
  
    int deleteByPrimaryKey(Integer id);
    
    int insert(SysRole record);

    SysRole selectByPrimaryKey(Integer id);

    List<SysRole> selectAll();

    int updateByPrimaryKey(SysRole record);
    
    List<SysRole> selectAllBySate();
    
    List<SysRole> selectAllByUserId(@Param("userId") String userId);
    
    /**
     * Description: 删除用户和角色关联关系    
     * @param uid 用户id
     * @param rid 角色id
     * date 2019年4月11日
     */
    void delUserRoleRef(@Param("uid") Integer uid,@Param("rid") String rid);
    
	/**
	 * Description: 查询所有角色
	 * @date 2019年4月6日    
	 * @param userName
	 * @return
	 */
	List<SysRole> queryRole(@Param("roleName") String roleName,int start ,int pageSize);
	
	/**
	 * Description: 统计所有角色数
	 * @date 2019年4月6日    
	 * @param userName
	 * @return
	 */
	Integer roleCount(@Param("roleName") String roleName);
	
	/**
	 * Description: 删除角色
	 * @date 2019年4月6日    
	 * @param ids
	 * @return
	 */
	void delRole(@Param("ids") List<String> ids);
	
	/**
	 * Description: 删除用户与角色的关联关系
	 * @date 2019年4月6日    
	 * @param ids
	 * @return
	 */
	void delRoleUserRef(@Param("ids") List<String> ids);
	
	/**
	 * Description: 删除角色权限关联关系
	 * @param ids 
	 * date 2019年4月12日
	 */
	void delRolePermRef(@Param("ids") List<String> ids);
	
	/**
	 * Description:根据状态改值    
	 * @param uid 用户id
	 * @param value 值
	 * @param type 操作类型
	 * date 2019年4月12日
	 */
	void updateRoleStateById(@Param("id") String uid,@Param("value") boolean value,@Param("type") String type);
	
	/**
	 * Description: 增加用户信息
	 * @param user 
	 * date 2019年4月10日
	 */
	Integer addRole(SysRole role);
	
	/**
	 * Description: 保存角色与权限的关联关系   
	 * @param permId
	 * @param roleId 
	 * date 2019年4月13日
	 */
	void addRolePermRef(@Param("permId") String permId,@Param("roleId") Integer roleId);
	
	/**
	 * Description:  更新角色数据  
	 * @param role 
	 * date 2019年4月13日
	 */
	void updateRole(SysRole role);
	
	/**
	 * Description: 删除角色权限关系 
	 * @param permId 权限id
	 * @param roleId 角色id
	 * date 2019年4月13日
	 */
	void delRolePermRefByRidAndPid(@Param("permId") String permId,@Param("roleId") Integer roleId);
	
	/**
	 * 
	 * Description: 根据id获取角色 
	 * @param ids
	 * @return 
	 * date 2019年9月11日
	 */
	List<SysRole> selectRoleByids(@Param("ids") List<String> ids);
	
	/**
	 * 
	 * Description: 根据角色名称获取角色 
	 * @param roleName
	 * @return 
	 * date 2019年9月11日
	 */
	SysRole selectByName(String roleName);
	
	/**
	 * 
	 * Description: 根据用户ID获取角色 
	 * @param userId
	 * @return 
	 * date 2019年9月11日
	 */
	List<SysRole> selectByUserId(Integer userId);
	
}