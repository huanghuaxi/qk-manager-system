package com.qk.manager.security.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.qk.manager.security.entity.SiteKv;

/**
 * 
 * <p>Description: 加载站点键值配置类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年4月17日   
 * @version 1.0
 */

@Mapper
public interface SiteKvMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(SiteKv record);

    SiteKv selectByPrimaryKey(Integer id);

    List<SiteKv> selectAll();

    int updateByPrimaryKey(SiteKv record);
    
    /**
     * 
     * Description: 查询所有站点键值配置    
     * @param siteKv
     * @param start
     * @param pageSize
     * @return 
     * date 2019年4月17日
     */
    List<SiteKv> querySiteKv(SiteKv siteKv, Integer start, Integer pageSize);
    
    /**
     * 
     * Description: 统计所有站点键值配置
     * @param siteKv
     * @return 
     * date 2019年4月17日
     */
    Integer siteKvCount(SiteKv siteKv);
    
    /**
     * 
     * Description: 删除站点键值配置   
     * @param ids 
     * date 2019年4月17日
     */
    void delSiteKv(@Param("ids") List<String> ids);
    
    /**
     * 
     * Description: 根据键值英文名查询   
     * @param enName
     * @return 
     * date 2019年4月17日
     */
    SiteKv selectByEnName(String enName);
    
    /**
     * 
     * Description: 根据键ID统计域名键值绑定    
     * @param ids
     * @return 
     * date 2019年4月17日
     */
    Integer siteDomainKvByIdsCount(@Param("ids") List<String> ids);
}