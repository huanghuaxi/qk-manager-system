package com.qk.manager.security.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.qk.manager.security.entity.Menu;
import com.qk.manager.security.entity.SysRole;

/**
 * Description:  获取菜单类 
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年3月31日   
 * @version 1.0
 */
@Mapper
public interface MenuMapper {
	
	/**
	 * Description:获取菜单方法
	 * @date 2019年3月31日    
	 * @param parentId 父节点
	 * @param roleLst 角色列表
	 * @return 获取子菜单
	 */
	List<Menu> selectMenu(Integer parentId,List<SysRole> roleLst);

	/**
	 * Description: 获取所有有效菜单  
	 * @param parentId 父节点
	 * @return 
	 * date 2019年4月13日
	 */
	List<Menu> selectAllMenu(Integer parentId);
	
	/**
	 * Description: 根据父节点，角色id查询权限    
	 * @param parentId
	 * @param roleId
	 * @return 
	 * date 2019年4月13日
	 */
	List<Menu> selectAllMenuByRoleId(Integer parentId,@Param("roleId") String roleId);
	
}
