package com.qk.manager.taskinfo.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.DataUtil;
import com.qk.manager.device.entity.Device;
import com.qk.manager.device.service.DeviceService;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.taskintf.DataExtend;
import com.qk.manager.websocket.server.WebSocketServer;

/**
 * <p>Description: 获取拉好友任务</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月31日   
 * @version 1.0
 */
@Service
public class PullFriendToGroupDataIntfImpl implements DataExtend {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DeviceService deviceService;
	
	@Override
	public void operate(String deviceNum, String account, Task task) {
		if(task == null) {
			return ;
		}
		JSONObject jsonObject = setPulData(task);
		Device device = deviceService.selectByDeviceNum(deviceNum);
		String msg = "";
		if(jsonObject != null) {
			msg = DataUtil.setBaseData(CodeUtil.SUCCESS_CODE, "获取任务成功", 1, 1, jsonObject);
			try {
				logger.info("向客户端发送添加好友任务");
				WebSocketServer.sendInfo(msg, device.getUserId() + "&" + deviceNum + account);
			} catch (IOException e) {
				logger.error("向客户端发送消息失败", e);
			}
		}
	}
	
	
	/**
	 * 
	 * Description: 获取添加好友数据    
	 * @param task
	 * @param account
	 * @return 
	 * date 2019年7月26日
	 */
	private JSONObject setPulData(Task task) {
		JSONObject jsonObject = new JSONObject();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("taskId", task.getId());
		dataMap.put("taskType", task.getTaskType());
		Map<String, String> itemMap = task.getItemMap();
		Map<String, String> pullToGroupMap = new HashMap<String, String>();
	    pullToGroupMap.put("type", itemMap.get("type"));
	    pullToGroupMap.put("groupName", itemMap.get("groupName"));
	    pullToGroupMap.put("invite", itemMap.get("invite"));
	    pullToGroupMap.put("noInvite", itemMap.get("noInvite"));
	    pullToGroupMap.put("memberNum", itemMap.get("memberNum"));
		dataMap.put("pullToGroupList", pullToGroupMap);
		jsonObject.putAll(dataMap);
		return jsonObject;
	}

}
