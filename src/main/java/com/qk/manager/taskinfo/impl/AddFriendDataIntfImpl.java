/**
 * 
 */
/**    
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月30日   
 * @version 1.0   
 */
package com.qk.manager.taskinfo.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.qk.manager.accountdata.entity.AccountData;
import com.qk.manager.accountdata.mapper.AccountDataMapper;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.DataUtil;
import com.qk.manager.common.DateFormateUtil;
import com.qk.manager.device.entity.Device;
import com.qk.manager.device.service.DeviceService;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.mapper.TaskMapper;
import com.qk.manager.task.service.TaskService;
import com.qk.manager.taskintf.DataExtend;
import com.qk.manager.websocket.server.WebSocketServer;

/**
 * <p>Description: 获取添加好友任务 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月31日   
 * @version 1.0
 */
@Service
public class AddFriendDataIntfImpl implements DataExtend {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TaskMapper taskMapper;
	
	@Autowired
	private AccountDataMapper accountDataMapper;
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private DeviceService deviceService;

	@Override
	public void operate(String deviceNum, String account, Task task) {
		
		if(task == null) {
			return ;
		}
		
		JSONObject jsonObject = setAddFriendData(task, account, deviceNum);
		Device device = deviceService.selectByDeviceNum(deviceNum);
		String msg = "";
		if(jsonObject != null) {
			msg = DataUtil.setBaseData(CodeUtil.SUCCESS_CODE, "获取任务成功", 1, 1, jsonObject);
			try {
				logger.info("向客户端发送添加好友任务");
				WebSocketServer.sendInfo(msg, device.getUserId() + "&" + deviceNum + account);
			} catch (IOException e) {
				logger.error("向客户端发送消息失败", e);
			}
		}
	}
	
	
	/**
	 * 
	 * Description: 获取添加好友数据    
	 * @param task
	 * @param account
	 * @return 
	 * date 2019年7月26日
	 */
	private JSONObject setAddFriendData(Task task, String account, String deviceNum) {
		JSONObject jsonObject = new JSONObject();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("taskId", task.getId());
		map.put("taskType", task.getTaskType());
		Map<String, String> itemMap = new HashMap<String, String>();
		for(Item item : task.getItemList()) {
			itemMap.put(item.getItemName(), item.getItemValue());
		}
		//获取分配的好友
		List<AccountData> aDataList = accountDataMapper.selectByAccountBylimit(account + "&" + task.getId(),new Random().nextInt(2));
		if(aDataList == null || aDataList.size() == 0) {
			//没有要添加的好友，修改任务状态为已完成
			taskService.editDeviceTaskState(deviceNum, task.getId(), 1);
			return null;
		} 
		
		List<Map<String, String>> friendList = new ArrayList<Map<String,String>>();
		Map<String, String> friendMap = new HashMap<String, String>();
		int type = Integer.parseInt(itemMap.get("type").trim());
		int applyType = Integer.parseInt(itemMap.get("applyType").trim());
		String[] applyTexts = itemMap.get("applyText").split("\\\n");
		//延迟时间
		Integer minTime = Integer.parseInt(itemMap.get("minTime") == null ? "0" : itemMap.get("minTime").trim());
		Integer maxTime = Integer.parseInt(itemMap.get("maxTime") == null ? "0" : itemMap.get("maxTime").trim());
		String date = DateFormateUtil.getDateStr(new Date(), DateFormateUtil.YYYYMMDD);
		Random random = new Random();              
		for(AccountData data : aDataList) {
			friendMap = new HashMap<String, String>();
			friendMap.put("account", data.getAccountName());
			Integer time = (random.nextInt(maxTime - minTime + 1) + minTime) * 1000;
			friendMap.put("sleepTime", time + "");
			//备注类型
			if(type == 0) {
				friendMap.put("remarkName", "");
			}else if(type == 1) {
				friendMap.put("remarkName", date + "_" + data.getId());
			}else if(type == 2){
				friendMap.put("remarkName", data.getRemarkName());
			}
			//申请语类型
			if(applyType == 0) {
				friendMap.put("applyText", applyTexts[random.nextInt(applyTexts.length)]);
			}else {
				friendMap.put("applyText", data.getApplicationTerms());
			}
			friendList.add(friendMap);
		}
		map.put("friendList", friendList);
		
		jsonObject.putAll(map);
		return jsonObject;
	}
}