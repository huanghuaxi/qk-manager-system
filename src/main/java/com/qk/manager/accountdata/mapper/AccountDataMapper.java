package com.qk.manager.accountdata.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.qk.manager.accountdata.entity.AccountData;

/**
 * <p>Description: 好友库mapper </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月24日   
 * @version 1.0
 */
@Mapper
public interface AccountDataMapper {
    /**
     * Description: 根据主键删除    
     * @param id
     * @return 
     * date 2019年7月24日
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * Description:插入好友库数据    
     * @param record
     * @return 
     * date 2019年7月24日
     */
    int insert(AccountData record);

    /**
     * Description:根据主键获取数据    
     * @param id
     * @return 
     * date 2019年7月24日
     */
    AccountData selectByPrimaryKey(Integer id);

    /**
     * Description:筛选所有数据    
     * @return 
     * date 2019年7月24日
     */
    List<AccountData> selectAll();

    /**
     * Description: 根据主键跟新数据
     * @param record
     * @return 
     * date 2019年7月24日
     */
    int updateByPrimaryKey(AccountData record);
    
    /**
     * Description: 查找好友库    
     * @param accountData
     * @param start
     * @param pageSize
     * @return 
     * date 2019年7月25日
     */
    List<AccountData> selectByPage(@Param("accountData") AccountData accountData, Integer start, Integer pageSize
    		, @Param("userIds") List<Integer> userIds);
    
    /**
     * Description: 统计好友库   
     * @param accountData
     * @return 
     * date 2019年7月25日
     */
    Integer countAccountData(@Param("accountData") AccountData accountData, @Param("userIds") List<Integer> userIds);
    
    /**
     * Description:批量插入好友库 
     * @param list 
     * date 2019年7月26日
     */
    void insertBatch(@Param("list") List<AccountData> list);
    
    /**
     * Description: 批量删除好友库
     * @param list id集合
     * date 2019年7月26日
     */
    void delByBatch(@Param("list") List<String> list);
    

    

    /**
     * 
     * Description: 获取可以添加好友    
     * @return 
     * date 2019年7月25日
     */
    List<AccountData> selectMayAdd(@Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 统计可以添加好友数量    
     * @return 
     * date 2019年7月25日
     */
    Integer countMayAdd(@Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 根据账号获取好友    
     * @param account
     * @param limit
     * @return 
     * date 2019年7月26日
     */
    List<AccountData> selectByAccount(String account);
    
    /**
     * Description: 根据账号获取好友（数量限制） 
     * @param account
     * @param limit
     * @return 
     * date 2019年9月4日
     */
    List<AccountData> selectByAccountBylimit(String account,int limit);
    
    
    /**
     * 
     * Description: 修改好友状态
     * @param accountName
     * @param targetAccount 
     * @param state 
     * date 2019年7月30日
     */
    void updateByAccount(String accountName, String targetAccount, Integer state);
    
    /**
     * Description: 统计状态值]
     * @param accountData 账号对象
     * @return 
     * date 2019年10月11日
     */
    List<Map<String, Object>> weChatStatitic(AccountData accountData,List<Integer> userIds);
    
    /**
     * Description: 获取导出qq内容   
     * @param accountData 
     * @param userIds
     * @return 
     * date 2019年9月25日
     */
    List<AccountData> exportData(@Param("accountData") AccountData accountData,@Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 根据目标账号和状态查询
     * @param account
     * @return 
     * date 2019年10月11日
     */
    List<AccountData> selectByAccountAndState(String account);
    
}