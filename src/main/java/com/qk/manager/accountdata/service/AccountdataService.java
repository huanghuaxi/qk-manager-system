package com.qk.manager.accountdata.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.qk.manager.accountdata.entity.AccountData;
import com.qk.manager.qqfriend.entity.QQAccountData;

/**
 * <p>Description: 好友库 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月26日   
 * @version 1.0
 */
public interface AccountdataService {
	
	/**
     * Description: 查找好友库    
     * @param accountData
     * @param start
     * @param pageSize
     * @return 
     * date 2019年7月25日
     */
    List<AccountData> selectByPage(AccountData accountData, Integer start, Integer pageSize);
    
    /**
     * Description: 统计好友库   
     * @param accountData
     * @return 
     * date 2019年7月25日
     */
    Integer countAccountData(AccountData accountData);
    
    /**
     * Description:批量插入    
     * @param list 
     * date 2019年7月26日
     */
    void insertBatch(List<AccountData> list);
    
    /**
     * Description: 批量删除好友库
     * @param list id集合
     * date 2019年7月26日
     */
    void delByBatch(@Param("list") List<String> list);
    
    /**
     * Description: 根据主键跟新数据
     * @param record
     * @return 
     * date 2019年7月24日
     */
    int updateByPrimaryKey(AccountData record);
    

    
    /**
     * 
     * Description: 统计可以添加好友数量    
     * @return 
     * date 2019年7月25日
     */
    Integer countMayAdd(Integer userId);
    
    /**
     * 
     * Description: 修改好友状态
     * @param accountName
     * @param targetAccount 
     * @param state 
     * date 2019年7月30日
     */
    void updateByAccount(String accountName, String targetAccount, Integer state);

    /**
     * 
     * Description: 根据账号获取未添加好友  
     * @param account
     * @return 
     * date 2019年9月4日
     */
    
    List<AccountData> selectByAccount(String account);
    
    /**
     * 
     * Description:    
     * @param accountData
     * @return 
     * date 2019年10月11日
     */
	List<Map<String, Object>> wechatStatitic(AccountData accountData);
	
	/**
	 * Description: 导出excell   
	 * @param accountData
	 * @return 
	 * date 2019年10月11日
	 */
	public HSSFWorkbook expordExcel(AccountData accountData);
	
}
