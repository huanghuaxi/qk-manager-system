package com.qk.manager.accountdata.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.qk.manager.accountdata.entity.AccountData;
import com.qk.manager.accountdata.mapper.AccountDataMapper;
import com.qk.manager.accountdata.service.AccountdataService;
import com.qk.manager.security.service.UserService;

@Service
public class AccountdataServiceImpl implements AccountdataService {
	
	@Autowired
	private AccountDataMapper accountDataMapper;
	
	@Autowired
	private UserService userService;

	@Override
	public List<AccountData> selectByPage(AccountData accountData, Integer start, Integer pageSize) {
		List<Integer> userIds = userService.getRoleUser();
 		return accountDataMapper.selectByPage(accountData, start, pageSize, userIds);
	}

	@Override
	public Integer countAccountData(AccountData accountData) {
		List<Integer> userIds = userService.getRoleUser();
		return accountDataMapper.countAccountData(accountData, userIds);
	}

	@Override
	@Transactional
	public void insertBatch(List<AccountData> list) {
        accountDataMapper.insertBatch(list);		
	}

	@Override
	public void delByBatch(List<String> list) {
		accountDataMapper.delByBatch(list);
	}

	@Override
	public int updateByPrimaryKey(AccountData record) {
		AccountData r = accountDataMapper.selectByPrimaryKey(record.getId());
		record.setUserId(r.getUserId());
		return accountDataMapper.updateByPrimaryKey(record);
	}

	@Override
	public Integer countMayAdd(Integer userId) {
		List<Integer> userIds = userService.getRoleUser();
		return accountDataMapper.countMayAdd(userIds);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: updateByAccount</p>   
	 * <p>Description: 修改好友状态</p>   
	 * @param accountName
	 * @param accountName
	 * @param targetAccount   
	 * @see com.qk.manager.accountdata.service.AccountdataService#updateByAccount(java.lang.String, java.lang.String)
	 */
	@Override
	public void updateByAccount(String accountName, String targetAccount, Integer state) {
		accountDataMapper.updateByAccount(accountName, targetAccount, state);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByAccount</p>   
	 * <p>Description: 根据账号获取未添加好友 </p>   
	 * @param account
	 * @return   
	 * @see com.qk.manager.accountdata.service.AccountdataService#selectByAccount(java.lang.String)
	 */
	@Override
	public List<AccountData> selectByAccount(String account) {
		return accountDataMapper.selectByAccount(account);
	}
	
	@Override
	public List<Map<String, Object>> wechatStatitic(AccountData accountData) {
		List<Integer> userIds = userService.getRoleUser();
		return accountDataMapper.weChatStatitic(accountData,userIds);
	}

	
	@Override
	public HSSFWorkbook expordExcel(AccountData accountData) {
		//查询中奖纪录
		List<Integer> userIds = userService.getRoleUser();
		List<AccountData> qqList = accountDataMapper.exportData(accountData, userIds);
		// 声明一个工作薄        
        HSSFWorkbook workbook = new HSSFWorkbook();
        //创建一个Excel表单,参数为sheet的名字
        HSSFSheet sheet = workbook.createSheet("微信数据");
        //创建表头
        setTitle(workbook, sheet);
        //新增数据行，并且设置单元格数据
        HSSFRow hssfRow = sheet.createRow(1);
        int rows = 1;
        for(AccountData qcd: qqList) {
        	hssfRow = sheet.createRow(rows);
        	hssfRow.createCell(0).setCellValue(qcd.getAccountName());
    		hssfRow.createCell(1).setCellValue(qcd.getRemarkName());
    		String val = "";
    		if( qcd.getTargetAccount()!=null&&!"".equals(qcd.getTargetAccount())) {
    		   val = qcd.getTargetAccount().split("&")[0];	
    		}
    		hssfRow.createCell(2).setCellValue(val);
        	rows++;
        }
		return workbook;
	}
	
	/**
	 * Description:  创建表头  
	 * @param workbook
	 * @param sheet 
	 * date 2019年10月11日
	 */
    private static void setTitle(HSSFWorkbook workbook, HSSFSheet sheet) {
        HSSFRow row = sheet.createRow(0);
        // 设置列宽，setColumnWidth的第二个参数要乘以256，这个参数的单位是1/256个字符宽度
        sheet.setColumnWidth(8, 60 * 256);
        // 设置为居中加粗
        HSSFCellStyle style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        //导出的Excel头部
        String[] headers = { "好友微信号", "备注名称", "添加到目标微信号"};
        // 设置表格默认列宽度为20个字节
        sheet.setDefaultColumnWidth((short) 20);
        for (short i = 0; i < headers.length; i++) {
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
            cell.setCellStyle(style);
        }
    }

	
}
