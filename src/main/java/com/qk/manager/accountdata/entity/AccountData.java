package com.qk.manager.accountdata.entity;

import java.util.Date;

/**
 * <p>Description: 好友库数据 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月24日   
 * @version 1.0
 */
public class AccountData {
	
    /**
     * 主键
     */
    private Integer id;

    /**
     * 账号名称
     */
    private String accountName;

    /**
     * 备注名称
     */
    private String remarkName;

    /**
     * 状态值
     */
    private Integer state;

    /**
     * 结果
     */
    private String result;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;
    
    /**
     * 目标账户
     */
    private String targetAccount;
    
    /**
     * 申请用语
     */
    private String applicationTerms;
    
	/**
     * 	管理员ID
     */
    private Integer userId;
    
    /**
     * 创建开始时间
     */
	private String startCTime;
    
	/**
     * 创建结束时间
     */
    private String endCTime;
    
    /**
     * 更新开始时间
     */
    private String startUTime;
    
    /**
     * 更新结束时间
     */
    private String endUTime;
    

    public String getApplicationTerms() {
		return applicationTerms;
	}

	public void setApplicationTerms(String applicationTerms) {
		this.applicationTerms = applicationTerms;
	}

	public String getTargetAccount() {
		return targetAccount;
	}

	public void setTargetAccount(String targetAccount) {
		this.targetAccount = targetAccount;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName == null ? null : accountName.trim();
    }

    public String getRemarkName() {
        return remarkName;
    }

    public void setRemarkName(String remarkName) {
        this.remarkName = remarkName == null ? null : remarkName.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result == null ? null : result.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public String getStartCTime() {
		return startCTime;
	}

	public void setStartCTime(String startCTime) {
		this.startCTime = startCTime;
	}

	public String getEndCTime() {
		return endCTime;
	}

	public void setEndCTime(String endCTime) {
		this.endCTime = endCTime;
	}

	public String getStartUTime() {
		return startUTime;
	}

	public void setStartUTime(String startUTime) {
		this.startUTime = startUTime;
	}

	public String getEndUTime() {
		return endUTime;
	}

	public void setEndUTime(String endUTime) {
		this.endUTime = endUTime;
	}
}