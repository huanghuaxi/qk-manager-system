package com.qk.manager.accountdata.controller;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qk.manager.accountdata.entity.AccountData;
import com.qk.manager.accountdata.service.AccountdataService;
import com.qk.manager.business.controller.PageUtil;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.CurrentParamUtil;
import com.qk.manager.common.ResultMessage;
import com.qk.manager.enums.AccountDataState;
import com.qk.manager.qqfriend.entity.QQAccountData;
import com.qk.manager.security.entity.SysUser;

@Controller
@RequestMapping("/accountdata")
public class AccountDataController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AccountdataService accountdataService;
	
	@ResponseBody
	@RequestMapping("/getlst")
	public PageUtil<AccountData> getTaskLst(PageUtil<AccountData> pageUtil,AccountData accountData) {
		List<AccountData> lst = accountdataService.selectByPage(accountData,  pageUtil.getCurrIndex(), pageUtil.getLimit());
		int count = accountdataService.countAccountData(accountData);
 		pageUtil.setCode("0");
		pageUtil.setCount(count);
		pageUtil.setMsg("成功");
		pageUtil.setData(lst);
		logger.info("获取任务列表");
		return pageUtil;
	}
	
	@RequestMapping("/accountlst")
	public String getList() {
		return "accountdata/accountdata.html";
	}
	
	
	@GetMapping("/toAdd")
	public String toAdd() {
		return "accountdata/add.html";
	}
	
	/**
	 * 
	 * Description: 保存 
	 * @param info
	 * @return 
	 * date 2019年7月26日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage save(String info) {
		try {
			List<AccountData> lst = new ArrayList<AccountData>();
			String[] infoArr = info.split("\n");
			SysUser user = CurrentParamUtil.getCurrentUser();
			for(String strs:infoArr) {
				AccountData accountData = new AccountData();
				String[]  str = strs.split("\\*");
				for(int i=0;i<str.length;i++) {
					if(i==0) {
					  accountData.setAccountName(str[i]);
					}
					if(i==1) {
					  accountData.setRemarkName(str[i]);	
					}
					if(i==2) {
					  accountData.setApplicationTerms(str[i]);
					}
				}
				accountData.setCreateTime(new Date());
				accountData.setState(AccountDataState.NEW_CREATE.getCode());
				accountData.setUserId(user.getId());
				lst.add(accountData);
			}    
			accountdataService.insertBatch(lst);
		}catch(Exception e) {
			logger.error("增加好友库失败", e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * Description:  删除  
	 * @param ids
	 * @return 
	 * date 2019年7月26日
	 */
	@ResponseBody
	@RequestMapping("/del")
	public ResultMessage del(String ids) {
		try {
			ids = ids.substring(0, ids.length()-1);
			List<String> idLst = Arrays.asList(ids.split(","));
			accountdataService.delByBatch(idLst);
		}catch(Exception e) {
			logger.error("删除好友库失败", e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 重定向新增页面
	 * @return
	 */
	@GetMapping("/redirect")
	public String addRedirect(String id,String accountName,String remarkName,String applicationTerms,String state,Model model) {
		model.addAttribute("id", id);
		model.addAttribute("accountName", accountName);
		model.addAttribute("applicationTerms", applicationTerms);
		model.addAttribute("state", state);
		model.addAttribute("remarkName",remarkName);
		return "accountdata/edit.html";
	}
	
	/**
	 * Description:  编辑 
	 * @param accountData
	 * @return 
	 * date 2019年7月26日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(AccountData accountData) {
		try {
			accountData.setUpdateTime(new Date());
			accountdataService.updateByPrimaryKey(accountData);
		}catch(Exception e) {
			logger.error("修改好友库失败", e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	

	/**
	 * 
	 * Description: 获取可以添加好友数量    
	 * @return 
	 * date 2019年7月25日
	 */
	@ResponseBody
	@RequestMapping("/getMayAddCount")
	public ResultMessage getMayAddCount() {
		try {
			SysUser user = CurrentParamUtil.getCurrentUser();
			Integer count = accountdataService.countMayAdd(user.getId());
			logger.info("获取可以添加好友数量");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, count);
		}catch (Exception e) {
			logger.error("获取可以添加好友数量失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * Description: 获取统计数据   
	 * @param state
	 * @param startCTime
	 * @param endCTime
	 * @param startUTime
	 * @param endUTime
	 * @param account
	 * @return 
	 * date 2019年9月24日
	 */
	@ResponseBody
	@RequestMapping("/wechatStatistic")
	public ResultMessage wechatStatistic(AccountData accountData) {
	  List<Map<String, 	Object>> list = accountdataService.wechatStatitic(accountData);
	  Long[] array = {0L,0L,0L,0L,0L,0L};
	  for(Map<String,Object> map: list) {
		  int key = Integer.valueOf(map.get("state").toString());
		  long count = Long.valueOf(map.get("count").toString());	
		  if(1 == key) {
			 array[0] = count;
		  }
		  
		  if(2 == key) {
			 array[1] = count;
		  }
		  
		  if(3 == key) {
			 array[2] = count;
		  }
		  
		  if(4 == key) {
			 array[3] = count;
		  }
		  
		  if(5 == key) {
			 array[4] = count;
		  }
		  
		  if(6 == key) {
			 array[5] = count;
		  }
	  }
	  return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, array);
	} 
	
	/**
	 * 
	 * Description: 导出中奖纪录    
	 * @param record
	 * @param response 
	 * date 2019年4月12日
	 */
	@RequestMapping("/expordExcel")
	public void expordExcel(AccountData accountData, HttpServletResponse response, HttpServletRequest request) {
		try {
			HSSFWorkbook workbook = accountdataService.expordExcel(accountData);
			String fileName = "微信添加好友统计数据" + new Date().getTime() + ".xls";
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {    
				fileName = URLEncoder.encode(fileName, "UTF-8");// IE浏览器    
			}else{    
				fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
			}
			//清空response  
            response.reset();  
            //设置response的Header  
            response.addHeader("Content-Disposition", "attachment;filename="+ fileName);  
            OutputStream os = new BufferedOutputStream(response.getOutputStream());  
            response.setContentType("application/vnd.ms-excel;charset=utf-8"); 
            //将excel写入到输出流中
            workbook.write(os);
            os.flush();
            os.close();
            logger.info("导出微信添加好友数据统计");
		}catch(Exception e) {
			logger.error("导出微信添加好友数据统计异常", e);
		}
	}
}
