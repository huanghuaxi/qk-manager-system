package com.qk.manager.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.qk.manager.common.Constants;

/**
 * Description: 静态资源路径访问映射
 * Copyright: Copyright (c) 2019    
 * @author binghe  
 * @date 2019年4月11日   
 * @version 1.0
 */
@SuppressWarnings("deprecation")
@Configuration  
public class WebMvcConfiguration extends WebMvcConfigurerAdapter {  
	
	@Value("${web.upload.path}")
    private String uploadPath;

	@Value("${web.upload.urlvisit.path}")
	private String urlVisitPath;
	
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        super.addResourceHandlers(registry);
        registry.addResourceHandler(urlVisitPath).addResourceLocations(
                Constants.SOURCE_FILE+uploadPath);
    }
	 
	 
}