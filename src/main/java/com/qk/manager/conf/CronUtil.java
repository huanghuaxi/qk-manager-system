package com.qk.manager.conf;

import java.util.Date;

import com.qk.manager.common.DateFormateUtil;

/**
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月30日   
 * @version 1.0
 */
public class CronUtil {
	
	/**
	 * Description:    
	 * @param month
	 * @param week
	 * @param dateTime
	 * @return 
	 * date 2019年7月30日
	 */
	@SuppressWarnings("deprecation")
	public static String getCron(String dateTime) {
		String corn ="";
		if(dateTime!=null && !"".equals(dateTime)) {
			Date da = DateFormateUtil.getDate(dateTime, DateFormateUtil.HH_MM_SS);
			
			int hour = da.getHours();
			int minutes = da.getMinutes();
			int seconds = da.getSeconds();
			corn = seconds + " " + minutes + " " + hour;
		}
		return corn;
	}
	
	public static String getDayCron(String cron) {
		return cron+" * * ?";
	}
	
	/**
	 * Description: 周循环corn表达式处理    
	 * @param corn
	 * @param week
	 * @return 
	 * date 2019年8月2日
	 */
	public static String getWeekCron(String corn,String week) {
		if(week!=null && !"".equals(week)) {
			corn = corn +" ? * "+ getWeek(week);
		}
		return corn;
	}
	
	/**
	 * Description: 周循环corn表达式处理    
	 * @param corn
	 * @param week
	 * @return 
	 * date 2019年8月2日
	 */
	public static String getMonthDayCron(String corn,String day) {
		if(day!=null && !"".equals(day)) {
			corn  = corn + " " + day+" * " + "?";
		} 
		return corn;
	}
	

	/**
	 * Description: 获取星期    
	 * @param week
	 * @return 
	 * date 2019年8月2日
	 */
	private static String getWeek(String week) {
		String weekEn ="";
		switch(week) {
		  case "1":
			  weekEn = "MON";
		    break;
		  case "2":
			  weekEn = "TUE";
			break;
		  case "3":
			  weekEn = "WED";
			break;
		  case "4":
			  weekEn = "THU";
			break;
		  case "5":
			  weekEn = "FRI";
			break;
		  case "6":
			  weekEn = "SAT";
			break;
		  case "7":
			  weekEn = "SUN";
			break;
		}
		return weekEn;
	}
	
	/**
	 * Description: 获取区间表达式    
	 * @return 
	 * date 2019年8月15日
	 */
	public static String betweenRegion(int startHour,int endHour, int rate) {
	    String cron = "0 0/" + rate + " "+startHour+"-"+endHour+" * * ?";
		return cron;
	}
}
