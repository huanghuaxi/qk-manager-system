package com.qk.manager.conf;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.qk.manager.security.filter.MyFilterSecurityInterceptor;
import com.qk.manager.security.service.impl.MyAuthenticationProvider;
import com.qk.manager.security.service.impl.MyUserDetailsService;
import com.qk.manager.security.validatecode.filter.ValidateCodeFilter;
import com.qk.manager.security.validatecode.service.CustomAuthenticationFailureHandler;
/**
 * Description:  security相关配置
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年4月3日   
 * @version 1.0
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Resource    
	private SessionRegistry sessionRegistry;  
	@Autowired
	private MyUserDetailsService myUserDetailsService;
	@Autowired
	private MyFilterSecurityInterceptor myFilterSecurityInterceptor;
	@Autowired
	private ValidateCodeFilter validateCodeFilter;
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// 允许所有用户访问登陆页
		http.csrf().disable().authorizeRequests().antMatchers("/image/code","/ruleVerify/*","/image/*","/login","/wechar/*","/websocket/*")
								.permitAll()
								.anyRequest()
								.authenticated() 
								.and()
								.formLogin()
								.loginPage("/login")
								.defaultSuccessUrl("/index")
								.failureHandler(customAuthenticationFailureHandler())
								.permitAll()
								.and()
								.logout()
								.logoutSuccessUrl("/login");
		http.headers().frameOptions().sameOrigin();
		//同一个用户，同时在线数只能有一个
        http.sessionManagement().maximumSessions(1).sessionRegistry(sessionRegistry).expiredUrl("/login");
		//http.addFilterBefore(domainInterceptor, UsernamePasswordAuthenticationFilter.class);
		http.addFilterBefore(validateCodeFilter, UsernamePasswordAuthenticationFilter.class);
		http.addFilterBefore(myFilterSecurityInterceptor, FilterSecurityInterceptor.class);
	}
	
	@Bean
    public MyAuthenticationProvider authenticationProvider() {
        MyAuthenticationProvider authenticationProvider = new MyAuthenticationProvider();
        authenticationProvider.setUserDetailsService(myUserDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }
	
   @Bean    
    public SessionRegistry sessionRegistry() {    
        return new SessionRegistryImpl();    
    }   

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
    public AuthenticationFailureHandler customAuthenticationFailureHandler() {
        return new CustomAuthenticationFailureHandler();
    }
	
	  /**
	   * 忽略静态资源
	   */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/activityPage/**", "/layui/**", "/uploads/**");
    }
	
	public static void main(String[] args) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		System.out.println(encoder.encode("1".trim()));
	}
}