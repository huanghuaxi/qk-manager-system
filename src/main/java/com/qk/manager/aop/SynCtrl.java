package com.qk.manager.aop;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * <p>Description: 日志注解 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年6月21日   
 * @version 1.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SynCtrl {
	
	  /**
	   * Description: 是否同步    
	   * @return 
	   * date 2019年9月4日
	   */
	  boolean syn() ;
	  
	  /**
	   * Description: 睡眠时间    
	   * @return 
	   * date 2019年9月4日
	   */
	  int sleep();
	  
	  /**
	   * Description: 是否等待
	   * @return 
	   * date 2019年9月4日
	   */
	  boolean isWait();
	
}
