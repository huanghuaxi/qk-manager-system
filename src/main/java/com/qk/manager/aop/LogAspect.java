package com.qk.manager.aop;

import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2019
 * </p>
 * 
 * @author huanghx
 * @date 2019年6月21日
 * @version 1.0
 */
@Aspect
@Component
public class LogAspect {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private volatile boolean isNotPorccess = true;
	
	private volatile Date date = null;

	@Before("execution(* com.qk.manager.quarz.service.impl.*.operate(..)) && @annotation(synCtrl)")
	public void beginTransaction(JoinPoint point, SynCtrl synCtrl) throws InterruptedException {
		/*
		 * //是否同步控制 boolean isSyn = synCtrl.syn(); if(!isSyn) { return ; }
		 * 
		 * //方法在执行中 if(!isNotPorccess) { if(synCtrl.isWait()) { throw new
		 * RuntimeException("有方法在执行中"); }
		 * 
		 * if(date != null) { long lastTime = date.getTime(); long currentTime =
		 * System.currentTimeMillis(); long subStr = currentTime - lastTime;
		 * if((subStr/1000)>=synCtrl.sleep()) { isNotPorccess = true; } else {
		 * Thread.sleep(synCtrl.sleep() * 1000); } } }
		 * 
		 * //锁住方法 synchronized(this){ if(isNotPorccess) { date = new Date();
		 * isNotPorccess = false; logger.info("方法再执行中"); } }
		 */
	}

	/**
	 * 前置通知 &&@annotation(com.system.manager.aop.LogRecord)
	 */

	@After("execution(* com.qk.manager.quarz.service.impl.*.operate(..)) && @annotation(synCtrl)")
	public void commit(JoinPoint point, SynCtrl synCtrl) {

	}

	@AfterReturning("execution(* com.qk.manager.quarz.service.impl.*.operate(..)) && @annotation(synCtrl)")
	public void afterReturning(JoinPoint joinPoint, SynCtrl synCtrl) {
		/*
		 * if(synCtrl.syn()) { isNotPorccess = true; logger.info("任务执行完成，退出方法"); }
		 */
	}

	@AfterThrowing("execution(* com.qk.manager.quarz.service.impl.*.operate(..)) && @annotation(synCtrl)")
	public void afterThrowing(SynCtrl synCtrl) {
		/*
		 * if(synCtrl.syn()) { isNotPorccess = false; logger.info("方法执行异常退出"); }
		 */
	}

	@Around("execution(* com.qk.manager.quarz.service.impl.*.operate(..)) && @annotation(synCtrl)")
	public Object around(ProceedingJoinPoint joinPoint, SynCtrl synCtrl) throws Throwable {
		try {
			System.out.println("around");
			return joinPoint.proceed();
		} catch (Throwable e) {
			throw e;
		} finally {
			
		}
	}

}
