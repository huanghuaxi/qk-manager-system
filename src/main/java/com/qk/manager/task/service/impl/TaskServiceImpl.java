package com.qk.manager.task.service.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.alibaba.fastjson.JSONObject;
import com.qk.manager.accountdata.entity.AccountData;
import com.qk.manager.accountdata.mapper.AccountDataMapper;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.CurrentParamUtil;
import com.qk.manager.common.DataUtil;
import com.qk.manager.common.DateFormateUtil;
import com.qk.manager.common.SpringBeanFactory;
import com.qk.manager.conf.CronUtil;
import com.qk.manager.device.entity.Account;
import com.qk.manager.device.entity.Device;
import com.qk.manager.device.mapper.AccountMapper;
import com.qk.manager.device.mapper.DeviceMapper;
import com.qk.manager.device.service.DeviceService;
import com.qk.manager.enums.CircleType;
import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.qqfriend.entity.QQAccountData;
import com.qk.manager.qqfriend.mapper.QQAccountDataMapper;
import com.qk.manager.quarz.entity.TaskInfo;
import com.qk.manager.quarz.service.ScheduleService;
import com.qk.manager.security.entity.SysUser;
import com.qk.manager.security.service.UserService;
import com.qk.manager.task.entity.DeviceTask;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.mapper.DeviceTaskMapper;
import com.qk.manager.task.mapper.ItemMapper;
import com.qk.manager.task.mapper.TaskMapper;
import com.qk.manager.task.service.TaskService;
import com.qk.manager.taskintf.DataExtend;
import com.qk.manager.websocket.server.WebSocketServer;

/**
 * 
 * <p>Description: 任务更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */

@Service
public class TaskServiceImpl implements TaskService{

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TaskMapper taskMapper;
	
	@Autowired
	private ItemMapper itemMapper;
	
	@Autowired
	private ScheduleService scheduleService;
	
	@Autowired
	private AccountDataMapper accountDataMapper;
	
	@Autowired
	private QQAccountDataMapper qqAccountDataMapper;
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Autowired
	private DeviceMapper deviceMapper;
	
	@Autowired
	private DeviceTaskMapper deviceTaskMapper;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private DeviceService deviceService;
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByPage</p>   
	 * <p>Description: 分页查询</p>   
	 * @param task
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.qk.manager.task.service.TaskService#selectByPage(com.qk.manager.task.entity.Task, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Task> selectByPage(Task task, Integer start, Integer pageSize) {
		List<Integer> userIds = userService.getRoleUser();
		List<Task> lst = taskMapper.selectByPage(task, start, pageSize, userIds);
		for(Task t:lst) {
			List<Item> itemlst = itemMapper.selectByTaskId(t.getId());
			t.setItemList(itemlst);
			Map<String,String> map = t.getItemMap();
		    String timeTypeValue = map.get("timeType");
		    String circleTypeValue = map.get("circleType");
            String day = map.get("day");
            if("null".equals(day)) {
            	day = "";
            }
            if("1".equals(timeTypeValue)) {
        	   String processTime = DateFormateUtil.getDateStr(t.getProcessTime(), DateFormateUtil.HH_MM_SS);
        	   t.setProcessTimeStr(CircleType.getNameByCode(Integer.valueOf(circleTypeValue)) +" "+day+("".equals(day)?"":"号")+" "+processTime );
           }else {
        	   t.setProcessTimeStr(DateFormateUtil.getDateStr(t.getProcessTime(), DateFormateUtil.YYYY_MM_DD_HH_mm_ss));
           }
		}
		return lst;
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: countTask</p>   
	 * <p>Description: 统计数量</p>   
	 * @param task
	 * @return   
	 * @see com.qk.manager.task.service.TaskService#countTask(com.qk.manager.task.entity.Task)
	 */
	@Override
	public Integer countTask(Task task) {
		List<Integer> userIds = userService.getRoleUser();
		return taskMapper.countTask(task, userIds);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: addTask</p>   
	 * <p>Description: 添加任务</p>   
	 * @param task   
	 * @see com.qk.manager.task.service.TaskService#addTask(com.qk.manager.task.entity.Task)
	 */
	@Override
	@Transactional
	public void addTask(Task task, String nameStr, String valueStr) {
		Assert.notNull(task.getTaskType(), "任务类型数据不存在");
		Assert.hasText(nameStr, "任务数据不存在");
		Assert.hasText(valueStr, "任务数据不存在");
		//新增任务
		if(task.getProcessTime() == null) {
			task.setProcessTime(new Date());
		}
		task.setState(0);
		task.setCreateTime(new Date());
		task.setUpdateTime(new Date());
		SysUser user = CurrentParamUtil.getCurrentUser();
		task.setUserId(user.getId());
		taskMapper.insert(task);
		//获取任务ID
		Task t = taskMapper.selectByNew();
		//添加任务项目
		String[] names = nameStr.trim().split(",", 50);
		String[] values = valueStr.trim().split("\\|", 50);
		Item item = new Item();
		Map<String, String> map = new HashMap<String, String>();
		List<Device> deviceList = null;
		for(int i = 0;i < names.length;i++) {
			if("devices".equals(names[i])) {
				String[] devices = values[i].trim().split(",");
				t.setDeviceInfo(devices.length + "台设备，共" + devices.length + "个账号");
				taskMapper.updateByPrimaryKey(t);
				//添加设备任务关系
				deviceList = deviceMapper.selectByIds(Arrays.asList(devices));
				
			}
			map.put(names[i].trim(), values[i].trim());
			item = new Item();
			item.setTaskId(t.getId());
			item.setItemValue(values[i].trim());
			item.setItemName(names[i].trim());
			itemMapper.insert(item);
		}
		
		DeviceTask deviceTask = new DeviceTask();
		if(deviceList != null) {
			for(Device device : deviceList) {
				deviceTask = new DeviceTask();
				deviceTask.setDeviceId(device.getId());
				deviceTask.setDeviceNum(device.getDeviceNum());
				deviceTask.setDeviceMark(device.getMark());
				deviceTask.setTaskId(t.getId());
				if(Integer.parseInt(map.get("timeType")) != 2 && deviceList != null) {
					deviceTask.setState(0);
				}else {
					deviceTask.setState(1);
				}
				deviceTaskMapper.insert(deviceTask);
			}
		}
		
		if(TaskTypeEnum.ADD_FRIEND.getCode().equals(t.getTaskType())) {
			//添加好友任务，分配好友
			distributeFriend(map, t.getId());
		}
		if(TaskTypeEnum.QQ_ADD_FRIEND.getCode().equals(t.getTaskType())) {
			//QQ添加好友任务，分配好友
			distributeQQFriend(map, t.getId());
		}
		
		TaskInfo taskInfo = new TaskInfo();
		taskInfo.setTaskId(t.getId());
		taskInfo.setTaskType(t.getTaskType());
		taskInfo.setJobName(TaskTypeEnum.getClsName(t.getTaskType()));
		taskInfo.setJobGroup(taskInfo.getTaskType()+"@"+taskInfo.getTaskId());
		//启用定时
		if(Integer.parseInt(map.get("timeType")) == 0) {
			//按日期
			taskInfo.setStartTime(t.getProcessTime());
			scheduleService.setSimpleTriggerJob(taskInfo);
		} else if(Integer.parseInt(map.get("timeType")) == 1) {
			//循环
			Integer circleType = Integer.parseInt(map.get("circleType"));
			SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
			String day = map.get("day")==null?"":map.get("day");
			String datetime = format.format(t.getProcessTime());
			if(circleType == 1) {
				//每天
				taskInfo.setCronExpression(CronUtil.getDayCron(CronUtil.getCron(datetime)));
			}else if(circleType == 2) {
				//每周
				taskInfo.setCronExpression(CronUtil.getWeekCron(CronUtil.getCron(datetime), day));
			}else if(circleType == 3) {
				//每周
				taskInfo.setCronExpression(CronUtil.getMonthDayCron(CronUtil.getCron(datetime), day));
			}
			scheduleService.addJob(taskInfo);
		} else if(Integer.parseInt(map.get("timeType")) == 2 || Integer.parseInt(map.get("timeType")) == 3){
			//时间区间
			String startTime = map.get("startTime");
			String endTime = map.get("endTime");
			Date sTime = DateFormateUtil.getDate(startTime, DateFormateUtil.HH_MM_SS);
			Date eTime = DateFormateUtil.getDate(endTime, DateFormateUtil.HH_MM_SS);
			int sH = sTime.getHours();
			int eH = eTime.getHours();
			String cron = CronUtil.betweenRegion(sH, eH, Integer.parseInt(map.get("taskRate")));
			taskInfo.setCronExpression(cron);
			scheduleService.addJob(taskInfo);
		}
		
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editTask</p>   
	 * <p>Description: 编辑任务</p>   
	 * @param task   
	 * @see com.qk.manager.task.service.TaskService#editTask(com.qk.manager.task.entity.Task)
	 */
	@Override
	public void editTask(Task task) {
		Assert.notNull(task.getId(), "任务ID数据不存在");
		Assert.notNull(task.getTaskType(), "任务类型数据不存在");
		Task t = taskMapper.selectByPrimaryKey(task.getId());
		Assert.notNull(t, "任务数据不存在");
		t.setProcessTime(task.getProcessTime());
		t.setState(task.getState());
		taskMapper.updateByPrimaryKey(t);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: delTask</p>   
	 * <p>Description: 删除任务</p>   
	 * @param id   
	 * @see com.qk.manager.task.service.TaskService#delTask(java.lang.Integer)
	 */
	@Override
	@Transactional
	public void delTask(Integer id) {
		Assert.notNull(id, "任务ID数据不存在");
		Task task = taskMapper.selectByPrimaryKey(id);
		//删除任务项目
		itemMapper.deleteByTaskId(id);
		//删除设备任务
		deviceTaskMapper.deleteByTaskId(id);
		//删除任务
		taskMapper.deleteByPrimaryKey(id);
		//暂停任务
		if("1".equals(task.getState()+"")) {
		   scheduleService.pauseJob(TaskTypeEnum.getClsName(task.getTaskType()), task.getTaskType()+"@"+task.getId());
		}
		scheduleService.deleteJob(TaskTypeEnum.getClsName(task.getTaskType()), task.getTaskType()+"@"+task.getId());
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByDevice</p>   
	 * <p>Description: 根据设备获取任务</p>   
	 * @param deviceNum   
	 * @param account   
	 * @see com.qk.manager.task.service.TaskService#selectByDevice(java.lang.Integer)
	 */
	@Override
	public void selectByDevice(String deviceNum, String account, Integer userId) {
		Assert.hasText(deviceNum, "设备号数据不存在");
		Assert.hasText(account, "微信账号不存在");
		//获取任务
		List<Task> taskList = taskMapper.selectByDevice(deviceNum);
		String msg = "";
		if(taskList == null || taskList.size() == 0) {
			//没有任务，回复客户端消息
			try {
				msg = DataUtil.setBaseData(CodeUtil.SUCCESS_CODE, "当前没有任务要执行", 0, 1, null);
				WebSocketServer.sendInfo("", userId + "&" + deviceNum + account);
			} catch (IOException e) {
				logger.error("向客户端发送消息失败", e);
			}
		}else {
			for(Task task : taskList) {
				//判断是否为拉人进群任务
				if(!task.getTaskType().equals(TaskTypeEnum.PULL_FRIEND_TO_GROUP.getCode())) {
					continue;
				}
				if(task.getState() != 1) {
					//任务不是执行中跳过此条
					continue;
				}
				//查看是否到执行时间
				if(task.getProcessTime().getTime() < new Date().getTime()) {
					Map<String, String> itemMap = task.getItemMap();
					if(Integer.parseInt(itemMap.get("timeType")) == 1) {
						//循环执行判断日期
						Integer circleType = Integer.parseInt(itemMap.get("circleType"));
						if(circleType == 2) {
							//按周
							Integer day = Integer.parseInt(itemMap.get("day"));
							Integer week = getWeekOfDate(new Date());
							if(day != week) {
								continue;
							}
						}else if(circleType == 3) {
							//按月
							Integer day = Integer.parseInt(itemMap.get("day"));
							Integer month = getDayOfDate(new Date());
							if(day != month) {
								continue;
							}
						}
					}
				}else {
					continue;
				}
				DataExtend intf = (DataExtend) SpringBeanFactory.getBean(TaskTypeEnum.getbeanNameByCode(task.getTaskType()));
				intf.operate(deviceNum, account, task);
			}	
		}	
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editDeviceTaskState</p>   
	 * <p>Description: 修改设备任务状态</p>   
	 * @param deviceNum
	 * @param taskId   
	 * @see com.qk.manager.task.service.TaskService#editDeviceTaskState(java.lang.String, java.lang.Integer)
	 */
	@Override
	public void editDeviceTaskState(String deviceNum, Integer taskId, Integer state) {
		Assert.hasText(deviceNum, "设备号数据不存在");
		Assert.notNull(taskId, "任务ID数据不存在");
		//查看任务是否为循环执行，如果不是则判断该任务下的设备是否全部完成
		List<Item> items = itemMapper.selectByTaskId(taskId);
		for(Item item : items) {
			if("timeType".equals(item.getItemName())) {
				if(Integer.parseInt(item.getItemValue()) != 2) {
					//单次执行
					//修改设备任务状态
					deviceTaskMapper.updateDeviceTaskState(deviceNum, taskId, state);
					Integer count = deviceTaskMapper.countState(taskId, 0);
					if(count == 0) {
						//全部设备完成任务
						taskMapper.updateState(taskId, 2);
					}
				}
				break;
			}
		}
	}
	
	
	/**
	 * 
	 * Description: 分配好友      
	 * @param map 
	 * date 2019年7月25日
	 */
	private void distributeFriend(Map<String, String> map, Integer taskId) {
		//分配数量
		int addNum = Integer.parseInt(map.get("addNum"));
		List<Integer> userIds = userService.getRoleUser();
		List<AccountData> aDatas = accountDataMapper.selectMayAdd(userIds);
		String devices = map.get("devices");
		List<Account> accounts = accountMapper.selectByDevice(Arrays.asList(devices.trim().split(",")));
		AccountData accountData = new AccountData();
		if(aDatas != null && accounts != null) {
			int j = 0;
			for(int i = 0;i < aDatas.size();i++) {
				if(i != 0 && i % addNum == 0) {
					j++;
				}
				if(j >= accounts.size()) {
					break;
				}
				accountData = aDatas.get(i);
				accountData.setState(2);
				accountData.setTargetAccount(accounts.get(j).getAccount() + "&" + taskId);
				accountDataMapper.updateByPrimaryKey(accountData);
			}
		}
	}
	
	/**
	 * 
	 * Description: 分配QQ好友 
	 * @param map
	 * @param taskId 
	 * date 2019年9月19日
	 */
	private void distributeQQFriend(Map<String, String> map, Integer taskId) {
		//分配数量
		int addNum = Integer.parseInt(map.get("addNum"));
		List<Integer> userIds = userService.getRoleUser();
		List<QQAccountData> aDatas = qqAccountDataMapper.selectMayAdd(userIds);
		String devices = map.get("devices");
		List<Device> deviceList = deviceMapper.selectByIds(Arrays.asList(devices.trim().split(",")));
		QQAccountData accountData = new QQAccountData();
		if(aDatas != null && deviceList != null) {
			int j = 0;
			for(int i = 0;i < aDatas.size();i++) {
				if(i != 0 && i % addNum == 0) {
					j++;
				}
				if(j >= deviceList.size()) {
					break;
				}
				accountData = aDatas.get(i);
				accountData.setState(2);
				accountData.setTargetDevice(deviceList.get(j).getDeviceNum() + "&" + taskId);
				qqAccountDataMapper.updateByPrimaryKey(accountData);
			}
		}
	}
	

	private JSONObject setGroupingData(Task task, String account) {
		JSONObject jsonObject = new JSONObject();
		if(task != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("taskId", task.getId());
			map.put("taskType", task.getTaskType());
			Map<String, String> itemMap = new HashMap<String, String>();
			for(Item item : task.getItemList()) {
				itemMap.put(item.getItemName(), item.getItemValue());
			}
			//群名
			String[] groupNames = itemMap.get("groupName").split("\\\n");
			//好友
		}
		return null;
	}

	/**
     * Description: 获取所有任务及item信息   
     * @param taskId
     * @return 
     * date 2019年7月31日
     */
	@Override
	public Task selectDataByTaskId(Integer taskId) {
		return taskMapper.selectDataByTaskId(taskId);
	}
	
	/**
	 * 
	 * Description: 获取星期
	 * @param date
	 * @return 
	 * date 2019年8月1日
	 */
	public int getWeekOfDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int w = cal.get(Calendar.DAY_OF_WEEK) + 1;
		return w;
	}
	
	/**
	 * 
	 * Description: 获取日期
	 * @param date
	 * @return 
	 * date 2019年8月1日
	 */
	public int getDayOfDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int m = cal.get(Calendar.DAY_OF_MONTH);
		return m;
	}

	/*
	 *(non-Javadoc)   
	 * <p>Title: updateStateReaById</p>   
	 * <p>Description: 更改状态 </p>   
	 * @param reason
	 * @param state
	 * @param taskId   
	 * @see com.qk.manager.task.service.TaskService#updateStateReaById(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void updateStateReaById(String reason, String state, Integer taskId) {
		taskMapper.updateStateReaById(reason,state,taskId);
	}

	/*
	 *(non-Javadoc)   
	 * <p>Title: operateCurrent</p>   
	 * <p>Description: 立即执行定时任务</p>   
	 * @param id   
	 * @see com.qk.manager.task.service.TaskService#operateCurrent(java.lang.String)
	 */
	@Override
	public void operateCurrent(String id) {
		Task t = selectDataByTaskId(Integer.valueOf(id));
		TaskInfo taskInfo = new TaskInfo();
		taskInfo.setStartTime(new Date());
		taskInfo.setTaskId(t.getId());
		taskInfo.setTaskType(t.getTaskType());
		taskInfo.setJobName(TaskTypeEnum.getClsName(t.getTaskType()));
		taskInfo.setJobGroup(taskInfo.getTaskType()+"@"+taskInfo.getTaskId()+""+Math.random());
		if(TaskTypeEnum.QQ_FRIEND_CHECK.getCode().equals(t.getTaskType())) {
			deviceService.updateDeviceTaskStateAll(t.getId(), 0);
		}
		scheduleService.setSimpleTriggerJob(taskInfo);
	}

	
	@Override
	@Transactional
	public void pauseAndStart(String id, String state) {
		Task task = selectDataByTaskId(Integer.valueOf(id));
		if("4".equals(state)) {
			scheduleService.resumeJob(TaskTypeEnum.getClsName(task.getTaskType()), task.getTaskType()+"@"+task.getId());
			updateStateReaById("开启定时", "1", task.getId());
		}
		if("1".equals(state) || "0".equals(state) || "3".equals(state) ) {
			scheduleService.pauseJob(TaskTypeEnum.getClsName(task.getTaskType()), task.getTaskType()+"@"+task.getId());
			updateStateReaById("定时任务暂停", "4", task.getId());
		}
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectById</p>   
	 * <p>Description: 根据ID获取任务</p>   
	 * @param taskId
	 * @return   
	 * @see com.qk.manager.task.service.TaskService#selectById(java.lang.Integer)
	 */
	@Override
	public Task selectById(Integer taskId) {
		Task task = taskMapper.selectByPrimaryKey(taskId);
		if(task != null) {
			List<Item> items = itemMapper.selectByTaskId(taskId);
			task.setItemList(items);
			task.setItemMap(task.getItemMap());
		}
		return task;
	}

}
