package com.qk.manager.task.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.qk.manager.task.entity.Task;

/**
 * 
 * <p>Description: 加载任务类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */
public interface TaskService {
	
	/**
	 * 
	 * Description: 分页查询    
	 * @param task
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年7月23日
	 */
	public List<Task> selectByPage(Task task, Integer start, Integer pageSize);
	
	/**
	 * 
	 * Description: 统计数量    
	 * @param task
	 * @return 
	 * date 2019年7月23日
	 */
	public Integer countTask(Task task);
	
	/**
	 * 
	 * Description: 添加任务    
	 * @param task 
	 * date 2019年7月23日
	 */
	public void addTask(Task task, String nameStr, String valueStr); 
	
	/**
	 * 
	 * Description: 编辑任务    
	 * @param task 
	 * date 2019年7月23日
	 */
	public void editTask(Task task);
	
	/**
	 * 
	 * Description: 删除任务    
	 * @param id 
	 * date 2019年7月23日
	 */
	public void delTask(Integer id);
	
	/**
	 * 
	 * Description: 根据设备获取任务      
	 * @param deviceNum
	 * @param account 
	 * date 2019年7月26日
	 */
	public void selectByDevice(String deviceNum, String account, Integer userId);
	
	/**
	 * 
	 * Description: 修改设备任务状态
	 * @param deviceNum
	 * @param taskId 
	 * date 2019年7月30日
	 */
	public void editDeviceTaskState(String deviceNum, Integer taskId, Integer state);
	
	/**
     * Description: 获取所有任务及item信息   
     * @param taskId
     * @return 
     * date 2019年7月31日
     */
    Task selectDataByTaskId(Integer taskId);
    
    /**
     * Description: 更改状态    
     * @param reason 
     * @param state
     * @param taskId 
     * date 2019年8月1日
     */
    void updateStateReaById(@Param("reason") String reason,@Param("state") String state,@Param("taskId") Integer taskId);
    
    /**
     * Description: 立即执行任务  
     * @param id 
     * date 2019年8月2日
     */
    void operateCurrent(String id);
    
    /**
     * Description: 暂停开启任务
     * @param id id
     * @param state 任务状态 
     * date 2019年8月22日
     */
    void pauseAndStart(String id,String state);
    
    /**
     * 
     * Description: 根据ID获取任务 
     * @param taskId
     * @return 
     * date 2019年9月4日
     */
    Task selectById(Integer taskId);
	
}
