package com.qk.manager.task.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;

import com.qk.manager.enums.TaskTypeEnum;

/**
 * 
 * <p>Description: 任务类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年7月23日   
 * @version 1.0
 */
public class Task {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	任务类型
     */
    private String taskType;
    
    /**
     * 任务类型名称
     */
    private String taskTypeName;

	/**
     *	设备信息
     */
    private String deviceInfo;

    /**
     *	执行时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date processTime;

    /**
     *	状态
     */
    private Integer state;

    /**
     *	创建时间
     */
    private Date createTime;
    
    /**
     * 	项目
     */
    private List<Item> itemList = new ArrayList<Item>();
    
    /**
     * 项目map
     */
    private Map<String,String> itemMap = new HashMap<String,String>();
    
    /**
     * 执行时间
     */
    private String processTimeStr;
    
    /**
     * 更新时间
     */
    private Date updateTime;
    
    /**
     * 	管理员ID
     */
    private Integer userId;
    
    public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getProcessTimeStr() {
		return processTimeStr;
	}

	public void setProcessTimeStr(String processTimeStr) {
		this.processTimeStr = processTimeStr;
	}

    
    public Map<String, String> getItemMap() {
    	for(Item item:itemList) {
    		itemMap.put(item.getItemName(), item.getItemValue());
    	}
		return itemMap;
	}

	public void setItemMap(Map<String, String> itemMap) {
		this.itemMap = itemMap;
	}

	/**
     * 原因
     */
    private String reason;
    
    public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getTaskTypeName() {
		return taskTypeName;
	}

	public void setTaskTypeName(String taskTypeName) {
		this.taskTypeName = taskTypeName;
	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskTypeName = TaskTypeEnum.getTaskNameByCode(taskType);
        this.taskType = taskType;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo == null ? null : deviceInfo.trim();
    }

    public Date getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Date processTime) {
        this.processTime = processTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

	public List<Item> getItemList() {
		return itemList;
	}

	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}