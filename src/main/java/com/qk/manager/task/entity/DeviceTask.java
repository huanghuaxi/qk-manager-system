package com.qk.manager.task.entity;

import java.util.Date;

/**
 * 
 * <p>Description: 设备任务类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月26日   
 * @version 1.0
 */
public class DeviceTask {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	设备ID
     */
    private Integer deviceId;

    /**
     *	设备号
     */
    private String deviceNum;

    /**
     *	设备名称
     */
    private String deviceMark;

    /**
     *	任务ID
     */
    private Integer taskId;

    /**
     *	状态
     */
    private Integer state;

    /**
     *	完成时间
     */
    private Date finishTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceNum() {
        return deviceNum;
    }

    public void setDeviceNum(String deviceNum) {
        this.deviceNum = deviceNum == null ? null : deviceNum.trim();
    }

    public String getDeviceMark() {
        return deviceMark;
    }

    public void setDeviceMark(String deviceMark) {
        this.deviceMark = deviceMark == null ? null : deviceMark.trim();
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }
}