package com.qk.manager.task.entity;

/**
 * 
 * <p>Description: 任务项目类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author Administrator   
 * @date 2019年7月23日   
 * @version 1.0
 */
public class Item {
    /**
     *	ID
     */
    private Integer taskId;

    /**
     *	项目名称
     */
    private String itemName;

    /**
     *	项目数值
     */
    private String itemValue;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName == null ? null : itemName.trim();
    }

    public String getItemValue() {
        return itemValue;
    }

    public void setItemValue(String itemValue) {
        this.itemValue = itemValue == null ? null : itemValue.trim();
    }
}