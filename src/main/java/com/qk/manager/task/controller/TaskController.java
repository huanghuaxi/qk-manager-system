package com.qk.manager.task.controller;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.qk.manager.business.controller.PageUtil;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.FileTypeUtil;
import com.qk.manager.common.RatioCalculateUtil;
import com.qk.manager.common.ResultMessage;
import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.service.TaskService;

/**
 * 
 * <p>Description: 任务控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */

@Controller
@RequestMapping("/task")
public class TaskController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private TaskService taskService;
	
	@Value("${web.upload.path}")
    private String uploadPath;
	
	/**
	 * 
	 * Description: 跳转到新建任务页面   
	 * @return 
	 * date 2019年7月23日
	 */
	@RequestMapping("/toAdd")
	public String toList() {
		return "task/addTask.html";
	}
	
	@RequestMapping("/tasklst")
	public String getList() {
		return "tasklst/tasklst.html";
	}
	
	/**
	 * 
	 * Description: 跳转到任务页面
	 * @param pageName
	 * @return 
	 * date 2019年8月1日
	 */
	@RequestMapping("/taskPage")
	public String readMessage(String pageName) {
		return "task/taskPage/" + pageName + ".html";
	}
	
	@ResponseBody
	@RequestMapping("/getlst")
	public PageUtil<Task> getTaskLst(PageUtil<Task> pageUtil,Task task) {
		List<Task> lst = taskService.selectByPage(task,  pageUtil.getCurrIndex(), pageUtil.getLimit());
		int count = taskService.countTask(task);
		pageUtil.setCode("0");
		pageUtil.setCount(count);
		pageUtil.setMsg("成功");
		pageUtil.setData(lst);
		logger.info("获取任务列表");
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 新增任务    
	 * @param task
	 * @param nameStr
	 * @param valueStr
	 * @return 
	 * date 2019年7月23日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage save(Task task, String nameStr, String valueStr) {
		try {
			taskService.addTask(task, nameStr, valueStr);
			logger.info("新增任务");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增任务失败", e);
		}
		return ResultMessage.getFail();
	}

	/**
	 * Description: 删除任务
	 * @param id
	 * @return 
	 * date 2019年7月24日
	 */
	@ResponseBody
	@RequestMapping("/del")
	public ResultMessage del(String ids) {
		try {
			String[] idsArr = ids.split(",");
			List<String> idLst = Arrays.asList(idsArr);
			for(String id: idLst) {
        		taskService.delTask(Integer.valueOf(id));
			}
			logger.info("新增任务");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增任务失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * Description: 删除任务
	 * @param id
	 * @return 
	 * date 2019年7月24日
	 */
	@ResponseBody
	@RequestMapping("/operateCurrent")
	public ResultMessage operateCurrent(String id) {
		try {
			taskService.operateCurrent(id);
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增任务失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * Description: 任务暂停和开启   
	 * @param id
	 * @param state
	 * @return 
	 * date 2019年8月22日
	 */
	@ResponseBody
	@RequestMapping("/pauseAndStart")
	public ResultMessage pauseAndStart(String id,String state) {
		try {
			taskService.pauseAndStart(id,state);
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("新增任务失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 上传图片 
	 * @param file
	 * @param request
	 * @return 
	 * date 2019年8月28日
	 */
	@ResponseBody
	@RequestMapping("/upload")
	public ResultMessage upload(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		try {
			if(file.isEmpty()) {
				return ResultMessage.getFail();
			}
			String fileName = file.getOriginalFilename();
			//判断文件类型是否符合
			String fileType = FileTypeUtil.getFileType(fileName);
			if(!FileTypeUtil.TYPE_IMAGE.equals(fileType) && !FileTypeUtil.TYPE_VIDEO.equals(fileType)) {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "上传文件类型只能为图片：jpg、png,视频：mp4", null);
			}
			//文件保存地址
			String path = uploadPath;
			path += "momentsImg/";
			//新文件名
			String newFileName = new Date().getTime() + fileName.substring(fileName.lastIndexOf("."));
			File dest = new File(path + newFileName);
			logger.info("文件上传路径："+path);
			//判断文件父目录是否存在
			if(!dest.getParentFile().exists()){ 
				logger.info(path + "targetPath is not exist");  
				dest.getParentFile().mkdirs(); 
			}
			file.transferTo(dest);
			logger.info("文件上传成功");
			//文件访问地址
			String url = "http://" + request.getServerName() + ":" 
					+ request.getServerPort()  + "/uploads/momentsImg/" + newFileName;
			logger.info("奖品图片上传结束");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, url);
		}catch(Exception e) {
			logger.error("文件上传异常", e);
			return ResultMessage.getFail();
		}
	}
	
	/**
	 * 
	 * Description: 获取时间段概率
	 * @return 
	 * date 2019年8月30日
	 */
	@ResponseBody
	@RequestMapping("/getTimeRegion")
	public ResultMessage getTimeRegion() {
		try {
			Map<String, String> map = RatioCalculateUtil.getTimeRegionMap();
			logger.info("获取时间段概率");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, map);
		}catch (Exception e) {
			logger.error("获取时间段概率失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 获取任务概率 
	 * @return 
	 * date 2019年8月30日
	 */
	@ResponseBody
	@RequestMapping("getTaskRatio")
	public ResultMessage getTaskRatio() {
		try {
			Map<String, String> map = RatioCalculateUtil.getActionMap();
			logger.info("获取任务概率");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, map);
		}catch (Exception e) {
			logger.error("获取任务概率失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 根据ID获取养号任务
	 * @param taskId
	 * @return 
	 * date 2019年9月4日
	 */
	@ResponseBody
	@RequestMapping("/getServiceAccById")
	public ResultMessage getServiceAccById(Integer taskId) {
		try {
			Task task = taskService.selectById(taskId);
			if(task != null) {
				if(task.getTaskType().equals(TaskTypeEnum.SERVICE_ACCOUNT.getCode())) {
					logger.error("根据ID获取养号任务");
					return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, task);
				}
			}
		}catch (Exception e) {
			logger.error("根据ID获取养号任务失败", e);
		}
		return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该养号任务不存在", null);
	}
	
	/**
	 * 
	 * Description: 根据ID获取QQ/微信添加好友任务 
	 * @param taskId
	 * @return 
	 * date 2019年9月19日
	 */
	@ResponseBody
	@RequestMapping("/getAddFriendById")
	public ResultMessage getAddFriendById(Integer taskId, Integer type) {
		try {
			Task task = taskService.selectById(taskId);
			if(task != null) {
				if(type == 1 && task.getTaskType().equals(TaskTypeEnum.QQ_ADD_FRIEND.getCode())) {
					logger.info("根据ID获取QQ添加好友任务");
					return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, task);
				}
				if(type == 2 && task.getTaskType().equals(TaskTypeEnum.ADD_FRIEND.getCode())) {
					logger.info("根据ID获取微信添加好友任务");
					return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, task);
				}
			}
		}catch (Exception e) {
			logger.error("根据ID获取QQ/微信添加好友任务失败", e);
		}
		if(type ==1) {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该QQ添加好友任务不存在", null);
		}else {
			return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, "该微信添加好友任务不存在", null);
		}
	}
	
}
