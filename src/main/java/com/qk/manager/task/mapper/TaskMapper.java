package com.qk.manager.task.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.qk.manager.task.entity.Task;

/**
 * 
 * <p>Description: 加载任务类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */

@Mapper
public interface TaskMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(Task record);

    Task selectByPrimaryKey(Integer id);

    List<Task> selectAll();

    int updateByPrimaryKey(Task record);
    
    /**
     * 
     * Description: 分页查询    
     * @param task
     * @param start
     * @param pageSize
     * @return 
     * date 2019年7月23日
     */
    List<Task> selectByPage(@Param("task") Task task, Integer start, Integer pageSize, @Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 统计数量    
     * @param task
     * @return 
     * date 2019年7月23日
     */
    Integer countTask(@Param("task") Task task, @Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 获取最新一条数据    
     * @return 
     * date 2019年7月23日
     */
    Task selectByNew();
    
    /**
     * 
     * Description: 根据设备获取账号    
     * @param deviceNum
     * @return 
     * date 2019年7月26日
     */
    List<Task> selectByDevice(String deviceNum);
    
    /**
     * 
     * Description: 修改任务状态    
     * @param id
     * @param state 
     * date 2019年7月26日
     */
    void updateState(Integer id, Integer state);
    
    /**
     * Description: 获取所有任务及item信息   
     * @param taskId
     * @return 
     * date 2019年7月31日
     */
    Task selectDataByTaskId(@Param("taskId") Integer taskId);
    
    /**
     * Description: 更改状态    
     * @param reason 
     * @param state
     * @param taskId 
     * date 2019年8月1日
     */
    void updateStateReaById(@Param("reason") String reason,@Param("state") String state,@Param("taskId") Integer taskId);
}