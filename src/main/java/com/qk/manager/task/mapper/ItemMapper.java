package com.qk.manager.task.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.qk.manager.task.entity.Item;

/**
 * 
 * <p>Description: 加载任务项目类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */

@Mapper
public interface ItemMapper {

	int insert(Item record);

    List<Item> selectAll();
    
    /**
     * 
     * Description: 根据任务查询    
     * @param taskId
     * @return 
     * date 2019年7月23日
     */
    List<Item> selectByTaskId(Integer taskId);
    
    /**
     * 
     * Description: 根据名称查询    
     * @param taskId
     * @param name
     * @return 
     * date 2019年7月23日
     */
    Item selectByName(Integer taskId, String name);
    
    /**
     * 
     * Description: 根据任务删除    
     * @param taskId 
     * date 2019年7月23日
     */
    void deleteByTaskId(Integer taskId);
}