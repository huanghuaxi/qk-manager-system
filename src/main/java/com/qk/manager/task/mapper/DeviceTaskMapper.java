package com.qk.manager.task.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.qk.manager.task.entity.DeviceTask;

/**
 * 
 * <p>Description: 加载设备任务类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月26日   
 * @version 1.0
 */

@Mapper
public interface DeviceTaskMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(DeviceTask record);

    DeviceTask selectByPrimaryKey(Integer id);

    List<DeviceTask> selectAll();

    int updateByPrimaryKey(DeviceTask record);
    
    /**
     * 
     * Description: 修改设备任务状态
     * @param deviceNum
     * @param taskId
     * @param state 
     * date 2019年7月30日
     */
    void updateDeviceTaskState(String deviceNum, Integer taskId, Integer state);
    
    /**
     * Description: 根据任务id 查询设备    
     * @param taskId
     * @return 
     * date 2019年7月31日
     */
    List<DeviceTask> selectByTaskId(Integer taskId);
    
    /**
     * 
     * Description: 统计设备完成任务情况
     * @param taskId
     * @param state
     * @return 
     * date 2019年8月1日
     */
    Integer countState(Integer taskId, Integer state);
    
    /**
     * 
     * Description: 根据任务ID删除数据 
     * @param taskId 
     * date 2019年8月5日
     */
    void deleteByTaskId(Integer taskId);
    
    /**
     * Description: 选择设备任务   
     * @param taskId
     * @param list
     * @return 
     * date 2019年9月11日
     */
    List<DeviceTask> selectByTaskIdAndDeviceId(Integer taskId,List<Integer> list);
    
    /**
     * Description: 更新设备状态    
     * @param taskId 任务号
     * @param state 状态
     * date 2019年9月27日
     */
    void updateDeviceTaskStateAll(Integer taskId, Integer state);
}