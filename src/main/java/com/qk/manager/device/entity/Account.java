package com.qk.manager.device.entity;

import java.util.Date;

/**
 * 
 * <p>Description: 账号类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */
public class Account {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	账号
     */
    private String account;

    /**
     *	设备ID
     */
    private Integer deviceId;

    /**
     *	状态
     */
    private Integer state;

    /**
     *	在线时间
     */
    private Date onlineTime;

    /**
     *	创建时间
     */
    private Date createTime;

    /**
     *	手机号
     */
    private String phone;
    
    /**
     * 	管理员ID
     */
    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getOnlineTime() {
        return onlineTime;
    }

    public void setOnlineTime(Date onlineTime) {
        this.onlineTime = onlineTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}