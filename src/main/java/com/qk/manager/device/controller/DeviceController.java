package com.qk.manager.device.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qk.manager.business.controller.PageUtil;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.ResultMessage;
import com.qk.manager.device.entity.Device;
import com.qk.manager.device.service.DeviceService;

/**
 * 
 * <p>Description: 设备控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */

@Controller
@RequestMapping("/device")
public class DeviceController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private DeviceService deviceService;
	
	/**
	 * 
	 * Description: 跳转到设备管理页面    
	 * @return 
	 * date 2019年7月23日
	 */
	@RequestMapping("/toList")
	public String toList() {
		return "device/device.html";
	}
	
	/**
	 * 
	 * Description: 获取设备列表    
	 * @param pageUtil
	 * @param device
	 * @return 
	 * date 2019年7月23日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil<Device> getList(PageUtil<Device> pageUtil, Device device) {
		try {
			List<Device> dList = deviceService.selectByPage(device, pageUtil.getCurrIndex(), pageUtil.getLimit());
			Integer count = deviceService.countDevice(device);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(dList);
			logger.info("获取设备列表");
		}catch(Exception e) {
			logger.error("获取设备列表失败", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 修改设备    
	 * @param device
	 * @return 
	 * date 2019年7月23日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(Device device) {
		try {
			deviceService.editDevice(device);
			logger.info("修改设备");
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("修改设备失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 删除设备    
	 * @param id
	 * @return 
	 * date 2019年7月23日
	 */
	@ResponseBody
	@RequestMapping("/del")
	public ResultMessage del(Integer id) {
		try {
			deviceService.delDevice(id);
			logger.info("删除设备");
			return ResultMessage.getSuccess();
		}catch (Exception e) {
			logger.error("删除设备失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 获取设备列表    
	 * @return 
	 * date 2019年7月24日
	 */
	@ResponseBody
	@RequestMapping("/getAll")
	public ResultMessage getAll() {
		try {
			List<Device> list = deviceService.selectAll();
			logger.info("获取设备列表");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, list);
		}catch (Exception e) {
			logger.error("获取设备列表失败", e);
		}
		return ResultMessage.getFail();
	}
}
