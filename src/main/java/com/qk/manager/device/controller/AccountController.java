package com.qk.manager.device.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qk.manager.business.controller.PageUtil;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.ResultMessage;
import com.qk.manager.device.entity.Account;
import com.qk.manager.device.service.AccountService;


/**
 * 
 * <p>Description: 账号控制类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */

@Controller
@RequestMapping("/account")
public class AccountController {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private AccountService accountService;
	
	/**
	 * 
	 * Description: 跳转到账号管理页面    
	 * @return 
	 * date 2019年7月23日
	 */
	@RequestMapping("/toList")
	public String toList() {
		return "device/account.html"; 
	}
	
	/**
	 * 
	 * Description: 获取账号列表    
	 * @param pageUtil
	 * @param account
	 * @return 
	 * date 2019年7月23日
	 */
	@ResponseBody
	@RequestMapping("/getList")
	public PageUtil<Account> getList(PageUtil<Account> pageUtil, Account account) {
		try {
			List<Account> aList = accountService.selectByPage(account, pageUtil.getCurrIndex(), pageUtil.getLimit());
			Integer count = accountService.countAccount(account);
			pageUtil.setCode("0");
			pageUtil.setCount(count);
			pageUtil.setMsg("成功");
			pageUtil.setData(aList);
			logger.info("获取账号列表");
		}catch (Exception e) {
			logger.error("获取账号列表失败", e);
			pageUtil.setCode("1");
			pageUtil.setMsg("失败");
		}
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 编辑账号    
	 * @param account
	 * @return 
	 * date 2019年7月23日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(Account account) {
		try {
			accountService.editAccount(account);
			logger.info("编辑账号");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("编辑账号失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 删除账号    
	 * @param id
	 * @return 
	 * date 2019年7月23日
	 */
	@ResponseBody
	@RequestMapping("/del")
	public ResultMessage del(Integer id) {
		try {
			accountService.delAccount(id);
			logger.info("删除账号");
			return ResultMessage.getSuccess();
		}catch(Exception e) {
			logger.error("删除账号失败", e);
		}
		return ResultMessage.getFail();
	}
	
	/**
	 * 
	 * Description: 获取账号数量 
	 * @return 
	 * date 2019年8月12日
	 */
	@ResponseBody
	@RequestMapping("/getAccountCount")
	public ResultMessage getAccountCount() {
		try {
			Integer count = accountService.countAccount(new Account());
			logger.info("获取账号数量");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, count);
		}catch (Exception e) {
			logger.error("获取账号数量失败", e);
		}
		return ResultMessage.getFail();
	}
}
