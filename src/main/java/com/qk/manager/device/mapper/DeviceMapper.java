package com.qk.manager.device.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.qk.manager.device.entity.Device;

/**
 * 
 * <p>Description: 加载设备类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */

@Mapper
public interface DeviceMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Device record);

    Device selectByPrimaryKey(Integer id);

    List<Device> selectAll();

    int updateByPrimaryKey(Device record);
    
    /**
     * 
     * Description: 分页查询    
     * @param device
     * @param start
     * @param pageSize
     * @return 
     * date 2019年7月23日
     */
    List<Device> selectByPage(Device device, Integer start, Integer pageSize, @Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 统计数量    
     * @param device
     * @return 
     * date 2019年7月23日
     */
    Integer countDevice(Device device, @Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 根据ID查询    
     * @param ids
     * @return 
     * date 2019年7月26日
     */
    List<Device> selectByIds(@Param("ids") List<String> ids);
    
    /**
     * 
     * Description: 根据设备号查询数据    
     * @param deviceNum
     * @return 
     * date 2019年7月29日
     */
    Device selectByDeviceNum(String deviceNum);
    
    /**
     * 
     * Description: 获取最新设备    
     * @return 
     * date 2019年7月29日
     */
    Device selectByNew();
    
    /**
     * 
     * Description: 修改设备状态
     * @param deviceNum 
     * @param state 
     * date 2019年7月29日
     */
    void updateState(String deviceNum, Integer state);
}