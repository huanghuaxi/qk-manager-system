package com.qk.manager.device.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.qk.manager.device.entity.Account;

/**
 * 
 * <p>Description: 加载账号类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */

@Mapper
public interface AccountMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(Account record);

    Account selectByPrimaryKey(Integer id);

    List<Account> selectAll();

    int updateByPrimaryKey(Account record);
    
    /**
     * 
     * Description: 分页查询    
     * @param account
     * @param start
     * @param pageSize
     * @return 
     * date 2019年7月23日
     */
    List<Account> selectByPage(Account account, Integer start, Integer pageSize, @Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 统计数量    
     * @param account
     * @return 
     * date 2019年7月23日
     */
    Integer countAccount(Account account, @Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 根据设备获取账号    
     * @param ids
     * @return 
     * date 2019年7月25日
     */
    List<Account> selectByDevice(@Param("ids") List<String> ids);
    
    /**
     * 
     * Description: 解绑设备    
     * @param deviceId 
     * date 2019年7月29日
     */
    void unbindingDevice(Integer deviceId);
    
    /**
     * 
     * Description: 绑定设备
     * @param account
     * @param deviceId 
     * date 2019年7月29日
     */
    void bindingDevice(String account, Integer deviceId);
    
    /**
     * 
     * Description: 根据账号查询
     * @param account
     * @return 
     * date 2019年7月29日
     */
    Account selectByAccount(String account);
    
    /**
     * 
     * Description: 修改账号状态
     * @param account 
     * @param state 
     * date 2019年7月29日
     */
    void updateState(String account, Integer state);
    
    /**
     * Description:  根据设备号查找微信账号
     * @param deviceId
     * @return 
     * date 2019年7月31日
     */
    List<Account> selectAccByDeviceId(Integer uid,@Param("deviceId") Integer deviceId);
    
    /**
     * 
     * Description:  根据设备id 获取账号信息
     * @return 
     * date 2019年8月14日
     */
    List<Account> selectAcctByDeviceIds(List<Integer> list);
    
    /**
     * Description: 查询目标好友   
     * @param accList
     * @return 
     * date 2019年9月11日
     */
    List<Account> selectTargetFriendByAccAndPhone(@Param("uid") Integer uid,@Param("accList") List<String> accList);
    
}