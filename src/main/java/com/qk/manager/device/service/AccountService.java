package com.qk.manager.device.service;

import java.util.List;

import com.qk.manager.device.entity.Account;

/**
 * 
 * <p>Description: 加载账号类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */
public interface AccountService {
	
	/**
	 * 
	 * Description: 分页查询    
	 * @param account
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年7月23日
	 */
	public List<Account> selectByPage(Account account, Integer start, Integer pageSize);
	
	/**
	 * 
	 * Description: 统计数量    
	 * @param account
	 * @return 
	 * date 2019年7月23日
	 */
	public Integer countAccount(Account account);
	
	/**
	 * 
	 * Description: 添加账号    
	 * @param account 
	 * date 2019年7月23日
	 */
	public void addAccount(Account account);
	
	/**
	 * 
	 * Description: 编辑账号    
	 * @param account 
	 * date 2019年7月23日
	 */
	public void editAccount(Account account);
	
	/**
	 * 
	 * Description: 删除账号    
	 * @param id 
	 * date 2019年7月23日
	 */
	public void delAccount(Integer id);
	
	/**
	 * 
	 * Description: 绑定设备   
	 * @param deviceNum
	 * @param account
	 * @param deviceType
	 * @param mobile 
	 * @param userId 
	 * date 2019年7月29日
	 */
	public void bindingDevice(String deviceNum, String account, String deviceType, String mobile, Integer userId);
	
	/**
	 * 
	 * Description: 修改状态
	 * @param account 
	 * @param state 
	 * date 2019年7月29日
	 */
	public void updateState(String account, Integer state);
	
	 /**
     * Description:  根据设备号查找微信账号
     * @param deviceId
     * @return 
     * date 2019年7月31日
     */
    List<Account> selectAccByDeviceId(Integer uid,Integer deviceId);
}
