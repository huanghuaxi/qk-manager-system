package com.qk.manager.device.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.qk.manager.common.CurrentParamUtil;
import com.qk.manager.device.entity.Account;
import com.qk.manager.device.entity.Device;
import com.qk.manager.device.mapper.AccountMapper;
import com.qk.manager.device.mapper.DeviceMapper;
import com.qk.manager.device.service.AccountService;
import com.qk.manager.security.entity.SysUser;
import com.qk.manager.security.service.UserService;

/**
 * 
 * <p>Description: 账号更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */

@Service
public class AccountServiceImpl implements AccountService{

	@Autowired
	private AccountMapper accountMapper;
	
	@Autowired
	private DeviceMapper deviceMapper;
	
	@Autowired
	private UserService userService;
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByPage</p>   
	 * <p>Description: 分页查询</p>   
	 * @param account
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.qk.manager.device.service.AccountService#selectByPage(com.qk.manager.device.entity.Account, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Account> selectByPage(Account account, Integer start, Integer pageSize) {
		List<Integer> userIds = userService.getRoleUser();
		return accountMapper.selectByPage(account, start, pageSize, userIds);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: countAccount</p>   
	 * <p>Description: 统计数量</p>   
	 * @param account
	 * @return   
	 * @see com.qk.manager.device.service.AccountService#countAccount(com.qk.manager.device.entity.Account)
	 */
	@Override
	public Integer countAccount(Account account) {
		List<Integer> userIds = userService.getRoleUser();
		return accountMapper.countAccount(account, userIds);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: addAccount</p>   
	 * <p>Description: 添加账号</p>   
	 * @param account   
	 * @see com.qk.manager.device.service.AccountService#addAccount(com.qk.manager.device.entity.Account)
	 */
	@Override
	public void addAccount(Account account) {
		Assert.hasText(account.getAccount(), "账号数据不存在");
		Assert.notNull(account.getDeviceId(), "账号所属设备数据不存在");
		SysUser user = CurrentParamUtil.getCurrentUser();
		account.setUserId(user.getId());
		accountMapper.insert(account);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editAccount</p>   
	 * <p>Description: 修改账号</p>   
	 * @param account   
	 * @see com.qk.manager.device.service.AccountService#editAccount(com.qk.manager.device.entity.Account)
	 */
	@Override
	public void editAccount(Account account) {
		Assert.notNull(account.getId(), "账号ID数据不存在");
		Assert.hasText(account.getAccount(), "账号数据不存在");
		Account ac = accountMapper.selectByPrimaryKey(account.getId());
		Assert.notNull(ac, "账号数据不存在");
		ac.setAccount(account.getAccount());
		accountMapper.updateByPrimaryKey(ac);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: delAccount</p>   
	 * <p>Description: 删除账号</p>   
	 * @param id   
	 * @see com.qk.manager.device.service.AccountService#delAccount(java.lang.Integer)
	 */
	@Override
	public void delAccount(Integer id) {
		Assert.notNull(id, "账号ID数据不存在");
		accountMapper.deleteByPrimaryKey(id);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: bindingDevice</p>   
	 * <p>Description: 绑定设备</p>   
	 * @param deviceNum
	 * @param account
	 * @param deviceType
	 * @param mobile   
	 * @see com.qk.manager.device.service.AccountService#bindingDevice(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	@Transactional
	public void bindingDevice(String deviceNum, String account, String deviceType, String mobile, Integer userId) {
		Assert.hasText(deviceNum, "设备号数据不存在");
		Assert.hasText(account, "账号数据不存在");
		Assert.notNull(userId, "管理员ID不存在");
		//查询设备
		Device device = deviceMapper.selectByDeviceNum(deviceNum);
		//查询账号
		Account ac = accountMapper.selectByAccount(account);
		if(ac == null) {
			//新增账号
			ac = new Account();
			ac.setAccount(account);
			ac.setPhone(mobile);
			ac.setCreateTime(new Date());
			ac.setOnlineTime(new Date());
			ac.setState(1);
			ac.setUserId(userId);
			accountMapper.insert(ac);
		}else {
			//修改账号状态
			ac.setOnlineTime(new Date());
			ac.setState(1);
			ac.setUserId(userId);
			accountMapper.updateByPrimaryKey(ac);
		}
		//解绑其他设备
		accountMapper.unbindingDevice(device.getId());
		//绑定设备
		accountMapper.bindingDevice(account, device.getId());
			
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: updateState</p>   
	 * <p>Description: 修改状态</p>   
	 * @param account   
	 * @param state   
	 * @see com.qk.manager.device.service.AccountService#updateState(java.lang.String)
	 */
	@Override
	public void updateState(String account, Integer state) {
		Assert.hasText(account, "账号数据不存在");
		accountMapper.updateState(account, state);
	}
	
    /*
     *(non-Javadoc)   
     * <p>Title: selectAccByDeviceId</p>   
     * <p>Description: 查找设备上的账号 </p>   
     * @param deviceId
     * @return   
     * @see com.qk.manager.device.service.AccountService#selectAccByDeviceId(java.lang.Integer)
     */
	@Override
	public List<Account> selectAccByDeviceId(Integer uid,Integer deviceId) {
		return accountMapper.selectAccByDeviceId(uid,deviceId);
	}
	
	
	
}
