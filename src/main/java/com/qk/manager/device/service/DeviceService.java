package com.qk.manager.device.service;

import java.util.List;

import com.qk.manager.device.entity.Device;

/**
 * 
 * <p>Description: 加载设备类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */
public interface DeviceService {
	
	/**
	 * 
	 * Description: 分页查询    
	 * @param device
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年7月23日
	 */
	public List<Device> selectByPage(Device device, Integer start, Integer pageSize);
	
	/**
	 * 
	 * Description: 统计数量    
	 * @param device
	 * @return 
	 * date 2019年7月23日
	 */
	public Integer countDevice(Device device);
	
	/**
	 * 
	 * Description: 添加设备    
	 * @param device 
	 * date 2019年7月23日
	 */
	public void addDevice(Device device);
	
	/**
	 * 
	 * Description: 编辑设备    
	 * @param device 
	 * date 2019年7月23日
	 */
	public void editDevice(Device device);
	
	/**
	 * 
	 * Description: 删除设备    
	 * @param id 
	 * date 2019年7月23日
	 */
	public void delDevice(Integer id);
	
	/**
	 * 
	 * Description: 获取全部设备    
	 * @return 
	 * date 2019年7月24日
	 */
	public List<Device> selectAll();
	
	/**
	 * 
	 * Description: 根据设备号获取设备
	 * @param deviceNum
	 * @return 
	 * date 2019年7月29日
	 */
	public Device selectByDeviceNum(String deviceNum);
	
	/**
	 * 
	 * Description: 修改设备状态
	 * @param deviceNum 
	 * @param state 
	 * date 2019年7月29日
	 */
	public void updateState(String deviceNum, Integer state);
	
	/**
	 * 
	 * Description: 绑定设备 
	 * @param deviceNum
	 * @param deviceType
	 * @param userId 
	 * date 2019年9月20日
	 */
	public void bindingDevice(String deviceNum, String deviceType, Integer userId);
	
	 /**
     * Description: 更新设备状态    
     * @param taskId 任务号
     * @param state 状态
     * date 2019年9月27日
     */
    void updateDeviceTaskStateAll(Integer taskId, Integer state);
}
