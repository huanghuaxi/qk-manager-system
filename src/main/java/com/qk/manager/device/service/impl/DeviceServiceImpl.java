package com.qk.manager.device.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.qk.manager.common.CurrentParamUtil;
import com.qk.manager.device.entity.Device;
import com.qk.manager.device.mapper.DeviceMapper;
import com.qk.manager.device.service.DeviceService;
import com.qk.manager.security.entity.SysUser;
import com.qk.manager.security.service.UserService;
import com.qk.manager.task.mapper.DeviceTaskMapper;

/**
 * 
 * <p>Description: 设备更新类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月23日   
 * @version 1.0
 */

@Service
public class DeviceServiceImpl implements DeviceService{

	@Autowired
	private DeviceMapper deviceMapper;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private DeviceTaskMapper deviceTaskMapper;
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByPage</p>   
	 * <p>Description: 分页查询</p>   
	 * @param device
	 * @param start
	 * @param pageSize
	 * @return   
	 * @see com.qk.manager.device.service.DeviceService#selectByPage(com.qk.manager.device.entity.Device, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Device> selectByPage(Device device, Integer start, Integer pageSize) {
		List<Integer> userIds = userService.getRoleUser();
		return deviceMapper.selectByPage(device, start, pageSize, userIds);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: countDevice</p>   
	 * <p>Description: 统计数量</p>   
	 * @param device
	 * @return   
	 * @see com.qk.manager.device.service.DeviceService#countDevice(com.qk.manager.device.entity.Device)
	 */
	@Override
	public Integer countDevice(Device device) {
		List<Integer> userIds = userService.getRoleUser();
		return deviceMapper.countDevice(device, userIds);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: addDevice</p>   
	 * <p>Description: 添加设备</p>   
	 * @param device   
	 * @see com.qk.manager.device.service.DeviceService#addDevice(com.qk.manager.device.entity.Device)
	 */
	@Override
	public void addDevice(Device device) {
		Assert.hasText(device.getDeviceNum(), "设备账号数据不存在");
		Assert.hasText(device.getMark(), "设备标识数据不存在");
		SysUser user = CurrentParamUtil.getCurrentUser();
		device.setUserId(user.getId());
		deviceMapper.insert(device);
	}
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: editDevice</p>   
	 * <p>Description: 编辑设备</p>   
	 * @param device   
	 * @see com.qk.manager.device.service.DeviceService#editDevice(com.qk.manager.device.entity.Device)
	 */
	@Override
	public void editDevice(Device device) {
		Assert.notNull(device.getId(), "设备ID数据不存在");
		Assert.hasText(device.getDeviceNum(), "设备编码数据不存在");
		Device dv = deviceMapper.selectByPrimaryKey(device.getId());
		Assert.notNull(dv, "设备数据不存在");
		dv.setDeviceNum(device.getDeviceNum());
		dv.setDeviceType(device.getDeviceType());
		dv.setMark(device.getMark());
		deviceMapper.updateByPrimaryKey(dv);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: delDevice</p>   
	 * <p>Description: 删除设备</p>   
	 * @param id   
	 * @see com.qk.manager.device.service.DeviceService#delDevice(java.lang.Integer)
	 */
	@Override
	public void delDevice(Integer id) {
		deviceMapper.deleteByPrimaryKey(id);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectAll</p>   
	 * <p>Description: 获取全部设备</p>   
	 * @return   
	 * @see com.qk.manager.device.service.DeviceService#selectAll()
	 */
	@Override
	public List<Device> selectAll() {
		return deviceMapper.selectAll();
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByDeviceNum</p>   
	 * <p>Description: 根据设备号获取设备</p>   
	 * @param deviceNum
	 * @return   
	 * @see com.qk.manager.device.service.DeviceService#selectByDeviceNum(java.lang.String)
	 */
	@Override
	public Device selectByDeviceNum(String deviceNum) {
		Assert.hasText(deviceNum, "设备号数据不存在");
		return deviceMapper.selectByDeviceNum(deviceNum);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: updateState</p>   
	 * <p>Description: 修改设备状态</p>   
	 * @param deviceNum   
	 * @param state   
	 * @see com.qk.manager.device.service.DeviceService#updateState(java.lang.String)
	 */
	@Override
	public void updateState(String deviceNum, Integer state) {
		Assert.hasText(deviceNum, "设备号数据不存在");
		deviceMapper.updateState(deviceNum, state);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: bindingDevice</p>   
	 * <p>Description: 绑定设备</p>   
	 * @param deviceNum
	 * @param deviceType
	 * @param userId   
	 * @see com.qk.manager.device.service.DeviceService#bindingDevice(java.lang.String, java.lang.String, java.lang.Integer)
	 */
	@Override
	public void bindingDevice(String deviceNum, String deviceType, Integer userId) {
		Assert.hasText(deviceNum, "设备号数据不存在");
		Assert.notNull(userId, "管理员ID数据不存在");
		//查询设备
		Device device = deviceMapper.selectByDeviceNum(deviceNum);
		if(device == null) {
			//新增设备
			device = new Device();
			device.setDeviceNum(deviceNum);
			device.setDeviceType(deviceType);
			device.setOnlineTime(new Date());
			device.setState(1);
			device.setUserId(userId);
			deviceMapper.insert(device);
			//获取新增设备
			device = deviceMapper.selectByDeviceNum(deviceNum);
			//初始化设备名
			device.setMark("设备" + device.getId());
			deviceMapper.updateByPrimaryKey(device);
		}else {
			//修改设备状态
			device.setOnlineTime(new Date());
			device.setState(1);
			device.setUserId(userId);
			deviceMapper.updateByPrimaryKey(device);
		}
		
	}

	@Override
	public void updateDeviceTaskStateAll(Integer taskId, Integer state) {
	     deviceTaskMapper.updateDeviceTaskStateAll(taskId, state);
	}

}
