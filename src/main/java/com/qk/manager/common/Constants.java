package com.qk.manager.common;

/**
 * Description:   常量类
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年4月3日   
 * @version 1.0
 */
public interface Constants {
	/**
	 * 任务id标识
	 */
	public static final String 	TASK_TAG = "taskId";
	
	/**
	 * Times New Roman 字体样式
	 */
	public static final String 	FONT_TIMES_NEW_ROMAN ="Fixedsys";
	
	/**
	 * 图片格式
	 */
	public static final String 	IMAGE_FORMAT ="jpeg";
	
	/**
	 * 管理员角色名称
	 */
	public static final String ROLE_NAME = "Administrator";
	
	/**
	 * 数值--200
	 */
	public static final int NUM_TYPE_200 = 200;
	
	/**
	 * 数值--250
	 */
	public static final int NUM_TYPE_250 = 250;
	
	/**
	 * 数值--160
	 */
	public static final int NUM_TYPE_160 = 160;
	
	/**
	 * 数值--160
	 */
	public static final int NUM_TYPE_155 = 155;
	
	/**
	 * 数值--20
	 */
	public static final int NUM_TYPE_20 = 20;
	
	/**
	 * 数值--12
	 */
	public static final int NUM_TYPE_12 = 12;
	
	/**
	 * 数值--110
	 */
	public static final int NUM_TYPE_110 = 110;
	
	/**
	 * 数值--13
	 */
	public static final int NUM_TYPE_13 = 13;
	
	/**
	 * 数值--6
	 */
	public static final int NUM_TYPE_6 = 6;
	
	/**
	 * 数值--6
	 */
	public static final int NUM_TYPE_16 = 16;
	
	/**
	 * 数值--0
	 */
	public static final int NUM_TYPE_0 = 0;
	
	/**
	 * 数值--255
	 */
	public static final int NUM_TYPE_255 = 255;
	
	/**
	 * 数值--1
	 */
	public static final int NUM_TYPE_1 = 1;
	
	/**
	 * 验证码缓存键名
	 */
	public final static String SESSION_KEY = "SESSION_KEY_IMAGE_CODE";
	
	/**
	 * 用户无效标识
	 */
	public final static String AUTH_MESSAGE_DISABLE = "auth.message.disable";
	
	/**
	 * 密码无效标识
	 */
	public final static String AUTH_MESSAGE_CREDENTIALS = "auth.message.credentials";
	
	/**
	 * 账户过期标识
	 */
	public final static String AUTH_MESSAGE_ACCOUNT_EXPIRED = "auth.message.account.expired";
	
	/**
	 * 密码过期标识
	 */
	public final static String AUTH_MESSAGE_PWD_EXPIRED = "auth.message.pwd.expired";
	
	/**
	 * 用户被锁标识
	 */
	public final static String AUTH_MESSAGE_ACCOUNT_LOCKED = "auth.message.account.locked";
	
	/**
	 * 验证码为空标识
	 */
	public final static String AUTH_MESSAGE_VALIDATECODE_NULL = "auth.message.validatecode.null";
	
	/**
	 * 验证码不存在标识
	 */
	public final static String AUTH_MESSAGE_VALIDATECODE_NOTEXIST = "auth.message.validatecode.notexist";
	
	/**
	 * 验证码过期标识
	 */
	public final static String AUTH_MESSAGE_VALIDATECODE_EXPIRED = "auth.message.validatecode.expired";
	
	/**
	 *  验证码不正确标识
	 */
	public final static String AUTH_MESSAGE_VALIDATECODE_INCORRECT = "auth.message.validatecode.incorrect";

	/**
	 * 用户被锁
	 */
	public final static String AUTH_MESSAGE_ACCOUNT_IS_LOCKED = "auth.message.account.is.locked";
	
	/**
	 *  图片码
	 */
	public final static String IMAGE_CODE = "imageCode";
	
	/**
	 *  post 
	 */
	public final static String POST = "post";
	
	/**
	 * 密码错误标识
	 */
	public final static String BAD_CREDENTIALS = "Bad credentials";
	
	/**
	 * 用户无效标识
	 */
	public final static String USER_IS_DISABLED = "User is disabled";
	
	/**
	 * 用户过期标识
	 */
	public final static String USER_ACCOUNT_HAS_EXPIRED = "User account has expired";
	
	/**
	 * 密码过期标识
	 */
	public final static String USER_CREDENTIALS_HAVE_EXPIRED = "User credentials have expired";
	
	/**
	 * 用户被锁
	 */
	public final static String USER_ACCOUNT_IS_LOCKED = "User account is locked";
	
	/**
	 * 用户不存在
	 */
	public final static String AUTH_MESSAGE_ACCOUNT_NOTEXIST = "auth.message.account.notexist";
	
	/**
	 * 用户不存在
	 */
	public final static String USER_NOT_EXIST = "user not exist";
	
	/**
	 * 资源访问路径
	 */
	public final static String  SOURCE_FILE = "file:";
	
	/**
	 * 24到早8点
	 */
	public final static String  TIME_REGION_SEVEN = "timeRegionSeven";
	
	/**
	 * 概率范围
	 */
	public final static String RATIO_REGIN_SEVEN = "0:8;2:3";
	
	/**
	 * 8点到10点
	 */
	public final static String  TIME_REGION_ONE = "timeRegionOne";
	
	/**
	 * 概率范围
	 */
	public final static String RATIO_REGIN_ONE = "8:10;4:5";
	
	/**
	 * 10点到10点
	 */
	public final static String  TIME_REGION_TWO = "timeRegionTwo";
	
	/**
	 * 概率范围
	 */
	public final static String RATIO_REGIN_TWO = "10:12;5:6";
	
	/**
	 * 12点到14点
	 */
	public final static String  TIME_REGION_THREE = "timeRegionThree";
	

	/**
	 * 概率范围
	 */
	public final static String RATIO_REGIN_THREE = "12:14;4:5";
	
	/**
	 * 14点到19点
	 */
	public final static String  TIME_REGION_FOUR = "timeRegionFour";
	
	/**
	 * 概率范围
	 */
	public final static String RATIO_REGIN_FOUR = "14:19;6:7";
	
	/**
	 * 19点到22点
	 */
	public final static String  TIME_REGION_FIVE = "timeRegionFive";
	
	/**
	 * 概率范围
	 */
	public final static String RATIO_REGIN_FIVE = "19:22;6:7";
	
	/**
	 * 22点到24点
	 */
	public final static String  TIME_REGION_SIX = "timeRegionSix";
	
	/**
	 * 概率范围
	 */
	public final static String RATIO_REGIN_SIX = "22:24;7:8";
	
	/**
	 * 发送文本概率
	 */
    public final static String RATIO_ACTION_SEND_MSG = "45";
    
    /**
     * 发送语音概率
     */
    public final static String RATIO_ACTION_VOICE = "10";
	
    /**
     * 发送视频概率
     */
    public final static String RATIO_ACTION_VIDEO = "10";
    
    /**
     * 阅读朋友圈概率
     */
    public final static String RATIO_ACTION_FRIEND_CYCLE = "10";
    
    
    /**
     * 发送朋友圈概率
     */
    public final static String RATIO_ACTION_FRIEND_CYCLE_PUBLISH = "5";
    
    /**
     * 阅读消息概率
     */
    public final static String RATIO_ACTION_READ_MESSAGE = "15";
    
    
    /**
     * 发送消息业务类
     */
    public final static String ACTION_SEND_MSG = "SMG";
    
    /**
     * 发送语音业务类
     */
    public final static String ACTION_VOICE = "LONGVOICE";
    
    /**
     * 发送视频业务类
     */
    public final static String ACTION_VIDEO = "LONGVIDEO";
    
    /**
     * 发送阅读朋友圈业务类
     */
    public final static String ACTION_FRIEND_CYCLE = "RFRICYCLE";
    
    /**
     * 发送发朋友圈任务业务类
     */
    public final static String ACTION_FRIEND_CYCLE_PUBLISH = "SENDMOMENTS";
    
    /**
     * 发送发朋友圈任务业务类
     */
    public final static String ACTION_READ_MESSAGE = "READMESSAGE";
    
    /**
     * 养号业务的概率类
     */
    public final static String TASK_RATIO_ACTION = "taskRatioAction";
    
    /**
     * 养号业务的概率类
     */
    public final static String TARGET_FRIEND_KEY = "friendTarget";
    
    /**
     * 安全密鑰
     */
    public final static String SECURITY_KEY = "123QWEQ";
    
    
}
