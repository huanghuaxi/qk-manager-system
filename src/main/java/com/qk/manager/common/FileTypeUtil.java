package com.qk.manager.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * <p>Description: 文件格式工具类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年8月29日   
 * @version 1.0
 */
public class FileTypeUtil {

	private static Logger logger = LoggerFactory.getLogger(FileTypeUtil.class);
	
	public static final String TYPE_IMAGE = "img";
	
	public static final String TYPE_VIDEO = "video";
	
	/**
	 * 
	 * Description: 获取文件类型是否符合 
	 * @param fileName
	 * @return 
	 * date 2019年8月29日
	 */
	public static String getFileType(String fileName) {
		try {
			String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
			suffix = suffix.toLowerCase();
			if("jpg".equals(suffix) || "png".equals(suffix) || "gif".equals(suffix)) {
				return TYPE_IMAGE;
			}
			if("mp4".equals(suffix)) {
				return TYPE_VIDEO;
			}
		}catch (Exception e) {
			logger.error("判断文件格式失败", e);
		}
		return "";
	}
	
	/**
	 * 
	 * Description: 判断是否为图片 
	 * @param fileName
	 * @return 
	 * date 2019年8月29日
	 */
	public static boolean isImage(String fileName) {
		try {
			String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
			suffix = suffix.toLowerCase();
			if("jpg".equals(suffix) || "png".equals(suffix) || "gif".equals(suffix)) {
				return true;
			}
		}catch (Exception e) {
			logger.error("判断是否为图片失败", e);
		}
		return false;
	}
	
	/**
	 * 
	 * Description: 判断是否为视频 
	 * @param fileName
	 * @return 
	 * date 2019年8月29日
	 */
	public static boolean isVideo(String fileName) {
		try {
			String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
			suffix = suffix.toLowerCase();
			if("mp4".equals(suffix)) {
				return true;
			}
		}catch (Exception e) {
			logger.error("判断是否为视频失败", e);
		}
		return false;
	}
}
