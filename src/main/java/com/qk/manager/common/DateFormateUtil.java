package com.qk.manager.common;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormateUtil {
	
	public final static String YYYY_MM_DD_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
	
	public final static String YYYY_MM_DD = "yyyy-MM-dd";
	
	public final static String YYYYMMDD = "yyyyMMdd";
	
	public final static String HH_MM_SS = "HH:mm:ss";
	
	/**
	 * Description:    
	 * @param date
	 * @param formateStr
	 * @return 
	 * date 2019年7月30日
	 */
	public static String getDateStr(Date date,String formateStr) {
		if(date == null ) {
			throw new RuntimeException("时间为空");
		}
		if(formateStr == null || "".equals(formateStr)) {
			throw new RuntimeException("时间为空");
		}
		return new SimpleDateFormat(formateStr).format(date);
	}
	
	/**
	 * Description:    
	 * @param date
	 * @param formateStr
	 * @return 
	 * date 2019年7月30日
	 */
	public static Date getDate(String date,String formateStr)  {
		if(date == null ) {
			throw new RuntimeException("时间为空");
		}
		if(formateStr == null || "".equals(formateStr)) {
			throw new RuntimeException("时间为空");
		}
		SimpleDateFormat sdf = new SimpleDateFormat(formateStr);
		java.util.Date parse=null;
		try {
			parse = sdf.parse(date);
		} catch (Exception e) {
			throw new RuntimeException();
		}
		return parse;
	}

}
