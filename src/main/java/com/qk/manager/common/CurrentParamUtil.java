package com.qk.manager.common;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.Assert;

import com.qk.manager.security.entity.SysUser;

/**
 * Description:  当前属性获取工具类 
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年3月31日   
 * @version 1.0   
 */
public class CurrentParamUtil {
	
	/**
	 * Description: 获取当前登陆对象
	 * @date 2019年3月31日    
	 * @return 返回登陆对象
	 */
	public static SysUser getCurrentUser() {
		SysUser user =	(SysUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Assert.notNull(user, "当前登陆对象为空");
		return user;
	}
	
	/**
	 * Description: 获取当前登陆对象的权限
	 * @date 2019年3月31日    
	 * @return 返回登陆对象
	 */
	public static Authentication getCurrentAuthentication() {
		Authentication  auth =	SecurityContextHolder.getContext().getAuthentication();
		Assert.notNull(auth, "当前登陆对象为空");
		return auth;
	}
}
