package com.qk.manager.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qk.manager.enums.ServiceAccountAction;

/**
 * 
 * <p>
 * Description: 概率工具类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2019
 * </p>
 * 
 * @author huanghx
 * @date 2019年8月14日
 * @version 1.0
 */
public class RatioCalculateUtil {
	
	private static Logger logger = LoggerFactory.getLogger(RatioCalculateUtil.class);

	private final static Map<String, String> map = new HashMap<String, String>();

	/**
	 * Description: 计算概率
	 * 
	 * @param hour
	 * @param min  时间区间概率范围最小值
	 * @param max  时间区间概率范围最大值
	 * @return date 2019年8月13日
	 */
	public static boolean calculate(float min, float max) {

		if (map.isEmpty()) {
			initMap();
		}

		// 时间区间段概率
		float ratio = min + new Random().nextFloat() * (max - min);
		logger.info("区间概率,ratio:"+ratio+",max:"+max+",min:"+min);
		// 随机概率
		Random random = new Random();
		float r = random.nextFloat() * 10;
		logger.info("随机概率,random:"+r);
		if (0 < r && r <= ratio) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * Description: 根据时间判断概率范围
	 * 
	 * @param hour
	 * @return date 2019年8月14日
	 */
	public static String getRatioRange(double hour,Map<String,String> map) {
		for (String key : map.keySet()) {
			String[] array = key.split(":");
			int start = Integer.valueOf(array[0]);
			int end = Integer.valueOf(array[1]);
			if (start < hour && hour <= end) {
				return map.get(key);
			}
		}
		return "";
	}

	/**
	 * Description: 组装map
	 * 
	 * @return date 2019年8月13日
	 */
	private static void initMap() {

		map.put(Constants.TIME_REGION_ONE, Constants.RATIO_REGIN_ONE);

		map.put(Constants.TIME_REGION_TWO, Constants.RATIO_REGIN_TWO);

		map.put(Constants.TIME_REGION_THREE, Constants.RATIO_REGIN_THREE);

		map.put(Constants.TIME_REGION_FOUR, Constants.RATIO_REGIN_FOUR);

		map.put(Constants.TIME_REGION_FIVE, Constants.RATIO_REGIN_FIVE);

		map.put(Constants.TIME_REGION_SIX, Constants.RATIO_REGIN_SIX);

		map.put(Constants.TIME_REGION_SEVEN, Constants.RATIO_REGIN_SEVEN);

	}

	/**
	 * Description: 组装map
	 * 
	 * @return date 2019年8月13日
	 */
	public static Map<String, String> getActionMap() {

		Map<String, String> actionMap = new HashMap<String, String>();

		actionMap.put(Constants.ACTION_FRIEND_CYCLE,Constants.RATIO_ACTION_FRIEND_CYCLE);

		actionMap.put(Constants.ACTION_SEND_MSG,Constants.RATIO_ACTION_SEND_MSG);

		actionMap.put(Constants.ACTION_VIDEO,Constants.RATIO_ACTION_VIDEO);

		actionMap.put(Constants.ACTION_VOICE,Constants.RATIO_ACTION_VOICE);
		
		actionMap.put(Constants.ACTION_FRIEND_CYCLE_PUBLISH,Constants.RATIO_ACTION_FRIEND_CYCLE_PUBLISH);
		
		actionMap.put(Constants.ACTION_READ_MESSAGE,Constants.RATIO_ACTION_READ_MESSAGE);
		
		return actionMap;
	}

	/**
	 * Description:根据概率选择bean    
	 * @return 
	 * date 2019年8月19日
	 */
	public static String calculateAct(String taskChecked,Map<String,String> actionMap) {
		int total = 0;
		for(String key : actionMap.keySet()) {
			total = total + Integer.valueOf(actionMap.get(key));
		}
		Random random = new Random();
		//概率没有超过100 按照100计算，超过100按照总数计算
		if(total<100) {
		   total = 100;
		}
		int ratioNum = random.nextInt(total);
		float primaryNum = 0;
		for (String key : actionMap.keySet()) {
			//该任务是否被选中
			Float range = Float.valueOf(actionMap.get(key));
			if(!taskChecked.contains(key)) {
				primaryNum = primaryNum + range;
				continue;
			}
			if (primaryNum <= ratioNum && ratioNum < (range + primaryNum)) {
				return ServiceAccountAction.getActionByCode(key);
			}
			primaryNum = primaryNum + range;
		}
		return "";
	}
	
	public static Map<String, String> getTimeRegionMap() {
		if (map.isEmpty()) {
			initMap();
		}
		return map;
	}
	
	
	/**
	 * Description: 获取阈值配置    
	 * @param itemMap
	 * @return 
	 * date 2019年8月29日
	 */
	public static Map<String,String> getRegionMap(Map<String, String> itemMap){
	   Map<String,String> map = new HashMap<String,String>();
	   String str = itemMap.get(Constants.TIME_REGION_ONE);
	   map.put(str.split(";")[0], str.split(";")[1]);
		
	   str = itemMap.get(Constants.TIME_REGION_TWO);
	   map.put(str.split(";")[0], str.split(";")[1]);

	   str = itemMap.get(Constants.TIME_REGION_THREE);
	   map.put(str.split(";")[0], str.split(";")[1]);
	   
	   str = itemMap.get(Constants.TIME_REGION_FOUR);
	   map.put(str.split(";")[0], str.split(";")[1]);
	   
	   str = itemMap.get(Constants.TIME_REGION_FIVE);
	   map.put(str.split(";")[0], str.split(";")[1]);
	   
	   str = itemMap.get(Constants.TIME_REGION_SIX);
	   map.put(str.split(";")[0], str.split(";")[1]);
	   
	   str = itemMap.get(Constants.TIME_REGION_SEVEN);
	   map.put(str.split(";")[0], str.split(";")[1]);
       return map;
	}
	
	/**
	 * Description: 获取业务类的map   
	 * @param itemMap
	 * @return 
	 * date 2019年8月29日
	 */
	public static Map<String,String> getActionRatio(Map<String,String> itemMap){
		Map<String,String> map = new HashMap<String,String>();
		String str = itemMap.get(Constants.TASK_RATIO_ACTION);
		String[] strArr = str.split(";");
		for(String strs:strArr) {
			map.put(strs.split(":")[0], strs.split(":")[1]);
		}
		return map;
	}
	
}
