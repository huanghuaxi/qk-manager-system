package com.qk.manager.common;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年6月10日   
 * @version 1.0
 */
@Component
public class SpringBeanFactory implements ApplicationContextAware {

	private static ApplicationContext context;

	public static <T> T getBean(Class<T> c) {
		return context.getBean(c);
	}

	public static <T> T getBean(String name, Class<T> clazz) {
		return context.getBean(name, clazz);
	}
	
	public static Object getBean(String name) {
		return context.getBean(name);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		if(context == null){  
			context = applicationContext;  
		}
	}

}
