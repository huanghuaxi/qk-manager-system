package com.qk.manager.common;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Description: 获取属性配置
 * Copyright: Copyright (c) 2019   
 * Company: zhidisoftware.Co.Ltd.   
 * @author hhx   
 * @date 2019年4月1日   
 * @version 1.0
 */
@Component
@ConfigurationProperties(prefix ="spring.common")
public class CommonProperties {
	
	/**
     * 验证码宽度
     */
    private int width ;
    
    /**
     * 高度
     */
    private int height ;
    /**
     * 长度（几个数字）
     */
    private int length ;
    /**
     * 过期时间
     */
    private int expireIn;

    /**
     * 需要图形验证码的 url
     */
    private String url;
    
    /**
     * 随机产生码
     */
    private String code;
    
    /**
     * 失败次数
     */
    private int failCount;
    
    public int getFailCount() {
		return failCount;
	}

	public void setFailCount(int failCount) {
		this.failCount = failCount;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getExpireIn() {
		return expireIn;
	}

	public void setExpireIn(int expireIn) {
		this.expireIn = expireIn;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
