package com.qk.manager.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Description:   
 * Copyright: Copyright (c) 2019    
 * @author binghe   
 * @date 2019年4月7日   
 * @version 1.0   
 */
public class CharactorTool {
	
	private static Pattern linePattern = Pattern.compile("_(\\w)");
	/** 下划线转驼峰 */
	public static String lineToHump(String str) {
		str = str.toLowerCase();
		Matcher matcher = linePattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}
 
	private static Pattern humpPattern = Pattern.compile("[A-Z]");
	/** 驼峰转下划线,效率比上面高 */
	public static String humpToLine(String str) {
		Matcher matcher = humpPattern.matcher(str);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

}
