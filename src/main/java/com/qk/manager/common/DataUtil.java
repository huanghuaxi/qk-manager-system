package com.qk.manager.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.alibaba.fastjson.JSONObject;
import com.qk.manager.accountdata.entity.AccountData;
import com.qk.manager.accountdata.service.AccountdataService;
import com.qk.manager.device.entity.Account;
import com.qk.manager.device.entity.Device;
import com.qk.manager.device.service.AccountService;
import com.qk.manager.device.service.DeviceService;
import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.qqfriend.entity.QQAccountData;
import com.qk.manager.qqfriend.service.QQfriendService;
import com.qk.manager.quarz.service.ScheduleService;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.service.TaskService;
import com.qk.manager.websocket.server.WebSocketServer;

/**
 * 
 * <p>Description: 数据处理类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年7月29日   
 * @version 1.0
 */
public class DataUtil {
	
	private static Logger logger = LoggerFactory.getLogger(DataUtil.class);
	
	/**
	 * 
	 * Description: 封装数据转成字符串    
	 * @param code
	 * @param msg
	 * @param isTask
	 * @param data
	 * @return 
	 * date 2019年7月26日
	 */
	public static String setBaseData(String code, String msg, Integer isTask, Integer isAllowRepeat, JSONObject data) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("code", code);
		jsonObject.put("msg", msg);
		jsonObject.put("isTask", isTask);
		jsonObject.put("isAllowRepeat", isAllowRepeat);
		jsonObject.put("data", data);
		return jsonObject.toJSONString();
	}
	
	/**
	 * 
	 * Description: 解析数据    
	 * @param str 
	 * date 2019年7月29日
	 */
	public static void getData(String str) {
		try {
			JSONObject jsonObject = JSONObject.parseObject(str);
			String code = jsonObject.getString("code");
			String msg = jsonObject.getString("msg");
			JSONObject data = jsonObject.getJSONObject("data");
			logger.info("接收到客户端消息："+msg+",code："+code);
			if(CodeUtil.SUCCESS_CODE.equals(code)) {
				//获取数据成功
				String opType = data.getString("opType");
				logger.info("接收到客户端消息："+msg+",code："+code+",opType:"+opType);
				String opClass = data.getString("opClass");
				JSONObject obj = data.getJSONObject("obj");
				if("setData".equals(opType)) {
					switch(opClass) {
						case "editDevice":
							editDevice(obj);
							break;
						case "editAccount":
							//修改设备账号信息
							editAccount(obj);
							break;
						case "finishTask":
							//完成任务
							finishTask(obj);
							break;
						case "editFriendState":
							//添加好友信息
							editFriendState(obj);
							break;
						case "editQQFriendState":
							//修改QQ好友状态
							edidQQFriendState(obj);
							break;
						case "editQQTargetState":
							//修改QQ账号状态
							editQQTargetState(obj);
							break;
						case "editQQTargetRemark":
							//修改QQ账号备注
							editQQTargetRemark(obj);
							break;
					}
				}
			}else {
				//获取数据失败
			}
		}catch (Exception e) {
			logger.error("解析数据失败", e);
		}
	}
	
	/**
	 * 
	 * Description: 修改账号
	 * @param obj 
	 * date 2019年7月29日
	 * @throws Exception 
	 */
	public static void editAccount(JSONObject obj) {
		try {
			ApplicationContext act = ApplicationContextRegister.getApplicationContext();
			AccountService accountService = act.getBean(AccountService.class);
			//修改设备关联账号
			String deviceNum = obj.getString("deviceNum");
			String account = obj.getString("wechatAccount");
			String deviceType = obj.getString("deviceType");
			String mobile = obj.getString("mobile");
			Integer userId = obj.getInteger("userId");
			accountService.bindingDevice(deviceNum, account, deviceType, mobile, userId);
			
			//获取任务
			TaskService taskService = act.getBean(TaskService.class);
			taskService.selectByDevice(deviceNum, account, userId);
		}catch (Exception e) {
			logger.error("修改设备绑定失败", e);
		}
	}
	
	/**
	 * 
	 * Description: 修改设备状态 
	 * @param obj 
	 * date 2019年9月20日
	 */
	public static void editDevice(JSONObject obj) {
		try {
			ApplicationContext act = ApplicationContextRegister.getApplicationContext();
			DeviceService deviceService = act.getBean(DeviceService.class);
			//修改设备关联账号
			String deviceNum = obj.getString("deviceNum");
			String deviceType = obj.getString("deviceType");
			Integer userId = obj.getInteger("userId");
			deviceService.bindingDevice(deviceNum, deviceType, userId);
			//获取设备信息
			Device device = deviceService.selectByDeviceNum(deviceNum);
			//发送设备名称
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("opType", "setData");
			jsonObject.put("opClass", "editDeviceName");
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("deviceName", device.getMark());
			jsonObject.put("obj", new JSONObject(map));
			String dataStr = setBaseData(CodeUtil.SUCCESS_CODE, "显示设备名称", 0, 1, jsonObject);
			WebSocketServer.sendInfo(dataStr, userId + "&" + deviceNum);
		}catch (Exception e) {
			logger.error("设备连接，修改设备状态失败", e);
		}
	}
	
	/**
	 * 
	 * Description: 设备离线
	 * @param deviceNum
	 * @param account 
	 * date 2019年7月29日
	 */
	public static void offline(String sid) {
		String[] data = sid.split("&");
		String deviceNum = data[1];
		Integer uid = Integer.parseInt(data[0]);
		ApplicationContext act = ApplicationContextRegister.getApplicationContext();
		Device device = new Device();
		try {
			DeviceService deviceService = act.getBean(DeviceService.class);
			deviceService.updateState(deviceNum, 0);
			device = deviceService.selectByDeviceNum(deviceNum);
		}catch (Exception e) {
			logger.error("设备断开连接，修改设备状态失败", e);
		}
		try {
			
			AccountService accountService = act.getBean(AccountService.class);
			
			List<Account> accounts = accountService.selectAccByDeviceId(uid, device.getId());
			if(accounts != null) {
				for(Account account : accounts) {
					accountService.updateState(account.getAccount(), 0);
				}
			}
		}catch (Exception e) {
			logger.error("设备断开连接，修改账号状态失败", e);
		}
	}
	
	/**
	 * 
	 * Description: 修改好友状态
	 * @param obj 
	 * date 2019年7月30日
	 */
	public static void editFriendState(JSONObject obj) {
		try {
			ApplicationContext act = ApplicationContextRegister.getApplicationContext();
			AccountdataService accountDataService = act.getBean(AccountdataService.class);
			String targetAccount = obj.getString("targetAccount");
			String account = obj.getString("account");
			Integer state = obj.getInteger("state");
			Integer type = obj.getInteger("type");
			logger.info("修改好友库状态：账号"+account+"，状态：state："+state);
			accountDataService.updateByAccount(account, targetAccount, state);
			//type 0：检查好友任务 1：添加好友任务
			if(type == 1) {
				//判断是否添加完全部好友
				List<AccountData> accList = accountDataService.selectByAccount(targetAccount);
				if(accList == null || accList.size() == 0) {
					//完成此条设备任务
					JSONObject json = new JSONObject();
					Integer taskId = Integer.parseInt(targetAccount.split("&")[1]);
					json.put("state", 1);
					json.put("deviceNum", obj.getString("deviceNum"));
					json.put("taskId", taskId);
					finishTask(json);
					//查看任务是否完成，如果完成停止删除循环
					TaskService taskService = act.getBean(TaskService.class);
					Task task = taskService.selectById(taskId);
					if(task.getState() == 2) {
						//删除循环
						ScheduleService scheduleService = act.getBean(ScheduleService.class);
						scheduleService.deleteJob(TaskTypeEnum.getClsName(task.getTaskType()), task.getTaskType()+"@"+task.getId());
						//暂停循环
						taskService.updateStateReaById("执行完成", "2", taskId);
					}
				}
			}
		}catch (Exception e) {
			logger.error("修改好友状态失败", e);
		}
	}
	
	/**
	 * 
	 * Description: 修改QQ好友状态 
	 * @param obj 
	 * date 2019年9月25日
	 */
	public static void edidQQFriendState(JSONObject obj) {
		try {
			ApplicationContext act = ApplicationContextRegister.getApplicationContext();
			QQfriendService qqFriendService = act.getBean(QQfriendService.class);
			String targetAccount = obj.getString("targetAccount");
			String account = obj.getString("account");
			String targetDevice = obj.getString("targetDevice");
			Integer state = obj.getInteger("state");
			Integer type = obj.getInteger("type");
			Integer taskId = obj.getInteger("taskId");
			logger.info("修改QQ好友库状态：账号"+account+"，状态：state："+state);
			qqFriendService.updateStateByAccount(account, targetAccount, targetDevice, state);
			//type 0：检查好友任务 1：添加好友任务
			if(type == 1) {
				//添加好友任务， 判断设备是否添加完全部好友
				List<QQAccountData> accList = qqFriendService.selectByDevice(targetDevice);
				if(accList == null || accList.size() == 0) {
					//完成此条设备任务
					JSONObject json = new JSONObject();
					String[] data = targetDevice.split("&");
					json.put("state", 1);
					json.put("deviceNum", data[0]);
					json.put("taskId", taskId);
					finishTask(json);
					//查看任务是否完成，如果完成停止删除循环
					TaskService taskService = act.getBean(TaskService.class);
					Task task = taskService.selectById(taskId);
					if(task.getState() == 2) {
						//删除循环
						ScheduleService scheduleService = act.getBean(ScheduleService.class);
						scheduleService.deleteJob(TaskTypeEnum.getClsName(task.getTaskType()), task.getTaskType()+"@"+task.getId());
						//暂停循环
						taskService.updateStateReaById("执行完成", "2", taskId);
					}
				}
			}
		}catch (Exception e) {
			logger.error("修改QQ好友状态失败", e);
		}
	}
	
	/**
	 * 
	 * Description: 修改QQ账号状态
	 * @param obj 
	 * date 2019年10月10日
	 */
	public static void editQQTargetState(JSONObject obj) {
		try {
			logger.info("修改QQ账号状态");
			ApplicationContext act = ApplicationContextRegister.getApplicationContext();
			QQfriendService qqFriendService = act.getBean(QQfriendService.class);
			String account = obj.getString("account");
			Integer state = obj.getInteger("state");
			qqFriendService.updateQQTargetState(account, state);
		}catch (Exception e) {
			logger.error("修改QQ账号状态失败", e);
		}
	}
	
	/**
	 * 
	 * Description: 修改QQ账号备注
	 * @param obj 
	 * date 2019年10月10日
	 */
	public static void editQQTargetRemark(JSONObject obj) {
		try {
			logger.info("修改QQ账号备注");
			ApplicationContext act = ApplicationContextRegister.getApplicationContext();
			QQfriendService qqFriendService = act.getBean(QQfriendService.class);
			String account = obj.getString("account");
			String remark = obj.getString("remark");
			qqFriendService.updateQQTargetRemark(account, remark);
		}catch (Exception e) {
			logger.error("修改QQ账号备注失败", e);
		}
	}
	
	/**
	 * 
	 * Description: 完成任务
	 * @param obj 
	 * date 2019年7月30日
	 */
	public static void finishTask(JSONObject obj) {
		try {
			ApplicationContext act = ApplicationContextRegister.getApplicationContext();
			TaskService taskService = act.getBean(TaskService.class);
			String deviceNum = obj.getString("deviceNum");
			Integer taskId = obj.getInteger("taskId");
			Integer state = obj.getInteger("state");
			taskService.editDeviceTaskState(deviceNum, taskId, state);
			
		}catch (Exception e) {
			logger.error("修改任务状态失败", e);
		}
	}
}
