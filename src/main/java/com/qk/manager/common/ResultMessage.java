package com.qk.manager.common;

/**
 * 返回结果对象
 * @author binghe
 *
 */
public class ResultMessage {
	
	/**
	 *编码
	 */
	private String code;
	
	/**
	 * 信息
	 */
	private String msg;
	
	/**
	 * 对象
	 */
	private Object data;
	
	/**
	 * 获取成功数据
	 * @return 返回结果对象
	 */
	public static ResultMessage getSuccess() {
		ResultMessage rmsg = new ResultMessage(); 	
		rmsg.code = CodeUtil.SUCCESS_CODE;
		rmsg.msg = CodeUtil.SUCCESS_CODE_MSG;
		return rmsg;
	}
	
	/**
	 * 获取失败结果对象
	 * @return
	 */
	public static ResultMessage getFail() {
		ResultMessage rmsg = new ResultMessage(); 	
		rmsg.code = CodeUtil.FAIL_CODE;
		rmsg.msg = CodeUtil.FAIL_CODE_MSG;
		return rmsg;
	}
	
	/**
	 * 获取结果对象
	 * @return
	 */
	public static ResultMessage getRtMsg(String code ,String msg,Object obj) {
		ResultMessage rmsg = new ResultMessage(); 	
		rmsg.code = code;
		rmsg.msg = msg;
		rmsg.data = obj;
		return rmsg;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
