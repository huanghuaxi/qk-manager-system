package com.qk.manager.quarz.job;

import javax.annotation.Resource;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import com.qk.manager.quarz.service.OperateService;

@DisallowConcurrentExecution
@Component
public class SendVoiceCallJob implements Job {
	
	@Resource(name="sendVoiceForEachServiceImpl")
	private OperateService operateService;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		operateService.operate(context.getJobDetail().getJobDataMap());
		
	}

}
