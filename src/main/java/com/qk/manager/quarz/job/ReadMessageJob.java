package com.qk.manager.quarz.job;

import javax.annotation.Resource;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import com.qk.manager.quarz.service.OperateService;

/**
 * <p>Description: 阅读消息job </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月30日   
 * @version 1.0
 */
@DisallowConcurrentExecution
@Component
public class ReadMessageJob implements Job {

	@Resource(name="readMessageServiceImpl")
	private OperateService operateService;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		operateService.operate(context.getJobDetail().getJobDataMap());
	}
}
