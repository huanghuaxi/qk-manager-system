package com.qk.manager.quarz.job;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.qk.manager.common.Constants;
import com.qk.manager.common.RatioCalculateUtil;
import com.qk.manager.quarz.service.OperateService;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.mapper.ItemMapper;
import com.qk.manager.task.service.TaskService;

/**
 * <p>Description: 添加好友job </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月30日   
 * @version 1.0
 */
@DisallowConcurrentExecution
@Component
public class AddFriendJob implements Job {
	
	@Resource(name="addFriendServiceImpl")
	private OperateService operateService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private ItemMapper itemMapper;
	
	private final static int MINUTES = 60;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		JobDataMap map = context.getJobDetail().getJobDataMap();
		Integer taskId = map.getInt(Constants.TASK_TAG);
		Task task = taskService.selectDataByTaskId(taskId);
		List<Item> itemLst = itemMapper.selectByTaskId(task.getId());
		task.setItemList(itemLst);
		Map<String, String> itemMap = task.getItemMap();
		
		if(itemMap.get("serviceTaskId")==null||"".equals(itemMap.get("serviceTaskId").toString())) {
			operateService.operate(context.getJobDetail().getJobDataMap());
			return;
		}
		
		String yhTaskId = itemMap.get("serviceTaskId").toString();
		List<Item> yhItemLst = itemMapper.selectByTaskId(Integer.valueOf(yhTaskId));
		Task yhTask = new Task();
		yhTask.setItemList(yhItemLst);
		Date date = new Date();
		float hour = date.getHours()+(date.getMinutes() != 0 ? (float)date.getMinutes()/MINUTES : 0);
	    String range = RatioCalculateUtil.getRatioRange(hour,RatioCalculateUtil.getRegionMap(yhTask.getItemMap()));	
	    
	    String[] spt = range.split(":");
	    //根据不同的时段获取不同概率，根据概率是否发送任务
	    boolean isSend = RatioCalculateUtil.calculate(Float.valueOf(spt[0]), Float.valueOf(spt[1]));
		if(!isSend) {
			if(0 == task.getState()) {
				taskService.updateStateReaById("执行中", "1", task.getId());
			}
           logger.info("加好友任务不发送"); 
           return; 
        }
		operateService.operate(context.getJobDetail().getJobDataMap());
	}

}
