package com.qk.manager.quarz.job;

import javax.annotation.Resource;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.qk.manager.quarz.service.OperateService;

/**
 * 
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author administrator   
 * @date 2019年9月25日   
 * @version 1.0
 */
public class PullQQFriendToGroupJob implements Job {
	
	@Resource(name="pullQQFriendToGroupServiceImpl")
	private OperateService operateService;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		operateService.operate(context.getJobDetail().getJobDataMap());
	}
}
