package com.qk.manager.quarz.job;

import javax.annotation.Resource;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import com.qk.manager.quarz.service.OperateService;

/**
 * 
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年10月11日   
 * @version 1.0
 */
@DisallowConcurrentExecution
@Component
public class WXCheckAccoutIsAddJob implements Job {

	@Resource(name="WXFriendCheckIntfImpl")
	private OperateService operateService;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		operateService.operate(context.getJobDetail().getJobDataMap());
	}

}
