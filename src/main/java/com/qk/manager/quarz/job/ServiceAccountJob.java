package com.qk.manager.quarz.job;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.qk.manager.common.Constants;
import com.qk.manager.common.RatioCalculateUtil;
import com.qk.manager.common.SpringBeanFactory;
import com.qk.manager.quarz.service.OperateService;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.mapper.ItemMapper;
import com.qk.manager.task.service.TaskService;
/**
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年8月19日   
 * @version 1.0
 */

@DisallowConcurrentExecution
@Component
public class ServiceAccountJob implements Job {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private TaskService taskService;

	@Autowired
	private ItemMapper itemMapper;
	
	private final static int MINUTES = 60;
	
	@Override
	@SuppressWarnings("deprecation")
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap map = context.getJobDetail().getJobDataMap();
		Integer taskId = map.getInt(Constants.TASK_TAG);
		Task task = taskService.selectDataByTaskId(taskId);
		List<Item> itemLst = itemMapper.selectByTaskId(task.getId());
		task.setItemList(itemLst);
		Map<String, String> itemMap = task.getItemMap();
		
		Date date = new Date();
		float hour = date.getHours()+(date.getMinutes() != 0 ? (float)date.getMinutes()/MINUTES : 0);
	    String range = RatioCalculateUtil.getRatioRange(hour,RatioCalculateUtil.getRegionMap(itemMap));	
	    
	    String[] spt = range.split(":");
	    //根据不同的时段获取不同概率，根据概率是否发送任务
	    boolean isSend = RatioCalculateUtil.calculate(Float.valueOf(spt[0]), Float.valueOf(spt[1]));
		if(!isSend) {
			if(0 == task.getState()) {
				taskService.updateStateReaById("执行中", "1", task.getId());
			}
           logger.info("养号不发送任务"); 
           return; 
        }
		
		//任务是否被选中配置
		String taskChecked = itemMap.get("taskChecked");
		String beanName = RatioCalculateUtil.calculateAct(taskChecked,RatioCalculateUtil.getActionRatio(itemMap));
		if("".equals(beanName)) {
			logger.info("没有筛选到的bean不执行任务");
			return ;
		}
		OperateService operateService = (OperateService) SpringBeanFactory.getBean(beanName);
		operateService.operate(context.getJobDetail().getJobDataMap());
	}
	
}
