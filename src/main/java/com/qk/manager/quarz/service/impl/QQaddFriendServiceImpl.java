package com.qk.manager.quarz.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.qk.manager.common.AESUtils;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.Constants;
import com.qk.manager.common.DataUtil;
import com.qk.manager.common.DateFormateUtil;
import com.qk.manager.common.EncryptUtil;
import com.qk.manager.qqfriend.entity.QQAccountData;
import com.qk.manager.qqfriend.entity.QQTargetAccount;
import com.qk.manager.qqfriend.mapper.QQAccountDataMapper;
import com.qk.manager.qqfriend.mapper.QQTargetAccountMapper;
import com.qk.manager.quarz.service.OperateService;
import com.qk.manager.task.entity.DeviceTask;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.mapper.DeviceTaskMapper;
import com.qk.manager.task.mapper.ItemMapper;
import com.qk.manager.task.service.TaskService;
import com.qk.manager.websocket.server.WebSocketServer;

/**
 * 
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author administrator  
 * @date 2019年9月20日   
 * @version 1.0
 */
@Service
public class QQaddFriendServiceImpl implements OperateService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DeviceTaskMapper deviceTaskMapper;

	@Autowired
	private TaskService taskService;

	@Autowired
	private ItemMapper itemMapper;
	
	@Autowired
	private QQAccountDataMapper qqAccountDataMapper;
	
	@Autowired
	private QQTargetAccountMapper qqTargetAccountMapper;

	@Override
	public void operate(JobDataMap map) {
		
		Integer taskId = map.getInt(Constants.TASK_TAG);
		Task task = taskService.selectDataByTaskId(taskId);
		boolean isFinish = true;
		// 执行成功不执行
		if (task.getState() == 2) {
			return;
		}
		
		List<DeviceTask> lst = selectByTaskId(taskId);
		StringBuilder sbd = new StringBuilder("");
		for (DeviceTask deviceTask : lst) {
			// 执行成功的小任务不再执行
			if (deviceTask.getState() == 1) {
				continue;
			}
			
			JSONObject jsonObject = toJson(deviceTask, task);
			try {
				String sid = task.getUserId() + "&" + deviceTask.getDeviceNum();
				if(!WebSocketServer.isExist(sid)) {
				  logger.info("设备"+deviceTask.getDeviceNum()+"未在线");
				  sbd.append("设备"+deviceTask.getDeviceNum()+"未在线").append("\r\n"); 
				  continue; 
				}
				WebSocketServer.sendInfo(DataUtil.setBaseData(CodeUtil.SUCCESS_CODE, "获取任务成功", 1, 1, jsonObject),
						sid);
				isFinish = false;
			} catch (IOException e) {
				isFinish = false;
				logger.error("向客户端发送消息失败", e);
			}
		}
		// 执行失败/待执行的任务更新任务状态
		if ((task.getState() == 3 || task.getState() == 0)) {
			String str = sbd.toString();
			String state = "3";
			if ("".equals(str)) {
				state = "2";
			}
			taskService.updateStateReaById(sbd.toString(), isFinish ? state : "1", taskId);
		}
	}
	
	
	/**
	 * Description: 数据组装
	 * 
	 * @param deviceTask
	 * @param task
	 * @param acc
	 * @return date 2019年7月31日
	 */
	private JSONObject toJson(DeviceTask deviceTask, Task task) {

		JSONObject jsonObject = new JSONObject();
		Map<String, Object> dataMap = new HashMap<String, Object>();

		dataMap.put("taskId", task.getId());
		dataMap.put("taskType", task.getTaskType());

		List<Item> itemLst = itemMapper.selectByTaskId(task.getId());
		task.setItemList(itemLst);
		Map<String, String> itemMap = task.getItemMap();

		List<QQAccountData> aDataList = qqAccountDataMapper.queryAccountData(deviceTask.getDeviceNum()+"&"+task.getId(),task.getUserId());
		List<Map<String, String>> friendList = new ArrayList<Map<String, String>>();
		int type = Integer.parseInt(itemMap.get("type").trim());
		int applyType = Integer.parseInt(itemMap.get("applyType").trim());
		String[] applyTexts = itemMap.get("applyText").split("\\\n");
		// 延迟时间
		Integer minTime = Integer.parseInt(itemMap.get("minTime") == null ? "0" : itemMap.get("minTime").trim());
		Integer maxTime = Integer.parseInt(itemMap.get("maxTime") == null ? "0" : itemMap.get("maxTime").trim());
		String date = DateFormateUtil.getDateStr(new Date(), DateFormateUtil.YYYYMMDD);
		Random random = new Random();
		
		for (QQAccountData data : aDataList) {
			Map<String, String> friendMap = new HashMap<String, String>();
			friendMap.put("account", data.getAccount());
			Integer time = (random.nextInt(maxTime - minTime + 1) + minTime) * 1000;
			friendMap.put("sleepTime", time + "");
			// 备注类型
			if (type == 0) {
				friendMap.put("remarkName", "");
			} else if (type == 1) {
				friendMap.put("remarkName", date + "_" + data.getId());
			} else if (type == 2) {
				friendMap.put("remarkName", data.getRemark());
			}
			// 申请语类型
			if (applyType == 0) {
				friendMap.put("applyText", applyTexts[random.nextInt(applyTexts.length)]);
			} else {
				friendMap.put("applyText", data.getApplicationTerms());
			}
			friendList.add(friendMap);
		}
		dataMap.put("friendList", friendList);
		dataMap.put("qqTargetData", getQQTargetAccount(task.getUserId()));
		jsonObject.putAll(dataMap);
		return jsonObject;
	}

	/**
	 * Description: 查询绑定设备
	 * 
	 * @param taskId
	 * @return date 2019年7月31日
	 */
	private List<DeviceTask> selectByTaskId(Integer taskId) {
		return deviceTaskMapper.selectByTaskId(taskId);
	}
	
	/**
	 * Description:  获取qq目标账号  
	 * @param userId 
	 * @return 
	 * date 2019年10月10日
	 */
	private Map<String,String> getQQTargetAccount(Integer userId) {
		List<QQTargetAccount> lst = qqTargetAccountMapper.selectQQTargetAccountData(userId);
		Map<String,String> map = new HashMap<String,String>();
		for(QQTargetAccount account : lst) {
			map.put(account.getQqAccount() ,account.getPwd());
		}
		return map;
	}
}
