package com.qk.manager.quarz.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.qk.manager.aop.SynCtrl;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.Constants;
import com.qk.manager.common.DataUtil;
import com.qk.manager.common.FileTypeUtil;
import com.qk.manager.device.entity.Account;
import com.qk.manager.device.service.AccountService;
import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.quarz.service.OperateService;
import com.qk.manager.task.entity.DeviceTask;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.mapper.DeviceTaskMapper;
import com.qk.manager.task.mapper.ItemMapper;
import com.qk.manager.task.service.TaskService;
import com.qk.manager.websocket.server.WebSocketServer;

@Service
public class FriendCyclePulishServiceImpl implements OperateService  {

private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 *  获取任务配置
	 */
	@Autowired
	private ItemMapper itemMapper;
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private DeviceTaskMapper deviceTaskMapper;
	
	@Autowired
	private AccountService accountService;

	@Override
	@SynCtrl(syn=true,sleep=30,isWait=true)
	public void operate(JobDataMap map) {
		logger.info("【养号】发朋友任务");
		Integer taskId = map.getInt(Constants.TASK_TAG);
		Task task = taskService.selectDataByTaskId(taskId);
		List<Item> itemlst = itemMapper.selectByTaskId(taskId);
		task.setItemList(itemlst);
		boolean isFinish = true;
		// 执行成功不执行
		if (task.getState() == 2) {
			return;
		}
		StringBuilder sbd = new StringBuilder("");
		// 每次获取发送消息的目标数-随机
		List<DeviceTask> lst = selectByTaskId(taskId);
		// 循环每个小任务
		for (int i = 0; i < lst.size(); i++) {
			// 向websocket发送消息
			sendWebSocket(lst.get(i), task, isFinish, sbd);
		}
	}

	/**
	 * Description: 给websocket发送消息
	 * 
	 * @param deviceTask
	 * @param acc        date 2019年8月15日
	 */
	private void sendWebSocket(DeviceTask deviceTask, Task task, boolean isFinish, StringBuilder sbd) {
		List<Account> acclst = accountService.selectAccByDeviceId(task.getUserId(),deviceTask.getDeviceId());
		if (acclst == null || acclst.size() == 0) {
			logger.info("未找到设备" + deviceTask.getDeviceId());
			return;
		}
		Account acc = acclst.get(0);
		if(acc.getState() == 0) {
			logger.info("账号不在线");
			return;
		}
		JSONObject jsonObject = toJson(deviceTask, task, acc);
		try {
			String sid = task.getUserId() + "&" + deviceTask.getDeviceNum();
			if (!WebSocketServer.isExist(sid)) {
				logger.info("设备" + deviceTask.getDeviceNum() + "未在线");
				sbd.append("设备" + deviceTask.getDeviceNum() + "未在线")
						.append("\r\n");
				return;
			}
			WebSocketServer.sendInfo(DataUtil.setBaseData(CodeUtil.SUCCESS_CODE, "获取任务成功", 1, 0, jsonObject),
					sid);
			isFinish = false;
		} catch (IOException e) {
			isFinish = false;
			logger.error("向客户端发送消息失败", e);
		}
		// 执行失败/待执行的任务更新任务状态
		if ((task.getState() == 3 || task.getState() == 0)) {
			String str = sbd.toString();
			String state = "3";
			if ("".equals(str)) {
				state = "2";
			}
			taskService.updateStateReaById(sbd.toString(), isFinish ? state : "1", task.getId());
		}
	}

	/**
	 * Description: 转成json
	 * @param deviceTask 设备任务对象
	 * @param task 任务对象
	 * @param acc 账号
	 * @param i
	 * @return 
	 * date 2019年8月21日
	 */
	private JSONObject toJson(DeviceTask deviceTask, Task task, Account acc) {
		JSONObject jsonObject = new JSONObject();
		Map<String, Object> targetMap = new HashMap<String, Object>();
		String taskType = "";
		targetMap.put("taskId", task.getId());
		
		String momentsMsg = task.getItemMap().get("momentsMsg") == null?"":task.getItemMap().get("momentsMsg");
		String momentsImg = task.getItemMap().get("momentsImg") == null?"":task.getItemMap().get("momentsImg");
		int length = momentsMsg.split(",",4).length;
        Random random = new Random();
        int index = random.nextInt(length);
        
        String msg = momentsMsg.split(",",4)[index];
        String sourceUrl = momentsImg.split(",",4)[index];
        targetMap.put("msg", momentsMsg.split(",",4)[index]);
        targetMap.put("imgUrl",momentsImg.split(",",4)[index]);
        if( !"".equals(msg) && "".equals(sourceUrl)) {
        	taskType =  TaskTypeEnum.MOMENTS_SEND_TEXT.getCode();
        }
        if(!"".equals(sourceUrl)){
        	if(FileTypeUtil.isImage(sourceUrl)) {
        		taskType =  TaskTypeEnum.MOMENTS_SEND_IMG.getCode();     		
        		targetMap.put("imgUrl", sourceUrl);
        	}
        	
        	if(FileTypeUtil.isVideo(sourceUrl)) {
        		taskType =  TaskTypeEnum.MOMENTS_SEND_VIDEO.getCode();
        		targetMap.put("videoUrl", sourceUrl);
        	}
        }
        
        targetMap.put("taskType", taskType);
		targetMap.put("taskId", task.getId());
		targetMap.put("msg", msg);
		jsonObject.putAll(targetMap);
		return jsonObject;
	}

	/**
	 * Description: 查询绑定设备
	 * 
	 * @param taskId
	 * @return date 2019年7月31日
	 */
	private List<DeviceTask> selectByTaskId(Integer taskId) {
		return deviceTaskMapper.selectByTaskId(taskId);
	}
	
}
