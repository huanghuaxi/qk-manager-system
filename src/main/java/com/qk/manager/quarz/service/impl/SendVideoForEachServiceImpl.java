package com.qk.manager.quarz.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.qk.manager.aop.SynCtrl;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.Constants;
import com.qk.manager.common.DataUtil;
import com.qk.manager.device.entity.Account;
import com.qk.manager.device.mapper.AccountMapper;
import com.qk.manager.device.service.AccountService;
import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.quarz.service.OperateService;
import com.qk.manager.task.entity.DeviceTask;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.mapper.DeviceTaskMapper;
import com.qk.manager.task.mapper.ItemMapper;
import com.qk.manager.task.service.TaskService;
import com.qk.manager.websocket.server.WebSocketServer;

/**
 * <p>Description: 发送视频任务 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年8月19日   
 * @version 1.0
 */
@Service
public class SendVideoForEachServiceImpl implements OperateService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 *  获取任务配置
	 */
	@Autowired
	private ItemMapper itemMapper;
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private DeviceTaskMapper deviceTaskMapper;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Override
	@SynCtrl(syn=true,sleep=30,isWait=true)
	public void operate(JobDataMap map) {
 		logger.info("【养号】发送视频任务");
		Integer taskId = map.getInt(Constants.TASK_TAG);
		Task task = taskService.selectDataByTaskId(taskId);
		List<Item> itemlst = itemMapper.selectByTaskId(taskId);
		task.setItemList(itemlst);
	    boolean isFinish = true;
		//执行成功不执行
		if(task.getState()==2) {
			return ;
		}
		StringBuilder sbd = new StringBuilder("");
	    //每次获取发送消息的目标数-随机
	    List<DeviceTask> lst = selectByTaskId(taskId);
	    List<Integer> deviceIdLst = selectTargetFriendByAccAndPhone(task.getUserId(),task.getItemMap().get(Constants.TARGET_FRIEND_KEY), lst);
	    List<DeviceTask>  dtLst = deviceTaskMapper.selectByTaskIdAndDeviceId(taskId,deviceIdLst);
	    List<DeviceTask> deviceList = order(dtLst, task.getUserId(),task.getItemMap().get(Constants.TARGET_FRIEND_KEY));
	    int lstNum = deviceList.size()%2;
		// 循环每个小任务
		for (int i = 0; i < (lstNum == 0 ? deviceList.size():deviceList.size() -1) ; i++) {
	    	//向websocket发送消息
	    	sendWebSocket(deviceList.get(i),task,isFinish,sbd,i); 
	    }
	}
	
	/**
	 * Description: 给websocket发送消息
	 * @param deviceTask
	 * @param acc 
	 * date 2019年8月15日
	 */
	private void sendWebSocket(DeviceTask deviceTask,Task task,boolean isFinish,StringBuilder sbd,int i) {
    	List<Account> acclst = accountService.selectAccByDeviceId(task.getUserId(),deviceTask.getDeviceId());
    	if (acclst == null || acclst.size() == 0) {
			logger.info("未找到设备" + deviceTask.getDeviceId());
			return ;
		}
		Account acc = acclst.get(0);
		if(acc.getState() == 0) {
			logger.info("账号不在线");
			return;
		}
		JSONObject jsonObject = toJson(deviceTask,task,acc,i);
    	try {
    		String sid = task.getUserId() + "&" + deviceTask.getDeviceNum();
			if(!WebSocketServer.isExist(sid)) {
			  logger.info("设备"+deviceTask.getDeviceNum()+"未在线");
			  sbd.append("设备"+deviceTask.getDeviceNum()+"未在线").append("\r\n"); 
			}else {
				WebSocketServer.sendInfo(DataUtil.setBaseData(CodeUtil.SUCCESS_CODE, "获取任务成功", 1, 0, jsonObject),
						sid);
				isFinish = false;
			}
		} catch (IOException e) {
			isFinish = false;
			logger.error("向客户端发送消息失败", e);
		}
    	// 执行失败/待执行的任务更新任务状态
		if ((task.getState() == 3 || task.getState() == 0)) {
			String str = sbd.toString();
			String state = "3";
			if ("".equals(str)) {
				state = "2";
			}
			taskService.updateStateReaById(sbd.toString(), isFinish ? state : "1", task.getId());
		}
	}
	
	
	/**
	 * Description:     
	 * date 2019年8月14日
	 */
	private static String memery = "";
	private static Integer callTime = 0;
	private JSONObject toJson(DeviceTask deviceTask, Task task,Account acc,int i) {
		JSONObject jsonObject = new JSONObject();
		Map<String, Object> targetMap = new HashMap<String, Object>();
		int ls = i%2;
		String taskType = "";
		Integer minTime = Integer.parseInt(task.getItemMap().get("videoMinTime"));
		Integer maxTime = Integer.parseInt(task.getItemMap().get("videoMaxTime"));
		//基数为发送方
		if(ls != 0) {
			taskType = TaskTypeEnum.SEND_VIDEO_CALL.getCode();
			targetMap.put("targetAcc", memery);
		}
		//偶数接收送方
		if(ls==0) {
			taskType = TaskTypeEnum.ACCEPT_VIDEO_CALL.getCode();
			memery = acc.getPhone();
			Random random = new Random();
			callTime = random.nextInt(maxTime - minTime) + minTime;
		}
		targetMap.put("taskType", taskType);
		targetMap.put("taskId", task.getId());
		
		targetMap.put("callTime", callTime);
		targetMap.put("minTime", minTime);
		targetMap.put("maxTime", maxTime);
    	jsonObject.putAll(targetMap);
		return jsonObject;
	}
	
	/**
	 * Description: 查询绑定设备
	 * 
	 * @param taskId
	 * @return date 2019年7月31日
	 */
	private List<DeviceTask> selectByTaskId(Integer taskId) {
		return deviceTaskMapper.selectByTaskId(taskId);
	}
	
	/**
	 * Description:选择目标    
	 * @param targetFriend
	 * @param lst
	 * @return 
	 * date 2019年9月11日
	 */
	private List<Integer> selectTargetFriendByAccAndPhone(Integer uid,String targetFriend,List<DeviceTask> lst){
		List<String> accList =  Arrays.asList(targetFriend.split(";"));
		List<Account> accLst = accountMapper.selectTargetFriendByAccAndPhone(uid,accList);
		List<DeviceTask> lstTemp = new ArrayList<DeviceTask>(lst);

		List<Integer> deviceIdLst = new ArrayList<Integer>();
		for(Account acc: accLst) {
			deviceIdLst.add(acc.getDeviceId());
			for(DeviceTask deviceTask:lst) {
				if(acc.getDeviceId().intValue() == deviceTask.getDeviceId().intValue()) {
					lstTemp.remove(deviceTask);
				}
			}
		}
		//随机获取账号
		if(lstTemp.size() > 0) {
			for(int i=0;i<accLst.size();i++) {
				if(lstTemp.size() > 0) {
					DeviceTask deviceTask = getDeviceTask(deviceIdLst,lstTemp);
					deviceIdLst.add(deviceTask.getDeviceId());
				}
				
			}
		}
		return deviceIdLst;
	}
	
	/**
	 * 
	 * Description:  递归寻找不一样的值
	 * @param deviceIdLst
	 * @param lstTemp
	 * @return 
	 * date 2019年9月11日
	 */
	private DeviceTask getDeviceTask(List<Integer> deviceIdLst,List<DeviceTask> lstTemp) {
		Random rand = new Random();
		int index = rand.nextInt(lstTemp.size());
		DeviceTask device = lstTemp.get(index);
		if(deviceIdLst.contains(device.getDeviceId())) {
			device = getDeviceTask(deviceIdLst,lstTemp);
		}
		lstTemp.remove(index);
		return device;
	}
	
	/**
	 * 
	 * Description: 排序
	 * @param deviceList
	 * @param uid
	 * @param targetFriend
	 * @return 
	 * date 2019年10月8日
	 */
	private List<DeviceTask> order(List<DeviceTask> deviceList, Integer uid, String targetFriend){
		List<String> accList =  Arrays.asList(targetFriend.split(";"));
		List<Account> accLst = accountMapper.selectTargetFriendByAccAndPhone(uid,accList);
		//打乱顺序
		Random random = new Random();
		List<Account> accLst1 = new ArrayList<Account>();
		while(accLst.size() > 0) {
			int num = random.nextInt(accLst.size());
			accLst1.add(accLst.get(num));
			accLst.remove(num);
		}
		List<DeviceTask> deviceList1 = new ArrayList<DeviceTask>(deviceList);
		List<DeviceTask> deviceList2 = new ArrayList<DeviceTask>();
		List<DeviceTask> deviceList3 = new ArrayList<DeviceTask>();
		for(Account acc: accLst1) {
			for(DeviceTask deviceTask:deviceList) {
				if(acc.getDeviceId().intValue() == deviceTask.getDeviceId().intValue()) {
					deviceList1.remove(deviceTask);
					deviceList2.add(deviceTask);
					break;
				}
			}
		}
		int index = 0;
		for(DeviceTask devictTask : deviceList2) {
			if(random.nextInt(2) == 0) {
				deviceList3.add(devictTask);
				if(index < deviceList1.size()) {
					deviceList3.add(deviceList1.get(index));	
				}
			}else {
				if(index < deviceList1.size()) {
					deviceList3.add(deviceList1.get(index));	
				}
				deviceList3.add(devictTask);
			}
			index++;
		}
		return deviceList3;
	}
}
