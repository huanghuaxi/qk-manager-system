package com.qk.manager.quarz.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.qk.manager.aop.SynCtrl;
import com.qk.manager.common.AESUtils;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.Constants;
import com.qk.manager.common.DataUtil;
import com.qk.manager.common.EncryptUtil;
import com.qk.manager.qqfriend.entity.QQTargetAccount;
import com.qk.manager.qqfriend.mapper.QQTargetAccountMapper;
import com.qk.manager.quarz.service.OperateService;
import com.qk.manager.task.entity.DeviceTask;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.mapper.DeviceTaskMapper;
import com.qk.manager.task.mapper.ItemMapper;
import com.qk.manager.task.service.TaskService;
import com.qk.manager.websocket.server.WebSocketServer;

@Service
public class PullQQFriendToGroupServiceImpl implements OperateService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DeviceTaskMapper deviceTaskMapper;

	@Autowired
	private TaskService taskService;
	
	@Autowired
	private ItemMapper itemMapper;
	
	@Autowired
	private QQTargetAccountMapper qqTargetAccountMapper;
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: Operate</p>   
	 * <p>Description: 组要操作类 </p>   
	 * @param map   
	 * @see com.qk.manager.quarz.service.OperateService#Operate(org.quartz.JobDataMap)
	 */
	@Override
	@SynCtrl(syn=true,sleep=30,isWait=true)
	public void operate(JobDataMap map) {
		
		Integer taskId = map.getInt(Constants.TASK_TAG);
		Task task = taskService.selectDataByTaskId(taskId);
		boolean isFinish = true;
		//执行成功不执行
		if(task.getState()==2) {
			return ;
		}
		StringBuilder sbd = new StringBuilder();
		List<DeviceTask> lst = selectByTaskId(taskId);
		for(DeviceTask deviceTask:lst) {
			//执行成功的小任务不再执行
		    if(deviceTask.getState()==1) {
		    	continue ;
		    }
			JSONObject jsonObject = toJson(deviceTask, task);
		  	try {
		  		String sid = task.getUserId() + "&" + deviceTask.getDeviceNum();
		  		if(!WebSocketServer.isExist(sid)) {
					logger.info("设备"+deviceTask.getDeviceNum()+"未在线");
					sbd.append("设备"+deviceTask.getDeviceNum()+"未在线").append("\r\n");
					continue;
				}
				WebSocketServer.sendInfo(DataUtil.setBaseData(CodeUtil.SUCCESS_CODE, "获取任务成功", 1, 1, jsonObject), 
						sid);
				isFinish = false;
			} catch (IOException e) {
				isFinish = false;
				logger.error("向客户端发送消息失败", e);
			}
		}
		//执行失败/待执行的任务更新任务状态
		if((task.getState() == 3 || task.getState() == 0)) {
			String str = sbd.toString();
			String state = "3";
			if("".equals(str)) {
			   state = "2";
			}
			taskService.updateStateReaById(sbd.toString(),isFinish? state:"1", taskId);
		}
	}
	
	
	/**
	 * Description: 数据组装    
	 * @param deviceTask
	 * @param task
	 * @param acc
	 * @return 
	 * date 2019年7月31日
	 */
	private JSONObject toJson(DeviceTask deviceTask, Task task) {
		JSONObject jsonObject = new JSONObject();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("taskId", task.getId());
		dataMap.put("taskType", task.getTaskType());
		List<Item> itemLst = itemMapper.selectByTaskId(task.getId());
		task.setItemList(itemLst);
		Map<String, String> itemMap = task.getItemMap();
		Map<String, String> pullToGroupMap = new HashMap<String, String>();
	    pullToGroupMap.put("type", itemMap.get("type"));
	    pullToGroupMap.put("groupName", itemMap.get("groupName"));
	    pullToGroupMap.put("invite", itemMap.get("invite"));
	    pullToGroupMap.put("noInvite", itemMap.get("noInvite"));
	    pullToGroupMap.put("memberNum", itemMap.get("memberNum"));
		dataMap.put("pullToGroupList", pullToGroupMap);
		dataMap.put("qqTargetData", getQQTargetAccount(task.getUserId()));
		jsonObject.putAll(dataMap);
		return jsonObject;
	}

	/**
	 * Description: 查询绑定设备
	 * 
	 * @param taskId
	 * @return date 2019年7月31日
	 */
	private List<DeviceTask> selectByTaskId(Integer taskId) {
		return deviceTaskMapper.selectByTaskId(taskId);
	}
	
	/**
	 * Description:  获取qq目标账号  
	 * @param userId 
	 * @return 
	 * date 2019年10月10日
	 */
	private Map<String,String> getQQTargetAccount(Integer userId) {
		List<QQTargetAccount> lst = qqTargetAccountMapper.selectQQTargetAccountData(userId);
		Map<String,String> map = new HashMap<String,String>();
		for(QQTargetAccount account : lst) {
			map.put(account.getQqAccount() ,account.getPwd());
		}
		return map;
	}
}
