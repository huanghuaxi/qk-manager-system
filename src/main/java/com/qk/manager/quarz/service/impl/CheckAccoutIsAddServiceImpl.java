package com.qk.manager.quarz.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.Constants;
import com.qk.manager.common.DataUtil;
import com.qk.manager.qqfriend.entity.QQAccountData;
import com.qk.manager.qqfriend.entity.QQTargetAccount;
import com.qk.manager.qqfriend.mapper.QQAccountDataMapper;
import com.qk.manager.qqfriend.mapper.QQTargetAccountMapper;
import com.qk.manager.quarz.service.OperateService;
import com.qk.manager.task.entity.DeviceTask;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.mapper.DeviceTaskMapper;
import com.qk.manager.task.mapper.ItemMapper;
import com.qk.manager.task.service.TaskService;
import com.qk.manager.websocket.server.WebSocketServer;

/**
 * 
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author administrator   
 * @date 2019年9月20日   
 * @version 1.0
 */
@Service
public class CheckAccoutIsAddServiceImpl implements OperateService  {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DeviceTaskMapper deviceTaskMapper;

	@Autowired
	private TaskService taskService;

	@Autowired
	private ItemMapper itemMapper;
	
	@Autowired
	private QQAccountDataMapper qqAccountDataMapper;
	
	@Autowired
	private QQTargetAccountMapper qqTargetAccountMapper;
	
	
	@Override
	public void operate(JobDataMap map) {
		Integer taskId = map.getInt(Constants.TASK_TAG);
 		Task task = taskService.selectDataByTaskId(taskId);
		List<Item> itemLst = itemMapper.selectByTaskId(task.getId());
		task.setItemList(itemLst);
        Map<String,String> itemMp = task.getItemMap();
		List<DeviceTask> lst = selectByTaskId(Integer.valueOf(itemMp.get("addFriendId")));
		StringBuilder sbd = new StringBuilder("");
		for (DeviceTask deviceTask : lst) {
			// 执行成功的小任务不再执行
			JSONObject jsonObject = toJson(deviceTask, task);
			try {
				String sid = task.getUserId() + "&" + deviceTask.getDeviceNum();
				if(!WebSocketServer.isExist(sid)) {
				  logger.info("设备"+deviceTask.getDeviceNum()+"未在线");
				  sbd.append("设备"+deviceTask.getDeviceNum()+"未在线").append("\r\n"); 
				  taskService.editDeviceTaskState(deviceTask.getDeviceNum(), taskId, 1);
				  continue; 
				}
				WebSocketServer.sendInfo(DataUtil.setBaseData(CodeUtil.SUCCESS_CODE, "获取任务成功", 1, 1, jsonObject),
						sid);
			} catch (IOException e) {
				taskService.updateStateReaById(sbd.toString(), "3", taskId);
				logger.error("向客户端发送消息失败", e);
			}
		}
		//更新任务状态
		taskService.updateStateReaById(sbd.toString(), "1", taskId);
	}
	
	
	/**
	 * Description: 数据组装
	 * 
	 * @param deviceTask
	 * @param task
	 * @param acc
	 * @return date 2019年7月31日
	 */
	private JSONObject toJson(DeviceTask deviceTask, Task task) {
		JSONObject jsonObject = new JSONObject();
		Map<String, Object> dataMap = new HashMap<String, Object>();

		dataMap.put("taskId", task.getId());
		dataMap.put("taskType", task.getTaskType());

		List<Item> itemLst = itemMapper.selectByTaskId(task.getId());
		task.setItemList(itemLst);
		Map<String, String> itemMap = task.getItemMap();

		dataMap.put("addFriendId", itemMap.get("addFriendId"));
		String taskId = itemMap.get("addFriendId");
		String msg = itemMap.get("message");
		dataMap.put("msg", msg);
		List<QQAccountData> aDataList = qqAccountDataMapper.queryAccountDataByState(deviceTask.getDeviceNum()+"&"+taskId,task.getUserId());
		if(aDataList != null && aDataList.size() > 0) {
			Map<String,JSONArray> map = new HashMap<String,JSONArray>();
			JSONArray jsonArray = new JSONArray();
			String targetAccount = aDataList.get(0).getTargetAccount();
			for (QQAccountData data : aDataList) {
				if(!targetAccount.equals(data.getTargetAccount())) {
					map.put(targetAccount, jsonArray);
					jsonArray = new JSONArray();
					targetAccount = data.getTargetAccount();
				}
				JSONObject object  = new JSONObject();
				object.put("account", data.getAccount());
				object.put("state", data.getState());
				jsonArray.add(object);
			}
			map.put(targetAccount, jsonArray);
			dataMap.put("account", map);
		}
		dataMap.put("qqTargetData", getQQTargetAccount(task.getUserId()));
		jsonObject.putAll(dataMap);
		return jsonObject;
	}
	
	/**
	 * Description: 查询绑定设备
	 * 
	 * @param taskId
	 * @return date 2019年7月31日
	 */
	private List<DeviceTask> selectByTaskId(Integer taskId) {
		return deviceTaskMapper.selectByTaskId(taskId);
	}
	
	/**
	 * Description:  获取qq目标账号  
	 * @param userId 
	 * @return 
	 * date 2019年10月10日
	 */
	private Map<String,String> getQQTargetAccount(Integer userId) {
		List<QQTargetAccount> lst = qqTargetAccountMapper.selectQQTargetAccountData(userId);
		Map<String,String> map = new HashMap<String,String>();
		for(QQTargetAccount account : lst) {
			map.put(account.getQqAccount() ,account.getPwd());
		}
		return map;
	}

}
