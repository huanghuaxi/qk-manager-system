package com.qk.manager.quarz.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.qk.manager.accountdata.entity.AccountData;
import com.qk.manager.accountdata.mapper.AccountDataMapper;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.Constants;
import com.qk.manager.common.DataUtil;
import com.qk.manager.device.entity.Account;
import com.qk.manager.device.mapper.AccountMapper;
import com.qk.manager.quarz.service.OperateService;
import com.qk.manager.task.entity.DeviceTask;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.mapper.DeviceTaskMapper;
import com.qk.manager.task.mapper.ItemMapper;
import com.qk.manager.task.service.TaskService;
import com.qk.manager.websocket.server.WebSocketServer;

@Service
public class WXFriendCheckIntfImpl implements OperateService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DeviceTaskMapper deviceTaskMapper;

	@Autowired
	private TaskService taskService;

	@Autowired
	private ItemMapper itemMapper;
	
	@Autowired
	private AccountDataMapper accountDataMapper;
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Override
	public void operate(JobDataMap map) {
		Integer taskId = map.getInt(Constants.TASK_TAG);
 		Task task = taskService.selectDataByTaskId(taskId);
		List<Item> itemLst = itemMapper.selectByTaskId(task.getId());
		task.setItemList(itemLst);
        Map<String,String> itemMp = task.getItemMap();
		List<DeviceTask> lst = selectByTaskId(Integer.valueOf(itemMp.get("addFriendId")));
		StringBuilder sbd = new StringBuilder("");
		for (DeviceTask deviceTask : lst) {
			// 执行成功的小任务不再执行
			JSONObject jsonObject = toJson(deviceTask, task);
			try {
				String sid = task.getUserId() + "&" + deviceTask.getDeviceNum();
				if(!WebSocketServer.isExist(sid)) {
				  logger.info("设备"+deviceTask.getDeviceNum()+"未在线");
				  sbd.append("设备"+deviceTask.getDeviceNum()+"未在线").append("\r\n"); 
				  taskService.editDeviceTaskState(deviceTask.getDeviceNum(), taskId, 1);
				  continue; 
				}
				WebSocketServer.sendInfo(DataUtil.setBaseData(CodeUtil.SUCCESS_CODE, "获取任务成功", 1, 1, jsonObject),
						sid);
			} catch (IOException e) {
				taskService.updateStateReaById(sbd.toString(), "3", taskId);
				logger.error("向客户端发送消息失败", e);
			}
		}
		//更新任务状态
		taskService.updateStateReaById(sbd.toString(), "1", taskId);
	}
	
	/**
	 * Description: 数据组装
	 * 
	 * @param deviceTask
	 * @param task
	 * @param acc
	 * @return date 2019年7月31日
	 */
	private JSONObject toJson(DeviceTask deviceTask, Task task) {
		JSONObject jsonObject = new JSONObject();
		Map<String, Object> dataMap = new HashMap<String, Object>();

		dataMap.put("taskId", task.getId());
		dataMap.put("taskType", task.getTaskType());

		List<Item> itemLst = itemMapper.selectByTaskId(task.getId());
		task.setItemList(itemLst);
		Map<String, String> itemMap = task.getItemMap();

		dataMap.put("addFriendId", itemMap.get("addFriendId"));
		String taskId = itemMap.get("addFriendId");
		String msg = itemMap.get("message");
		dataMap.put("msg", msg);
		List<Account> accounts = accountMapper.selectAccByDeviceId(task.getUserId(), deviceTask.getDeviceId());
		List<AccountData> aDataList = accountDataMapper.selectByAccountAndState(accounts.get(0).getAccount() + "&" + taskId);
		if(aDataList != null && aDataList.size() > 0) {
			JSONArray jsonArray = new JSONArray();
			for (AccountData data : aDataList) {
				JSONObject object  = new JSONObject();
				object.put("account", data.getAccountName());
				object.put("state", data.getState());
				jsonArray.add(object);
			}
			dataMap.put("account", jsonArray);
		}
		
		jsonObject.putAll(dataMap);
		return jsonObject;
	}
	
	/**
	 * Description: 查询绑定设备
	 * 
	 * @param taskId
	 * @return date 2019年7月31日
	 */
	private List<DeviceTask> selectByTaskId(Integer taskId) {
		return deviceTaskMapper.selectByTaskId(taskId);
	}

}
