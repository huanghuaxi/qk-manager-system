/**
 * 
 */
/**    
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月29日   
 * @version 1.0   
 */
package com.qk.manager.quarz.service;

import org.quartz.JobDataMap;

/**
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月30日   
 * @version 1.0
 */
public interface OperateService {
	 
	  /**
		* Description:    
		* @param map 
		* date 2019年7月30日
		*/
      public void operate(JobDataMap map);
	
}