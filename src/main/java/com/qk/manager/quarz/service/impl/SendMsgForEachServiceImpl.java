package com.qk.manager.quarz.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.qk.manager.aop.SynCtrl;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.Constants;
import com.qk.manager.common.DataUtil;
import com.qk.manager.device.entity.Account;
import com.qk.manager.device.mapper.AccountMapper;
import com.qk.manager.device.service.AccountService;
import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.message.mapper.MessageBaseMapper;
import com.qk.manager.quarz.service.OperateService;
import com.qk.manager.task.entity.DeviceTask;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.mapper.DeviceTaskMapper;
import com.qk.manager.task.mapper.ItemMapper;
import com.qk.manager.task.service.TaskService;
import com.qk.manager.websocket.server.WebSocketServer;

/**
 * <p>Description: 发送消息 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年8月13日   
 * @version 1.0
 */
@Service
public class SendMsgForEachServiceImpl implements OperateService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 *  获取任务配置
	 */
	@Autowired
	private ItemMapper itemMapper;
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private DeviceTaskMapper deviceTaskMapper;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private MessageBaseMapper messageBaseMapper;
	
	@Override
	@SynCtrl(syn=true,sleep=30,isWait=true)
	public void operate(JobDataMap map) {
		Integer taskId = map.getInt(Constants.TASK_TAG);
		Task task = taskService.selectDataByTaskId(taskId);
		List<Item> itemlst = itemMapper.selectByTaskId(taskId);
		task.setItemList(itemlst);
	    boolean isFinish = true;
		//执行成功不执行
		if(task.getState()==2) {
			return ;
		}
		StringBuilder sbd = new StringBuilder("");
	    //每次获取发送消息的目标数-随机
     	List<DeviceTask> lst = selectByTaskId(taskId);
	    
	    Random randomMsgNum = new Random();
	    int msgNum = randomMsgNum.nextInt(4) + 1;
	    //设备作为循环体像目标账号发送消息
	    String targetFriend = task.getItemMap().get(Constants.TARGET_FRIEND_KEY);
	    List<String> accList =  Arrays.asList(targetFriend.split(";"));
		List<Account> accLst1 = accountMapper.selectTargetFriendByAccAndPhone(task.getUserId(), accList);
	    List<Integer> deviceIdLst = selectTargetFriendByAccAndPhone(task.getUserId(),task.getItemMap().get(Constants.TARGET_FRIEND_KEY), lst);
	    if(deviceIdLst.size() > 0) {
			List<DeviceTask>  dtLst1 = deviceTaskMapper.selectByTaskIdAndDeviceId(taskId,deviceIdLst);
			if(accLst1.size() <= 0 || dtLst1.size() <= 0) {
		    	return;
		    }
		    for(DeviceTask deviceTask:dtLst1) {
		    	//向websocket 发送消息
		    	sendWebSocket(deviceTask,task,isFinish,sbd,accLst1, msgNum);
		    }
	    }
	    
	    
	    //目标账号作为循环体向设备发送消息
	    List<Integer> deviceIds = new ArrayList<Integer>();
	    List<String> deviceIdList = new ArrayList<String>();
	    
	    for(Account acc : accLst1) {
	    	deviceIds.add(acc.getDeviceId());
	    	deviceIdList.add(acc.getDeviceId().toString());
	    }
	    List<DeviceTask>  dtLst2 = deviceTaskMapper.selectByTaskIdAndDeviceId(taskId,deviceIds);
	    List<Account> accLst2 = accountMapper.selectByDevice(deviceIdList);
	    if(accLst2.size() <= 0) {
	    	return ;
	    }
	    for(DeviceTask deviceTask:dtLst2) {
	    	//向websocket 发送消息
	    	sendWebSocket(deviceTask,task,isFinish,sbd,accLst2, msgNum);
	    }
	}
	
	/**
	 * Description: 给websocket发送消息
	 * @param deviceTask
	 * @param acc 
	 * date 2019年8月15日
	 */
	private void sendWebSocket(DeviceTask deviceTask,Task task,boolean isFinish,StringBuilder sbd,List<Account> accLst, int msgNum) {
		List<Account> acclst = accountService.selectAccByDeviceId(task.getUserId(),deviceTask.getDeviceId());
		if (acclst == null || acclst.size() == 0) {
			logger.info("未找到设备" + deviceTask.getDeviceId());
			return ;
		}
		Account acc = acclst.get(0);
		if(acc.getState() == 0) {
			logger.info("账号不在线");
			return;
		}
    	JSONObject jsonObject = toJson(deviceTask,task,accLst, msgNum);
		try {
			String sid = task.getUserId() + "&" + deviceTask.getDeviceNum();
			if(!WebSocketServer.isExist(sid)) {
				  logger.info("设备"+deviceTask.getDeviceNum()+"未在线");
				  sbd.append("设备"+deviceTask.getDeviceNum()+"未在线").append("\r\n"); 
			} else {
				WebSocketServer.sendInfo(DataUtil.setBaseData(CodeUtil.SUCCESS_CODE, "获取任务成功", 1, 0, jsonObject),
						sid);
				isFinish = false;
			}
		} catch (IOException e) {
			isFinish = false;
			logger.error("向客户端发送消息失败", e);
		}
    	// 执行失败/待执行的任务更新任务状态
		if ((task.getState() == 3 || task.getState() == 0)) {
			String str = sbd.toString();
			String state = "3";
			if ("".equals(str)) {
				state = "2";
			}
			taskService.updateStateReaById(sbd.toString(), isFinish ? state : "1", task.getId());
		}
	}
	
	
	
	/**
	 * Description: 数据组装
	 * 
	 * @param deviceTask
	 * @param task
	 * @param acc
	 * @return date 2019年7月31日
	 */
	private JSONObject toJson(DeviceTask deviceTask, Task task,  List<Account> accLst, int msgNum) {
		
		JSONObject jsonObject = new JSONObject();
	    Random randomMenNum = new Random();
	    int menNum = randomMenNum.nextInt(3) + 1;
	    
		Map<String, Object> targetMap = new HashMap<String, Object>();
    	List<String> targetAcc = new ArrayList<String>();
    	if(accLst.size() < 3) {
    		menNum = accLst.size();
    	}
    	for(int i = 0;i < menNum;i++) {
    	  Account acc = get(accLst,deviceTask);
    	  if(!targetAcc.contains(acc.getPhone())) {
    		  targetAcc.add(acc.getPhone());
    	  }
    	  
    	}
    	targetMap.put("taskId", task.getId());
    	targetMap.put("taskType", TaskTypeEnum.SEND_MESSAGE.getCode());
    	//组织养好账号
    	targetMap.put("target", targetAcc);
    	//获取消息并组织成map
    	List<String> list = getMsg(task.getUserId());
    	//获取消息库消息-随机
    	targetMap.put("sendMsg", list);
    	targetMap.put("minTime",task.getItemMap().get("smgMinTime"));
    	targetMap.put("maxTime", task.getItemMap().get("smgMaxTime"));
    	targetMap.put("msgNum", msgNum);
    	jsonObject.putAll(targetMap);
		return jsonObject;
	}
	
	
	
	/**
	 * Description: 递增获取account 对象
	 * @param accLst
	 * @param deviceTask
	 * @return 
	 * date 2019年8月14日
	 */
	public Account get(List<Account> accLst, DeviceTask deviceTask) {
		Random rad = new Random();
   	    int index = rad.nextInt(accLst.size());
   	    Account acc = accLst.get(index);
   	    if(deviceTask.getDeviceId() == acc.getDeviceId()) {
   	      acc = get(accLst,deviceTask);
   	    }
   	    return acc;
	}
	
	/**
	 * Description: 获取发送消息内容
	 * @return 
	 * date 2019年8月14日
	 */
	public List<String> getMsg(Integer userId){
		List<Integer> userIds = new ArrayList<Integer>();
		userIds.add(userId);
		int count = messageBaseMapper.countMessage(userIds);
		if(count == 0) {
			return new ArrayList<String>();
		}
		int num = count%15;
		int page = 0;
		page = count/15;
		if(num != 0) {
		  page = page + 1;
		}
		Random pageR = new Random();
		int pageRv =  pageR.nextInt(page);
     	int from  = pageRv * 15;
     	List<String> msgLst = messageBaseMapper.selectByPageForString(from, 15,userIds);
		return msgLst;
	}
	
	/**
	 * Description: 查询绑定设备
	 * 
	 * @param taskId
	 * @return date 2019年7月31日
	 */
	private List<DeviceTask> selectByTaskId(Integer taskId) {
		return deviceTaskMapper.selectByTaskId(taskId);
	}
	
	/**
	 * Description:选择目标    
	 * @param targetFriend
	 * @param lst
	 * @return 
	 * date 2019年9月11日
	 */
	private List<Integer> selectTargetFriendByAccAndPhone(Integer uid,String targetFriend,List<DeviceTask> lst){
		List<String> accList =  Arrays.asList(targetFriend.split(";"));
		List<Account> accLst = accountMapper.selectTargetFriendByAccAndPhone(uid,accList);
		List<DeviceTask> lstTemp = new ArrayList<DeviceTask>(lst);
		
		List<Integer> deviceIdLst = new ArrayList<Integer>();
		for(Account acc: accLst) {
			for(DeviceTask deviceTask:lst) {
				if(acc.getDeviceId().intValue() == deviceTask.getDeviceId().intValue()) {
					lstTemp.remove(deviceTask);
				}
			}
		}
		for(DeviceTask deviceTask : lstTemp) {
			deviceIdLst.add(deviceTask.getDeviceId());
		}
		return deviceIdLst;
	}
}
