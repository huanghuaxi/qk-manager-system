package com.qk.manager.quarz.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.qk.manager.aop.SynCtrl;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.Constants;
import com.qk.manager.common.DataUtil;
import com.qk.manager.device.entity.Account;
import com.qk.manager.device.service.AccountService;
import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.quarz.service.OperateService;
import com.qk.manager.task.entity.DeviceTask;
import com.qk.manager.task.entity.Item;
import com.qk.manager.task.entity.Task;
import com.qk.manager.task.mapper.DeviceTaskMapper;
import com.qk.manager.task.mapper.ItemMapper;
import com.qk.manager.task.service.TaskService;
import com.qk.manager.websocket.server.WebSocketServer;

/**
 * <p>Description: 阅读消息 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月30日   
 * @version 1.0
 */
@Service
public class ReadMessageServiceImpl implements OperateService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private DeviceTaskMapper deviceTaskMapper;

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private TaskService taskService;
	/**
	 *  获取任务配置
	 */
	@Autowired
	private ItemMapper itemMapper;
	
	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: Operate</p>   
	 * <p>Description: 组要操作类 </p>   
	 * @param map   
	 * @see com.qk.manager.quarz.service.OperateService#Operate(org.quartz.JobDataMap)
	 */
	@Override
	@SynCtrl(syn=true,sleep=30,isWait=true)
	public void operate(JobDataMap map) {
		Integer taskId = map.getInt(Constants.TASK_TAG);
		Task task = taskService.selectDataByTaskId(taskId);
		List<Item> itemlst = itemMapper.selectByTaskId(taskId);
		task.setItemList(itemlst);
	    boolean isFinish = true;
		//执行成功不执行
		if(task.getState()==2) {
			return ;
		}
		StringBuilder sbd = new StringBuilder();
		List<DeviceTask> lst = selectByTaskId(taskId);
		for(DeviceTask deviceTask:lst) {
			try {
				//执行成功的小任务不再执行
//			    if(deviceTask.getState()==1) {
//			    	continue;
//			    }
				List<Account> acclst = accountService.selectAccByDeviceId(task.getUserId(),deviceTask.getDeviceId());
				if (acclst == null || acclst.size() == 0) {
					logger.info("未找到设备" + deviceTask.getDeviceId());
					continue;
				}
				Account acc = acclst.get(0);
				if(acc.getState() == 0) {
					logger.info("账号不在线");
					return;
				}
				String sid = task.getUserId() + "&" + deviceTask.getDeviceNum();
				JSONObject jsonObject = toJson(deviceTask,task);
		  		if(!WebSocketServer.isExist(sid)) {
					logger.info("设备"+deviceTask.getDeviceNum()+"未在线");
					sbd.append("设备"+deviceTask.getDeviceNum()+"未在线").append("\r\n");
					continue;
				}
				WebSocketServer.sendInfo(DataUtil.setBaseData(CodeUtil.SUCCESS_CODE, "获取任务成功", 1, 0, jsonObject), sid);
				isFinish = false;
			} catch (IOException e) {
				isFinish = false;
				logger.error("向客户端发送消息失败", e);
			}
		}
		//执行失败/待执行的任务更新任务状态
//		if((task.getState() == 3 || task.getState() == 0)) {
//			String str = sbd.toString();
//			String state = "3";
//			if("".equals(str)) {
//			   state = "2";
//			}
//			taskService.updateStateReaById(sbd.toString(),isFinish? state:"1", taskId);
//		}
	}
	
	/**
	 * Description: 数据组装    
	 * @param deviceTask
	 * @param task
	 * @param acc
	 * @return 
	 * date 2019年7月31日
	 */
	private JSONObject toJson(DeviceTask deviceTask, Task task) {

		JSONObject jsonObject = new JSONObject();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.put("taskId", task.getId());
		dataMap.put("taskType", TaskTypeEnum.READ_MESSAGE.getCode());
		jsonObject.putAll(dataMap);
		return jsonObject;
	}

	/**
	 * Description: 查询绑定设备
	 * 
	 * @param taskId
	 * @return date 2019年7月31日
	 */
	private List<DeviceTask> selectByTaskId(Integer taskId) {
		return deviceTaskMapper.selectByTaskId(taskId);
	}

}
