package com.qk.manager.quarz.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * <p>Description: 任务实例   </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年7月30日   
 * @version 1.0
 */
public class TaskInfo implements Serializable {
	
private static final long serialVersionUID = -8054692082716173379L;
	
	/**
	 * 增加或修改标识
	 */
	private int id;
 
	/**
	 * 任务名称
	 */
	private String jobName;
	
	/**
	 * 任务分组
	 */
	private String jobGroup;
	
	/**
	 * 任务描述
	 */
	private String jobDescription;
	
	/**
	 * 任务状态
	 */
	private String jobStatus;
	
	/**
	 * 任务表达式
	 */
	private String cronExpr;
	
	/**
	 * 创建时间
	 */
	private String createTime;
	
	/**
	 * 任务类型
	 */
	private String taskType;

	/**
	 * 任务id
	 */
	private Integer taskId;
	
	/**
	 * 开始时间
	 */
	private Date startTime;

 
	public String getJobName() {
		return jobName;
	}
 
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
 
	
	public String getJobGroup() { 
		  return jobGroup; 
	}
	  
	public void setJobGroup(String jobGroup) { 
		this.jobGroup = jobGroup; 
	}
	 
 
	public String getJobDescription() {
		return jobDescription;
	}
 
	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}
 
	public String getJobStatus() {
		return jobStatus;
	}
 
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
 
	public String getCronExpr() {
		return cronExpr;
	}
 
	public void setCronExpression(String cronExpr) {
		this.cronExpr = cronExpr;
	}
 
	public String getCreateTime() {
		return createTime;
	}
 
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
 
	public int getId() {
		return id;
	}
 
	public void setId(int id) {
		this.id = id;
	}
	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
}

