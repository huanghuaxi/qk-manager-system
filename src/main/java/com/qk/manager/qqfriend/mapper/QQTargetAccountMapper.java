package com.qk.manager.qqfriend.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.qk.manager.qqfriend.entity.QQTargetAccount;

/**
 * 
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author admin   
 * @date 2019年10月9日   
 * @version 1.0
 */
@Mapper
public interface QQTargetAccountMapper {
	
    /**
     * 
     * Description: 根据主键删除信息    
     * @param id
     * @return 
     * date 2019年10月9日
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 
     * Description:  插入信息
     * @param record
     * @return 
     * date 2019年10月9日
     */
    int insert(QQTargetAccount record);

    /**
     * 
     * Description: 更具主键选择    
     * @param id
     * @return 
     * date 2019年10月9日
     */
    QQTargetAccount selectByPrimaryKey(Integer id);

    /**
     * 
     * Description: 选择所有    
     * @return 
     * date 2019年10月9日
     */
    List<QQTargetAccount> selectAll();

    /**
     * 
     * Description: 更具主键更新信息    
     * @param record 主体对象
     * @return 
     * date 2019年10月9日
     */
    int updateByPrimaryKey(QQTargetAccount record);
    
    /**
     * Description: 查找好友库    
     * @param accountData
     * @param start
     * @param pageSize
     * @return 
     * date 2019年7月25日
     */
    List<QQTargetAccount> selectByPage(@Param("accountData") QQTargetAccount accountData, Integer start, Integer pageSize
    		, @Param("userIds") List<Integer> userIds);
    
    /**
     * Description: 统计好友库   
     * @param accountData
     * @return 
     * date 2019年7月25日
     */
    Integer countAccountData(@Param("accountData") QQTargetAccount accountData, @Param("userIds") List<Integer> userIds);
    
    /**
     * Description:  批量插入信息  
     * @param list 集合
     * date 2019年10月10日
     */
    void insertBatch(@Param("list") List<QQTargetAccount> list);
    
    /**
     * Description: 统计个数    
     * @param qqAccount qq 账号
     * @return 
     * date 2019年10月10日
     */
    Integer isExistByQQaccount(@Param("qqAccount") String qqAccount);
    
    /**
     * Description: 更新QQ目标号   
     * @param accountData 
     * date 2019年10月10日
     */
    void updateTargetQQById(@Param("accountData") QQTargetAccount accountData);
    
    /**
     * Description: 批量删除目标QQ    
     * @param list 
     * date 2019年10月10日
     */
    void delByBatchQQTarget(@Param("list") List<String> list);
    
    /**
     * Description: 查找    
     * @return 
     * date 2019年10月10日
     */
    List<QQTargetAccount> selectQQTargetAccountData(Integer userId);
    
    /**
     * 
     * Description: 根据QQ账号获取数据
     * @param qqAccount
     * @return 
     * date 2019年10月10日
     */
    QQTargetAccount selectByAccount(@Param("qqAccount") String qqAccount);
}