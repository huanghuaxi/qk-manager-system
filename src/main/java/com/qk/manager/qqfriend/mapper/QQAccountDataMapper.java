package com.qk.manager.qqfriend.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.qk.manager.qqfriend.entity.QQAccountData;

/**
 * 
 * <p>Description: 加载QQ好友库类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年9月19日   
 * @version 1.0
 */
@Mapper
public interface QQAccountDataMapper {

	int deleteByPrimaryKey(Integer id);

    int insert(QQAccountData record);

    QQAccountData selectByPrimaryKey(Integer id);

    List<QQAccountData> selectAll();

    int updateByPrimaryKey(QQAccountData record);
    
    /**
     * 
     * Description: 根据管理员查询可添加好友 
     * @param userIds
     * @return 
     * date 2019年9月19日
     */
    List<QQAccountData> selectMayAdd(@Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 根据管理员查询可添加好友 
     * @param userIds
     * @return 
     * date 2019年9月19日
     */
    Integer countMayAdd(@Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 根据设备查找已经分配的好友   
     * @param targetDevice
     * @param userId
     * @return 
     * date 2019年9月20日
     */
    List<QQAccountData> queryAccountData(@Param("targetDevice") String targetDevice,Integer userId);
    
    /**
     * 
     * Description: 根据状态获取数据  
     * @param targetDevice 目标数据
     * @param userId 用户
     * @return 
     * date 2019年9月20日
     */
    List<QQAccountData> queryAccountDataByState(@Param("targetDevice") String targetDevice,Integer userId);
    
    
    /**
     * Description: 查找好友库    
     * @param accountData
     * @param start
     * @param pageSize
     * @return 
     * date 2019年7月25日
     */
    List<QQAccountData> selectByPage(@Param("accountData") QQAccountData accountData, Integer start, Integer pageSize
    		, @Param("userIds") List<Integer> userIds);
    
    /**
     * Description: 统计好友库   
     * @param accountData
     * @return 
     * date 2019年7月25日
     */
    Integer countAccountData(@Param("accountData") QQAccountData accountData, @Param("userIds") List<Integer> userIds);
    
    /**
     * Description:批量插入好友库 
     * @param list 
     * date 2019年7月26日
     */
    void insertBatch(@Param("list") List<QQAccountData> list);
    
    /**
     * Description: 批量删除好友库
     * @param list id集合
     * date 2019年7月26日
     */
    void delByBatch(@Param("list") List<String> list);
    
    /**
     * Description: 统计qq 好友数量
     * @param state
     * @param startCTime
     * @param endCTime
     * @param start
     * @param account
     * @return 
     * date 2019年9月24日
     */
    List<Map<String,Object>> qqStatitic(@Param("accountData") QQAccountData accountData,@Param("userIds") List<Integer> userIds);
    
    /**
     * Description: 获取导出qq内容   
     * @param accountData 
     * @param userIds
     * @return 
     * date 2019年9月25日
     */
    List<QQAccountData> exportData(@Param("accountData") QQAccountData accountData,@Param("userIds") List<Integer> userIds);
    
    /**
     * 
     * Description: 根据账号修改好友状态
     * @param accountName
     * @param targetAccount
     * @param state 
     * date 2019年9月25日
     */
    void updateStateByAccount(String accountName, String targetAccount, Integer state);
    
    /**
     * 
     * Description: 根据目标设备查询好友
     * @param targetDevice
     * @return 
     * date 2019年9月25日
     */
    List<QQAccountData> selectByDevice(String targetDevice);
    
    /**
     * 
     * Description: 根据账号和目标设备查找好友
     * @param accountName
     * @param deviceAccount
     * @return 
     * date 2019年9月26日
     */
    QQAccountData selectByAccount(String accountName, String targetDevice);
}