/**
 * 
 */
/**    
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年9月23日   
 * @version 1.0   
 */
package com.qk.manager.qqfriend.controller;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.qk.manager.business.controller.PageUtil;
import com.qk.manager.common.CodeUtil;
import com.qk.manager.common.CurrentParamUtil;
import com.qk.manager.common.ExcelUtil;
import com.qk.manager.common.ResultMessage;
import com.qk.manager.enums.AccountDataState;
import com.qk.manager.qqfriend.entity.QQAccountData;
import com.qk.manager.qqfriend.entity.QQTargetAccount;
import com.qk.manager.qqfriend.service.QQfriendService;
import com.qk.manager.security.entity.SysUser;
import com.qk.manager.security.service.UserService;

@Controller
@RequestMapping("/qqAccountData")
public class QQfriendController{
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private QQfriendService qqfriendService;
	
	@Autowired
	private UserService userService;
	
	/**
	 * 
	 * Description: 获取可添加QQ好友数量 
	 * @return 
	 * date 2019年9月19日
	 */
	@ResponseBody
	@RequestMapping("/getMayAddCount")
	public ResultMessage getMayAddCount() {
		try {
			SysUser user = CurrentParamUtil.getCurrentUser();
			Integer count = qqfriendService.countMayAdd(user.getId());
			logger.info("获取可添加QQ好友数量");
			return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, count);
		}catch (Exception e) {
			logger.error("获取可添加QQ好友数量失败", e);
		}
		return ResultMessage.getFail();
	}
	
	
	/**
	 * 
	 * Description: 跳转到账号管理页面    
	 * @return 
	 * date 2019年7月23日
	 */
	@RequestMapping("/toList")
	public String toList() {
		return "qqaccountdata/qqfriend.html"; 
	}
	
	/**
	 * Description:跳转新增页面    
	 * @return 
	 * date 2019年9月23日
	 */
	@RequestMapping("/toAdd")
	public String toAdd() {
		return "qqaccountdata/add.html";
	}
	
	
	
	
	@ResponseBody
	@RequestMapping("/getlst")
	public PageUtil<QQAccountData> getTaskLst(PageUtil<QQAccountData> pageUtil,QQAccountData accountData) {
		  List<Integer> userIds = userService.getRoleUser();
		  List<QQAccountData> lst = qqfriendService.selectByPage(accountData,
		  pageUtil.getCurrIndex(), pageUtil.getLimit(),userIds); 
		  int count = qqfriendService.countAccount(accountData,userIds); 
		  pageUtil.setCode("0");
		  pageUtil.setCount(count); 
		  pageUtil.setMsg("成功"); pageUtil.setData(lst);
		  logger.info("获取任务列表");
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 保存 
	 * @param info
	 * @return 
	 * date 2019年7月26日
	 */
	@ResponseBody
	@RequestMapping("/save")
	public ResultMessage save(String info) {
		try {
			List<QQAccountData> lst = new ArrayList<QQAccountData>();
			String[] infoArr = info.split("\n");
			SysUser user = CurrentParamUtil.getCurrentUser();
			for(String strs:infoArr) {
				QQAccountData accountData = new QQAccountData();
				String[]  str = strs.split("\\*");
				for(int i=0;i<str.length;i++) {
					if(i==0) {
					  accountData.setAccount(str[i]);
					}
					if(i==1) {
					  accountData.setRemark(str[i]);	
					}
					if(i==2) {
					  accountData.setApplicationTerms(str[i]);
					}
				}
				accountData.setCreateTime(new Date());
				accountData.setUpdateTime(new Date());
				accountData.setState(AccountDataState.NEW_CREATE.getCode());
				accountData.setUserId(user.getId());
				lst.add(accountData);
			}    
			qqfriendService.insertBatch(lst);
		}catch(Exception e) {
			logger.error("增加好友库失败", e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * Description:  删除  
	 * @param ids
	 * @return 
	 * date 2019年7月26日
	 */
	@ResponseBody
	@RequestMapping("/del")
	public ResultMessage del(String ids) {
		try {
			ids = ids.substring(0, ids.length()-1);
			List<String> idLst = Arrays.asList(ids.split(","));
			qqfriendService.delByBatch(idLst);
		}catch(Exception e) {
			logger.error("删除好友库失败", e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 重定向新增页面
	 * @return
	 */
	@GetMapping("/redirect")
	public String addRedirect(String id,String account,String remark,String applicationTerms,String state,Model model) {
		model.addAttribute("id", id);
		model.addAttribute("account", account);
		model.addAttribute("applicationTerms", applicationTerms);
		model.addAttribute("state", state);
		model.addAttribute("remark",remark);
		return "qqaccountdata/edit.html";
	}
	
	/**
	 * Description:  编辑 
	 * @param accountData
	 * @return 
	 * date 2019年7月26日
	 */
	@ResponseBody
	@RequestMapping("/edit")
	public ResultMessage edit(QQAccountData accountData) {
		try {
			accountData.setUpdateTime(new Date());
			qqfriendService.updateByPrimaryKey(accountData);
		}catch(Exception e) {
			logger.error("修改好友库失败", e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * 
	 * Description: 跳转
	 * @return 
	 * date 2019年9月23日
	 */
	@RequestMapping("/accountlst")
	public String getList() {
		return "qqaccountdata/qqfriend.html";
	}
	
	/**
	 * Description: 获取统计数据   
	 * @param state
	 * @param startCTime
	 * @param endCTime
	 * @param startUTime
	 * @param endUTime
	 * @param account
	 * @return 
	 * date 2019年9月24日
	 */
	@ResponseBody
	@RequestMapping("/qqStatistic")
	public ResultMessage qqStatistic(QQAccountData accountData) {
	  List<Map<String, 	Object>> list = qqfriendService.qqStatitic(accountData);
	  Long[] array = {0L,0L,0L,0L,0L,0L};
	  for(Map<String,Object> map: list) {
		  int key = Integer.valueOf(map.get("state").toString());
		  long count = Long.valueOf(map.get("count").toString());	
		  if(1 == key) {
			 array[0] = count;
		  }
		  
		  if(2 == key) {
			 array[1] = count;
		  }
		  
		  if(3 == key) {
			 array[2] = count;
		  }
		  
		  if(4 == key) {
			 array[3] = count;
		  }
		  
		  if(5 == key) {
			 array[4] = count;
		  }
		  
		  if(6 == key) {
			 array[5] = count;
		  }
	  }
	  return ResultMessage.getRtMsg(CodeUtil.SUCCESS_CODE, CodeUtil.SUCCESS_CODE_MSG, array);
	} 
	
	/**
	 * 
	 * Description: 导出中奖纪录    
	 * @param record
	 * @param response 
	 * date 2019年4月12日
	 */
	@RequestMapping("/expordExcel")
	public void expordExcel(QQAccountData accountData, HttpServletResponse response, HttpServletRequest request) {
		try {
			HSSFWorkbook workbook = qqfriendService.expordExcel(accountData);
			String fileName = "QQ好友统计数据" + new Date().getTime() + ".xls";
			if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {    
				fileName = URLEncoder.encode(fileName, "UTF-8");// IE浏览器    
			}else{    
				fileName = new String(fileName.getBytes("UTF-8"), "ISO8859-1");
			}
			//清空response  
            response.reset();  
            //设置response的Header  
            response.addHeader("Content-Disposition", "attachment;filename="+ fileName);  
            OutputStream os = new BufferedOutputStream(response.getOutputStream());  
            response.setContentType("application/vnd.ms-excel;charset=utf-8"); 
            //将excel写入到输出流中
            workbook.write(os);
            os.flush();
            os.close();
            logger.info("导出QQ统计数据");
		}catch(Exception e) {
			logger.error("导出QQ统计数据异常", e);
		}
	}
	
	
	
	@ResponseBody
	@RequestMapping("/getlstForQQfriend")
	public PageUtil<QQTargetAccount> getlstForQQfriend(PageUtil<QQTargetAccount> pageUtil,QQTargetAccount accountData) {
		  List<Integer> userIds = userService.getRoleUser();
		  List<QQTargetAccount> lst = qqfriendService.selectByPageForQQTarget(accountData,
		  pageUtil.getCurrIndex(), pageUtil.getLimit(),userIds); 
		  int count = qqfriendService.countAccountForQQTarget(accountData,userIds); 
		  pageUtil.setCode("0");
		  pageUtil.setCount(count); 
		  pageUtil.setMsg("成功"); pageUtil.setData(lst);
		  logger.info("获取任务列表");
		return pageUtil;
	}
	
	/**
	 * 
	 * Description: 跳转到账号管理页面    
	 * @return 
	 * date 2019年7月23日
	 */
	@RequestMapping("/toTargetQQAccountList")
	public String toTargetAccountList() {
		return "qqaccountdata/targetQQAccount.html"; 
	}
	
	/**
	 * Description: 奖品内定批量导入    
	 * @param file
	 * @param actId
	 * @return 
	 * date 2019年4月13日
	 */
	@ResponseBody
	@RequestMapping("/impExcel")
	public ResultMessage impExcel(@RequestParam("file") MultipartFile file) {
		try {
			//读取Excel数据内容
			logger.info("QQ目标账号导入 ");
			SysUser user = CurrentParamUtil.getCurrentUser();
			Workbook workbook = ExcelUtil.getWorkBook(file);
			String msg = qqfriendService.impExcel(workbook, user.getId());
			if("".equals(msg)) {
				return ResultMessage.getSuccess();
			}else {
				return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE, msg, null);
			}
		}catch(Exception e) {
			logger.error("QQ目标账号导入", e);
			return ResultMessage.getFail();
		}
	}
	
	@ResponseBody
	@RequestMapping("/saveTargetQQ")
	public ResultMessage saveTargetQQ(String info) {
		try {
			List<QQTargetAccount> lst = new ArrayList<QQTargetAccount>();
			String[] infoArr = info.split("\n");
			SysUser user = CurrentParamUtil.getCurrentUser();
			Map<String,String> map = new HashMap<String,String>();
			for(String strs:infoArr) {
				QQTargetAccount accountData = new QQTargetAccount();
				String[]  str = strs.split("\\*");
				for(int i=0;i<str.length;i++) {
					if(i==0) {
						 Integer count = qqfriendService.selectQQTargetAccount(str[i]);
						 if(count > 0) {
							return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE,"保存失败，【"+str[i]+"】账号已存在", null);
						 }
    					 if(map.size()!=0 && map.get(str[i])!=null && !"".equals(str[i])) {
						    return ResultMessage.getRtMsg(CodeUtil.FAIL_CODE,"保存失败，【"+str[i]+"】该账号重复填写", null); 
						 }
						 accountData.setQqAccount(str[i]);
						 map.put(str[i], "1");
					}
					if(i==1) {
					  accountData.setPwd(str[i]);	
					}
				}
				accountData.setCreateTime(new Date());
				accountData.setUpdateTime(new Date());
				accountData.setState(0);
				accountData.setUserId(user.getId());
				lst.add(accountData);
			}    
			qqfriendService.insertBatchQQTargetAccount(lst);
		}catch(Exception e) {
			logger.error("增加好友库失败", e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	} 
	
	/**
	 * Description:跳转新增页面    
	 * @return 
	 * date 2019年9月23日
	 */
	@RequestMapping("/targetQQAccountAdd")
	public String targetQQAccountAdd() {
		return "qqaccountdata/targetQQAccountAdd.html";
	}
	
	/**
	 * 添加到目标QQ重定向新增页面
	 * @return
	 */
	@GetMapping("/redirectQQTarget")
	public String addRedirect(String id,String qqAccount,String pwd,String state,Model model) {
		model.addAttribute("id", id);
		model.addAttribute("qqAccount", qqAccount);
		model.addAttribute("pwd", pwd);
		model.addAttribute("state", state);
		return "qqaccountdata/targetQQAccountEdit.html";
	}
	
	/**
	 * Description:  编辑 
	 * @param accountData
	 * @return 
	 * date 2019年7月26日
	 */
	@ResponseBody
	@RequestMapping("/editQQTarget")
	public ResultMessage editQQTarget(QQTargetAccount accountData) {
		try {
			accountData.setUpdateTime(new Date());
			qqfriendService.updateTargetQQById(accountData);
		}catch(Exception e) {
			logger.error("修改好友库失败", e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
	/**
	 * Description:  删除  
	 * @param ids
	 * @return 
	 * date 2019年7月26日
	 */
	@ResponseBody
	@RequestMapping("/delQQTarget")
	public ResultMessage delQQTarget(String ids) {
		try {
			ids = ids.substring(0, ids.length()-1);
			List<String> idLst = Arrays.asList(ids.split(","));
			qqfriendService.delByBatchQQTarget(idLst);
		}catch(Exception e) {
			logger.error("删除目标QQ好友失败", e);
			return ResultMessage.getFail();
		}
		return ResultMessage.getSuccess();
	}
	
}