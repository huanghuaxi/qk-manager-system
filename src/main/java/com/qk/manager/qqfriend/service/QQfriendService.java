/**
 * 
 */
/**    
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年9月23日   
 * @version 1.0   
 */
package com.qk.manager.qqfriend.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

import com.qk.manager.qqfriend.entity.QQAccountData;
import com.qk.manager.qqfriend.entity.QQTargetAccount;

public interface QQfriendService{
	

	/**
     * 
     * Description: 根据管理员查询可添加好友 
     * @param userIds
     * @return 
     * date 2019年9月19日
     */
    public List<QQAccountData> selectMayAdd(Integer userId);
    
    /**
     * 
     * Description: 根据管理员查询可添加好友 
     * @param userIds
     * @return 
     * date 2019年9月19日
     */
    public Integer countMayAdd(Integer userId);
	
	/**
	 * 
	 * Description: 分页查询    
	 * @param account
	 * @param start
	 * @param pageSize
	 * @return 
	 * date 2019年7月23日
	 */
	public List<QQAccountData> selectByPage(QQAccountData account, Integer start, Integer pageSize,List<Integer> userIds);
	
	/**
	 * 
	 * Description: 统计数量    
	 * @param account
	 * @return 
	 * date 2019年7月23日
	 */
	public Integer countAccount(QQAccountData account,List<Integer> userIds);
	
	/**
     * Description:批量插入好友库 
     * @param list 
     * date 2019年7月26日
     */
    void insertBatch(List<QQAccountData> list);
    
    /**
     * Description: 根据主键跟新数据
     * @param record
     * @return 
     * date 2019年7月24日
     */
    int updateByPrimaryKey(QQAccountData record);
    
    /**
     * Description: 批量删除好友库
     * @param list id集合
     * date 2019年7月26日
     */
    void delByBatch(@Param("list") List<String> list);
    
    /**
     * Description: 统计qq 好友数量
     * @param state
     * @param startCTime
     * @param endCTime
     * @param start
     * @param account
     * @return 
     * date 2019年9月24日
     */
    List<Map<String, Object>> qqStatitic(QQAccountData accountData);
    
    
    /**
	 * 
	 * Description: 导出中奖纪录   
	 * @param record
	 * @return 
	 * date 2019年4月12日
	 */
	public HSSFWorkbook expordExcel(QQAccountData accountData);
	
	/**
	 * 
	 * Description: 根据账号修改好友状态
	 * @param accountName
	 * @param targetAccount
	 * @param state 
	 * date 2019年9月25日
	 */
	public void updateStateByAccount(String accountName, String targetAccount, String targetDevice, Integer state);
	
	/**
	 * 
	 * Description: 根据目标设备查询好友 
	 * @param targetDevice
	 * @return 
	 * date 2019年9月25日
	 */
	public List<QQAccountData> selectByDevice(String targetDevice);
	
	/**
	 * Description: 查询QQ好友页数数量好友   
	 * @param account
	 * @param start
	 * @param pageSize
	 * @param userIds
	 * @return 
	 * date 2019年10月9日
	 */
	public List<QQTargetAccount> selectByPageForQQTarget(QQTargetAccount account, Integer start, Integer pageSize,List<Integer> userIds);
	
	/**
	 * 
	 * Description: 统计数量    
	 * @param account
	 * @return 
	 * date 2019年7月23日
	 */
	public Integer countAccountForQQTarget(QQTargetAccount account,List<Integer> userIds);
	
	/**
	 * Description: 导入excell表格内容   
	 * @param actId
	 * @param workbook
	 * @param flag
	 * @param uid
	 * @return 
	 * date 2019年10月10日
	 */
	public String impExcel(Workbook workbook, Integer uid);
	
	/**
	 * Description:    
	 * @param lst 
	 * date 2019年10月10日
	 */
	public String insertBatchQQTargetAccount( List<QQTargetAccount> lst);
    
	/**
	 * Description: 查找相同目标qq数量    
	 * @param qqAccount qq号名称
	 * @return 
	 * date 2019年10月10日
	 */
	public Integer selectQQTargetAccount(String qqAccount);
	
	/**
     * Description: 更新QQ目标号   
     * @param accountData 
     * date 2019年10月10日
     */
    void updateTargetQQById(QQTargetAccount accountData);
    
    /**
     * Description: 批量删除目标QQ    
     * @param list 
     * date 2019年10月10日
     */
    void delByBatchQQTarget(List<String> list);
    
    /**
     * 
     * Description: 修改QQ账号状态
     * @param account
     * @param state 
     * date 2019年10月10日
     */
    void updateQQTargetState(String account, Integer state);
    
    /**
     * 
     * Description: 修改QQ账号备注
     * @param account
     * @param remark 
     * date 2019年10月10日
     */
    void updateQQTargetRemark(String account, String remark);
}