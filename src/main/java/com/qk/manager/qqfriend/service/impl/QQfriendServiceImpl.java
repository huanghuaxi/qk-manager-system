/**
 * 
 */
/**    
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年9月23日   
 * @version 1.0   
 */
package com.qk.manager.qqfriend.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qk.manager.common.ExcelUtil;
import com.qk.manager.qqfriend.entity.QQAccountData;
import com.qk.manager.qqfriend.entity.QQTargetAccount;
import com.qk.manager.qqfriend.mapper.QQAccountDataMapper;
import com.qk.manager.qqfriend.mapper.QQTargetAccountMapper;
import com.qk.manager.qqfriend.service.QQfriendService;
import com.qk.manager.security.service.UserService;

/**
 * 
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author admin   
 * @date 2019年10月9日   
 * @version 1.0
 */

@Service
public class QQfriendServiceImpl implements QQfriendService{
	
	@Autowired
	private QQAccountDataMapper qqAccountDataMapper;
	
	@Autowired
	private QQTargetAccountMapper qqTargetAccountMapper;
	
	@Autowired
	private UserService userService;
	
	private final static String CONS_CODE = "value";
	
	/*
	 *(non-Javadoc)   
	 * <p>Title: selectMayAdd</p>   
	 * <p>Description: 根据管理员查询可以添加好友</p>   
	 * @param userIds
	 * @return   
	 * @see com.qk.manager.accountdata.service.QQAccountDataService#selectMayAdd(java.util.List)
	 */
	@Override
	public List<QQAccountData> selectMayAdd(Integer userId) {
		List<Integer> userIds = userService.getRoleUser();
		return qqAccountDataMapper.selectMayAdd(userIds);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: countMayAdd</p>   
	 * <p>Description: 根据管理员统计可以添加好友数量</p>   
	 * @param userIds
	 * @return   
	 * @see com.qk.manager.accountdata.service.QQAccountDataService#countMayAdd(java.util.List)
	 */
	@Override
	public Integer countMayAdd(Integer userId) {
		List<Integer> userIds = userService.getRoleUser();
		return qqAccountDataMapper.countMayAdd(userIds);
	}

	@Override
	public List<QQAccountData> selectByPage(QQAccountData account, Integer start, Integer pageSize,List<Integer> userIds) {
		return qqAccountDataMapper.selectByPage(account, start, pageSize, userIds);
	}

	@Override
	public Integer countAccount(QQAccountData accountData,List<Integer> userIds) {
		return qqAccountDataMapper.countAccountData(accountData, userIds);
	}

	@Override
	public void insertBatch(List<QQAccountData> list) {
		qqAccountDataMapper.insertBatch(list);
	}

	@Override
	public int updateByPrimaryKey(QQAccountData record) {
		QQAccountData rd = qqAccountDataMapper.selectByPrimaryKey(record.getId());
		rd.setAccount(record.getAccount());
		rd.setRemark(record.getRemark());
		rd.setApplicationTerms(record.getApplicationTerms());
		rd.setState(record.getState());
		rd.setTargetAccount("");
		rd.setUpdateTime(new Date());
		return qqAccountDataMapper.updateByPrimaryKey(rd);
	}

	@Override
	public void delByBatch(List<String> list) {
		qqAccountDataMapper.delByBatch(list);		
	}

	@Override
	public List<Map<String, Object>> qqStatitic(QQAccountData accountData) {
		List<Integer> userIds = userService.getRoleUser();
		return qqAccountDataMapper.qqStatitic(accountData,userIds);
	}

	@Override
	public HSSFWorkbook expordExcel(QQAccountData accountData) {
		//查询中奖纪录
		List<Integer> userIds = userService.getRoleUser();
		List<QQAccountData> qqList = qqAccountDataMapper.exportData(accountData, userIds);
		// 声明一个工作薄        
        HSSFWorkbook workbook = new HSSFWorkbook();
        //创建一个Excel表单,参数为sheet的名字
        HSSFSheet sheet = workbook.createSheet("QQ数据");
        //创建表头
        setTitle(workbook, sheet);
        //新增数据行，并且设置单元格数据
        HSSFRow hssfRow = sheet.createRow(1);
        int rows = 1;
        for(QQAccountData qcd: qqList) {
        	hssfRow = sheet.createRow(rows);
        	hssfRow.createCell(0).setCellValue(qcd.getAccount());
    		hssfRow.createCell(1).setCellValue(qcd.getRemark());
    		hssfRow.createCell(2).setCellValue(qcd.getTargetAccount());
    		hssfRow.createCell(2).setCellValue(qcd.getTargetDevice());
        	rows++;
        }
		return workbook;
	}
	
	// 创建表头
    private static void setTitle(HSSFWorkbook workbook, HSSFSheet sheet) {
        HSSFRow row = sheet.createRow(0);
        // 设置列宽，setColumnWidth的第二个参数要乘以256，这个参数的单位是1/256个字符宽度
        sheet.setColumnWidth(8, 60 * 256);
        // 设置为居中加粗
        HSSFCellStyle style = workbook.createCellStyle();
        HSSFFont font = workbook.createFont();
        font.setBold(true);
        style.setFont(font);
        //导出的Excel头部
        String[] headers = { "好友qq号", "备注名称", "添加到目标QQ号", "目标设备" };
        // 设置表格默认列宽度为20个字节
        sheet.setDefaultColumnWidth((short) 20);
        for (short i = 0; i < headers.length; i++) {
            HSSFCell cell = row.createCell(i);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
            cell.setCellStyle(style);
        }
    }

    /*
     * 
     *(non-Javadoc)   
     * <p>Title: updateStateByAccount</p>   
     * <p>Description: 根据账号修改好友状态</p>   
     * @param accountName
     * @param targetAccount
     * @param state   
     * @see com.qk.manager.qqfriend.service.QQfriendService#updateStateByAccount(java.lang.String, java.lang.String, java.lang.Integer)
     */
	@Override
	public void updateStateByAccount(String accountName, String targetAccount, String targetDevice, Integer state) {
		QQAccountData account = qqAccountDataMapper.selectByAccount(accountName, targetDevice);
		if(state == 3) {
			//已申请添加，修改状态和目标账号
			if(account != null && account.getState() == 2) {
				account.setTargetAccount(targetAccount);
				account.setState(state);
				account.setUpdateTime(new Date());
				qqAccountDataMapper.updateByPrimaryKey(account);
			}
		}else {
			if(account != null) {
				if(account.getTargetAccount() == null || "".equals(account.getTargetAccount())) {
					account.setTargetAccount(targetAccount);
					account.setState(state);
					account.setUpdateTime(new Date());
					qqAccountDataMapper.updateByPrimaryKey(account);
				}else {
					qqAccountDataMapper.updateStateByAccount(accountName, targetAccount, state);
				}
			}
		}
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: selectByDevice</p>   
	 * <p>Description: 根据目标设备查询好友</p>   
	 * @param targetDevice
	 * @return   
	 * @see com.qk.manager.qqfriend.service.QQfriendService#selectByDevice(java.lang.String)
	 */
	@Override
	public List<QQAccountData> selectByDevice(String targetDevice) {
		return qqAccountDataMapper.selectByDevice(targetDevice);
	}
	

	@Override
	public List<QQTargetAccount> selectByPageForQQTarget(QQTargetAccount account, Integer start, Integer pageSize,
			List<Integer> userIds) {
		return qqTargetAccountMapper.selectByPage(account, start, pageSize, userIds);
	}

	@Override
	public Integer countAccountForQQTarget(QQTargetAccount account, List<Integer> userIds) {
		return qqTargetAccountMapper.countAccountData(account, userIds);
	}
	
	@Override
	public String impExcel( Workbook workbook,Integer uid) {
		if(workbook != null) {
			for(int sheetNum = 0;sheetNum < workbook.getNumberOfSheets();sheetNum++){
				//获得当前sheet工作表
                Sheet sheet = workbook.getSheetAt(sheetNum);
                if(sheet == null){
                    continue;
                }
                //获得当前sheet的开始行
                int firstRowNum  = sheet.getFirstRowNum();
                //获得当前sheet的结束行
                int lastRowNum = sheet.getLastRowNum();
                //循环除了第一行的所有行
                List<QQTargetAccount> lst = new ArrayList<QQTargetAccount>();
              //为了过滤到第一行因为我的第一行是数据库的列
                Map<String,String> map = new HashMap<String,String>();
                for(int rowNum = firstRowNum+1;rowNum <= lastRowNum;rowNum++){ 
                    //获得当前行
                    Row row = sheet.getRow(rowNum);
                    if(row == null){
                    	return "导入失败，第"+rowNum+"行数据为空";
                    }
                    QQTargetAccount qqtargetAccount = new QQTargetAccount();
                    //获取每列数据
                    Cell cell = row.getCell(0);
                    String qqAccount = ExcelUtil.getCellValue(cell).trim();
                    cell = row.getCell(1);
                    String pwd = ExcelUtil.getCellValue(cell).trim();
                    if(qqAccount == null || "".equals(qqAccount)) {
        				return "导入失败，第"+rowNum+"行，qq账号为空";
        			}
                    Integer count = qqTargetAccountMapper.isExistByQQaccount(qqAccount);
                    if(count > 0) {
                    	return "导入失败，第"+rowNum+"行，【"+qqAccount+"】该账号已存在";
                    }
                    if(pwd == null || "".equals(pwd) ) {
        				return "导入失败，第"+rowNum+"行，密码为空";
        			}
                    if(map.size()!=0) {
                    	String val = map.get(qqAccount);
                    	if(val != null && !"".equals(val)) {
                    		return "导入失败，【"+qqAccount+"】该QQ号在模板中重复";
                    	}
                    }
                    qqtargetAccount.setQqAccount(qqAccount);
                    qqtargetAccount.setPwd(pwd);
                    qqtargetAccount.setCreateTime(new Date());
                    qqtargetAccount.setUpdateTime(new Date());
                    qqtargetAccount.setUserId(uid);
                    qqtargetAccount.setState(1);
                    map.put(qqAccount, CONS_CODE);
                    lst.add(qqtargetAccount);	
                }
                //新增QQ号
                qqTargetAccountMapper.insertBatch(lst);
             }
		}
		return "";
	}
	
	/**
	 * Description: 批量插入目标QQ  
	 * @param lst 
	 * date 2019年10月10日
	 */
	public String insertBatchQQTargetAccount( List<QQTargetAccount> lst) {
		//新增QQ号
        qqTargetAccountMapper.insertBatch(lst);
        return "";
	}

	@Override
	public Integer selectQQTargetAccount(String qqAccount) {
		return qqTargetAccountMapper.isExistByQQaccount(qqAccount);
	}

	@Override
	public void updateTargetQQById(QQTargetAccount accountData) {
		qqTargetAccountMapper.updateTargetQQById(accountData);
	}

	@Override
	public void delByBatchQQTarget(List<String> list) {
		qqTargetAccountMapper.delByBatchQQTarget(list);
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: updateQQTargetState</p>   
	 * <p>Description: 修改QQ账号状态</p>   
	 * @param account
	 * @param state   
	 * @see com.qk.manager.qqfriend.service.QQfriendService#updateQQTargetState(java.lang.String, java.lang.Integer)
	 */
	@Override
	public void updateQQTargetState(String account, Integer state) {
		QQTargetAccount qqAccount = qqTargetAccountMapper.selectByAccount(account);
		if(qqAccount != null) {
			qqAccount.setState(state);
			qqAccount.setUpdateTime(new Date());
			qqTargetAccountMapper.updateByPrimaryKey(qqAccount);
		}
	}

	/*
	 * 
	 *(non-Javadoc)   
	 * <p>Title: updateQQTargetRemark</p>   
	 * <p>Description: 修改QQ账号备注</p>   
	 * @param account
	 * @param remark   
	 * @see com.qk.manager.qqfriend.service.QQfriendService#updateQQTargetRemark(java.lang.String, java.lang.String)
	 */
	@Override
	public void updateQQTargetRemark(String account, String remark) {
		QQTargetAccount qqAccount = qqTargetAccountMapper.selectByAccount(account);
		if(qqAccount != null) {
			qqAccount.setRemark(remark);
			qqAccount.setUpdateTime(new Date());
			qqTargetAccountMapper.updateByPrimaryKey(qqAccount);
		}
	}
	
	
}