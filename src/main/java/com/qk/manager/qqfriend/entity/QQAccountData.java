package com.qk.manager.qqfriend.entity;

import java.util.Date;

/**
 * 
 * <p>Description: QQ好友库类</p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author zhangxin   
 * @date 2019年9月19日   
 * @version 1.0
 */
public class QQAccountData {
    /**
     *	ID
     */
    private Integer id;

    /**
     *	账号
     */
    private String account;

    /**
     *	备注
     */
    private String remark;

    /**
     *	申请语
     */
    private String applicationTerms;

    /**
     *	状态
     */
    private Integer state;

    /**
     *	结果
     */
    private Integer result;

    /**
     *	目标设备
     */
    private String targetDevice;

    /**
     *	目标账号
     */
    private String targetAccount;

    /**
     *	创建时间
     */
    private Date createTime;

    /**
     *	更新时间
     */
    private Date updateTime;

    /**
     * 创建开始时间
     */
	private String startCTime;
    
	/**
     * 创建结束时间
     */
    private String endCTime;
    
    /**
     * 更新开始时间
     */
    private String startUTime;
    
    /**
     * 更新结束时间
     */
    private String endUTime;

    /**
     *	管理员ID
     */
    private Integer userId;
    
    public String getStartCTime() {
 		return startCTime;
 	}

 	public void setStartCTime(String startCTime) {
 		this.startCTime = startCTime;
 	}

 	public String getEndCTime() {
 		return endCTime;
 	}

 	public void setEndCTime(String endCTime) {
 		this.endCTime = endCTime;
 	}

 	public String getStartUTime() {
 		return startUTime;
 	}

 	public void setStartUTime(String startUTime) {
 		this.startUTime = startUTime;
 	}

 	public String getEndUTime() {
 		return endUTime;
 	}

 	public void setEndUTime(String endUTime) {
 		this.endUTime = endUTime;
 	}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getApplicationTerms() {
        return applicationTerms;
    }

    public void setApplicationTerms(String applicationTerms) {
        this.applicationTerms = applicationTerms == null ? null : applicationTerms.trim();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getTargetDevice() {
        return targetDevice;
    }

    public void setTargetDevice(String targetDevice) {
        this.targetDevice = targetDevice == null ? null : targetDevice.trim();
    }

    public String getTargetAccount() {
        return targetAccount;
    }

    public void setTargetAccount(String targetAccount) {
        this.targetAccount = targetAccount == null ? null : targetAccount.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    
}