package com.qk.manager.enums;

/**
 * <p>Description: </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年8月29日   
 * @version 1.0
 */
public enum ServiceAccountAction {
	
	SMG("互聊","SMG","sendMsgForEachServiceImpl"),
	LONGVOICE("发送语音","LONGVOICE","sendVoiceForEachServiceImpl"),
	LONGVIDEO("发送视频","LONGVIDEO","sendVideoForEachServiceImpl"),
	RFRICYCLE("阅读朋友圈","RFRICYCLE","friendCycleServiceImpl"),
	SENDMOMENTS("发朋友圈","SENDMOMENTS","friendCyclePulishServiceImpl"),
	READMESSAGE("阅读消息","READMESSAGE","readMessageServiceImpl");
	
   private ServiceAccountAction(String desc,String code,String action) {
	   this.desc = desc;
	   this.code = code;
	   this.action = action;
   }
   
   /**
    * 描述
    */
    private String desc;
    
    /**
     * 编码 
     */
	private String code;
	
	/**
	 * 执行业务类
	 */
	private String action;
	
	public String getDesc() {
		return desc;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getAction() {
		return action;
	}
	
	public void setAction(String action) {
		this.action = action;
	}
   
   
	
	/**
	 * Description: 根据code 获取名称    
	 * @param code
	 * @return 
	 * date 2019年8月2日
	 */
	public static String getActionByCode(String code) {
		String action = "";
        switch (code) {
            case "SMG":
            	action = SMG.action;
                break;
            case "LONGVOICE":
            	action = LONGVOICE.action;
            	 break;
            case "LONGVIDEO":
            	action = LONGVIDEO.action;
            	 break;
            case "RFRICYCLE":
            	action = RFRICYCLE.action;
            	 break;
            case "SENDMOMENTS":
            	action = SENDMOMENTS.action;
            	 break;
            case "READMESSAGE":
            	action = READMESSAGE.action;
            	 break;
        }
        return action;
	}
}