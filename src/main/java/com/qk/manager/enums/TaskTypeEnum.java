package com.qk.manager.enums;

/**
 * 
 * <p>Description: 任务类型枚举 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年8月19日   
 * @version 1.0
 */
public enum TaskTypeEnum {

    ADD_FRIEND("微信添加好友","ADDFRIEND","com.qk.manager.quarz.job.AddFriendJob","addFriendDataIntfImpl"),
    PULL_FRIEND_TO_GROUP("拉好友进群","PULLFRIEDTOGROUP","com.qk.manager.quarz.job.PullFriendToGroupJob","pullFriendToGroupDataIntfImpl"),
    READ_MESSAGE("定时阅读消息","READMESSAGE","",""),
    SEND_MESSAGE("发送消息","SENDMESSAGE","com.qk.manager.quarz.job.SendMessageJob","sendMessageDataIntfImpl"),
    SEND_VOICE_CALL("发送语音通话","SENDVOICECALL","com.qk.manager.quarz.job.SendVoiceCallJob","sendVoiceCallDataIntfImpl"),
    SEND_VIDEO_CALL("发送视频通话","SENDVIDEOCALL","com.qk.manager.quarz.job.SendVideoCallJob","sendVideoCallDataIntfImpl"),
	ACCEPT_VOICE_CALL("接收语音通话","ACCEPTVOICECALL","com.qk.manager.quarz.job.AcceptVoiceCallJob","acceptVoiceCallDataIntfImpl"),
	ACCEPT_VIDEO_CALL("接收视频通话","ACCEPTVIDEOCALL","com.qk.manager.quarz.job.AcceptVideoCallJob","acceptVideoCallDataIntfImpl"),
	FRIEND_CYCLE("朋友圈阅读点赞","FRIENDCYCLE","",""),
	MOMENTS_SEND_TEXT("朋友圈发送文字", "MOMENTSSENDTEXT", "", ""),
    MOMENTS_SEND_IMG("朋友圈发送图片", "MOMENTSSENDIMG", "", ""),
    MOMENTS_SEND_VIDEO("朋友圈发送视频", "MOMENTSSENDVIDEO", "", ""),
	SERVICE_ACCOUNT("微信养号","SERVICEACCOUNT","com.qk.manager.quarz.job.ServiceAccountJob",""),
	QQ_ADD_FRIEND("QQ添加好友", "QQADDFRIEND", "com.qk.manager.quarz.job.QQAddFriendJob", "QQAddFriendIntfImpl"),
	QQ_PULL_FRIEND_TO_GROUP("QQ拉好友进群", "QQPULLFRIENDTOGROUP", "com.qk.manager.quarz.job.PullQQFriendToGroupJob", "PullQQFriendToGroupServiceImpl"),
	QQ_FRIEND_CHECK("QQ好友检查", "QQFRIENDCHECK", "com.qk.manager.quarz.job.CheckAccoutIsAddJob", "QQFriendCheckIntfImpl"),
	WX_FRIEND_CHECK("微信好友检查", "WXFRIENDCHECK", "com.qk.manager.quarz.job.WXCheckAccoutIsAddJob", "WXFriendCheckIntfImpl");

    /**
     *	任务名称
     */
    private String taskName;

    /**
     * 	编码
     */
    private String code;

    /**
     *
     */
    private String clsName;
    
    private String beanName;

    private TaskTypeEnum(String taskName,String code,String clsName,String beanName){
        this.code = code;
        this.taskName = taskName;
        this.clsName = clsName;
        this.beanName = beanName;
    }

    /**
     * 根据code 获取任务名称
     * @param code
     * @return
     */
    public static String getTaskNameByCode(String code){
       String taskName = "";
        switch (code) {
            case "ADDFRIEND":
                taskName = ADD_FRIEND.taskName;
                break;
            case "PULLFRIEDTOGROUP":
                taskName = PULL_FRIEND_TO_GROUP.taskName;
                break;
            case "READMESSAGE":
                taskName = READ_MESSAGE.taskName;
                break;
            case "SENDMESSAGE":
            	taskName = SEND_MESSAGE.taskName;
            	break;
            case "SENDVOICECALL":
            	taskName = SEND_VOICE_CALL.taskName;
            	break;
            case "SENDVIDEOCALL":
            	taskName = SEND_VIDEO_CALL.taskName;
            	break;
            case "ACCEPTVOICECALL":
            	taskName = ACCEPT_VOICE_CALL.taskName;
            	break;
            case "ACCEPTVIDEOCALL":
            	taskName = ACCEPT_VIDEO_CALL.taskName;
            	break;
            case "SERVICEACCOUNT" :
            	taskName = SERVICE_ACCOUNT.taskName;
            	break;
            case "QQADDFRIEND" :
            	taskName = QQ_ADD_FRIEND.taskName;
            	break;
            case "QQFRIENDCHECK" :
            	taskName = QQ_FRIEND_CHECK.taskName;
            	break;
            case "QQPULLFRIENDTOGROUP" :
            	taskName = QQ_PULL_FRIEND_TO_GROUP.taskName;
            	break;
            case "WXFRIENDCHECK" :
            	taskName = WX_FRIEND_CHECK.taskName;
            	break;
                
        }
        return taskName;
    }

    /**
     * 根据code 获取任务类
     * @param code
     * @return
     */
    public  static String  getClsName(String code){
        String clsName = "";
        switch (code) {
            case "ADDFRIEND":
                clsName = ADD_FRIEND.clsName;
                break;
            case "PULLFRIEDTOGROUP":
                clsName = PULL_FRIEND_TO_GROUP.clsName;
                break;
            case "READMESSAGE":
                clsName = READ_MESSAGE.clsName;
                break;
            case "SENDMESSAGE":
            	clsName = SEND_MESSAGE.clsName;
            	break;
            case "SENDVOICECALL":
            	clsName = SEND_VOICE_CALL.clsName;
            	break;
            case "SENDVIDEOCALL":
            	clsName = SEND_VIDEO_CALL.clsName;
            	break;
            case "ACCEPTVOICECALL":
            	clsName = ACCEPT_VOICE_CALL.clsName;
            	break;
            case "ACCEPTVIDEOCALL":
            	clsName = ACCEPT_VIDEO_CALL.clsName;
            	break;
            case "SERVICEACCOUNT":
            	clsName = SERVICE_ACCOUNT.clsName;
            	break;
            case "QQADDFRIEND" :
            	clsName = QQ_ADD_FRIEND.clsName;
            	break;
            case "QQFRIENDCHECK" :
            	clsName = QQ_FRIEND_CHECK.clsName;
            	break;
            case "QQPULLFRIENDTOGROUP" :
            	clsName = QQ_PULL_FRIEND_TO_GROUP.clsName;
            	break;
            case "WXFRIENDCHECK" :
            	clsName = WX_FRIEND_CHECK.clsName;
            	break;
        }
        return clsName;
    }
    
    /**
     * 根据code 获取bean
     * @param code
     * @return
     */
    public  static String  getbeanNameByCode(String code){
        String beanName = "";
        switch (code) {
            case "ADDFRIEND":
                beanName = ADD_FRIEND.beanName;
                break;
            case "PULLFRIEDTOGROUP":
            	beanName = PULL_FRIEND_TO_GROUP.beanName;
            	break;
            case "READMESSAGE":
            	beanName = READ_MESSAGE.beanName;
            	break;
            case "SENDMESSAGE":
            	beanName = SEND_MESSAGE.beanName;
            	break;
            case "SENDVOICECALL":
            	beanName = SEND_VOICE_CALL.beanName;
            	break;
            case "SENDVIDEOCALL":
            	beanName = SEND_VIDEO_CALL.beanName;
            	break;
            case "ACCEPTVOICECALL":
            	beanName = ACCEPT_VOICE_CALL.beanName;
            	break;
            case "ACCEPTVIDEOCALL":
            	beanName = ACCEPT_VIDEO_CALL.beanName;
            	break;
            case "SERVICEACCOUNT":
            	beanName = SERVICE_ACCOUNT.beanName;
            	break;
            case "QQADDFRIEND" :
            	beanName = QQ_ADD_FRIEND.beanName;
            	break;
            case "QQFRIENDCHECK" :
            	beanName = QQ_FRIEND_CHECK.beanName;
            	break;
            case "QQPULLFRIENDTOGROUP" :
            	beanName = QQ_PULL_FRIEND_TO_GROUP.beanName;
            	break;
            case "WXFRIENDCHECK" :
            	beanName = WX_FRIEND_CHECK.beanName;
            	break;
        }
        return beanName;
    }
    
    public String getCode() {
    	return this.code;
    }
}
