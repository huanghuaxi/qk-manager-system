package com.qk.manager.enums;

public enum AccountDataState {
	
	NEW_CREATE("新增",1),
	DISTRIBUTED_NOT_INCREATE("已分配未增加",2),
	APPLYED("已申请添加",3);
	
	
   private AccountDataState(String stateName,int code) {
	   this.stateName = stateName;
	   this.code = code;
   }
   
    private String stateName;
	private int code;
   
    public String getStateName() {
	    return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
}
