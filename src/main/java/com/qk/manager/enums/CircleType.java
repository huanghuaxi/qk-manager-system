package com.qk.manager.enums;

/**
 * <p>Description: 循环 </p>   
 * <p>Copyright: Copyright (c) 2019</p>    
 * @author huanghx   
 * @date 2019年8月2日   
 * @version 1.0
 */
public enum CircleType {
	
	PER_DAY("每天",1),
	PER_WEEK("每周",2),
	PER_MONTH("每月",3);
	
	
   private CircleType(String name,int code) {
	   this.name = name;
	   this.code = code;
   }
   
    private String name;
	private int code;
   
    public String getName() {
	    return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	
	/**
	 * Description: 根据code 获取名称    
	 * @param code
	 * @return 
	 * date 2019年8月2日
	 */
	public static String getNameByCode(int code) {
		String name = "";
        switch (code) {
            case 1:
                name = PER_DAY.name;
                break;
            case 2:
                name = PER_WEEK.name;
            case 3:
                name = PER_MONTH.name;

        }
        return name;
	}

}
