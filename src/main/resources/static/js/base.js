
//判断文件类型
function getFileType(fileName){
	if(fileName == "" && fileName.lastIndexOf(".") < 0){
		return "";
	}
	var suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
	suffix = suffix.toLowerCase();
	if(suffix == "jpg" || suffix == "png" || suffix == "gif"){
		return "img";
	}
	if(suffix == "mp4"){
		return "video";
	}
	return "";
}