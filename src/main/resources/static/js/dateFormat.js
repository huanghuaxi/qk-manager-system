function createTime(v){
	var date = new Date(v);
    var y = date.getFullYear();
    var m = date.getMonth()+1;
    m = m<10?'0'+m:m;
    var d = date.getDate();
    d = d<10?("0"+d):d;
    var h = date.getHours();
    h = h<10?("0"+h):h;
    var M = date.getMinutes();
    M = M<10?("0"+M):M;
    var sec = date.getSeconds();
    sec = sec<10?("0" + sec) : sec;
    var str = y+"-"+m+"-"+d+" "+h+":"+M+":"+sec;
    return str;
}