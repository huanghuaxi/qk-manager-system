layui.define([ 'element', 'common', 'jquery', 'admin'], function(exports) {
	"use strict";

	var $ = layui.jquery, layer = parent.layer === undefined ? layui.layer
			: parent.layer, element = layui.element, common = layui.common, admin = layui.admin;

	var tabsPage = admin.tabsPage;
	var MenuBar = function() {
		/**
		 * 默认配置
		 */
		this.config = {
			elem : undefined, // 容器
			url : undefined
		// 数据源地址
		};
		this.v = '1.0.0';
	};

	/**
	 * 组装菜单成html
	 */
	function getHtml(data) {
		var uHtml = appendTitle();
		uHtml += appendUiStart();
		for (var i = 0; i < data.length; i++) {
			if (data[i].parentId == 0)
				uHtml = uHtml + appendLiStart()
						+ appendA(data[i].url, data[i].label, data[i].leaf,data[i].id,data[i].icon,"");
			if (data[i].leaf != true) {
				uHtml = uHtml + handler(data[i].children,"&nbsp;");
			}
			uHtml = uHtml + appendLiEnd();
		}
		uHtml = uHtml + appendUiEnd();
		return uHtml;
	}

	/**
	 * 拼接A标签
	 */
	function appendA(url, label, leaf,id,icon,spance) {
		var aSign = "";
		if (leaf) {
			aSign = "<a  class='site-demo-active' data-id='"+id+"' data-title='"+label+"' data-url='"+url+"' " 
					+"onmouseover='this.style.cursor=\"pointer\";' " 
					+"onmouseout='this.style.cursor=\"default\";'>" 
			        +"<i class='layui-icon' >"+icon+"</i>"
			        +"<cite>"+spance+label+"</cite>"
			        +"</a>";
		} else {
			aSign = "<a>" 
	        +"<i class='layui-icon' >"+icon+"</i>"
	        +"<cite>"+spance+label+"</cite>"
	        +"</a>";
		}
		return aSign;
	}
	

	/**
	 * 子级菜单递归处理
	 */
	function handler(menu,spance) {
		var dlHtml = "";
		if (menu != null && menu.length != 0) {
			var dlHtml = appendDlStart();
			for (var i = 0; i < menu.length; i++) {
				if (menu.leaf != true) {
					dlHtml = dlHtml + appendDdStart()
							+ appendA(menu[i].url, menu[i].label, menu[i].leaf,menu[i].id,menu[i].icon,spance)
							+ handler(menu[i].children,spance+"&nbsp;&nbsp;&nbsp;") + appendDdEnd();
				} else {
					dlHtml = dlHtml + appendDdStart()
							+ appendA(menu.url, menu.label, menu.leaf,menu.id,spance)
							+ appendDdEnd();
				}
			}
			dlHtml = dlHtml + appendDlEnd();
		}
		return dlHtml;
	}

	/**
	 * li开始标签
	 */
	function appendLiStart() {
		return "<li class='layui-nav-item'>";
	}

	/**
	 * li结束标签
	 */
	function appendLiEnd() {
		return "</li>";
	}

	/**
	 * ui开始标签
	 */
	function appendUiStart() {
		return "<ul class='layui-nav layui-nav-tree' id='LAY-system-side-menu' lay-filter='layadmin-system-side-menu'>";
	}

	/**
	 * ui结束标签
	 */
	function appendUiEnd() {
		return "</ul>";
	}

	/**
	 * dl开始标签
	 */
	function appendDlStart(menu) {
		return "<dl class='layui-nav-child' >";
	}

	/**
	 * dl结束标签
	 */
	function appendDlEnd() {
		return "</dl>";
	}

	/**
	 * dd开始标签
	 */
	function appendDdStart() {
		return "<dd>";
	}

	/**
	 * dd结束标签
	 */
	function appendDdEnd() {
		return "</dd>";
	}
	
	function appendTitle() {
		return '<div class="layui-logo" lay-href="home/console.html"><span>拼图科技</span></div>';
	}

	/**
	 * 加载将拼接的菜单html嵌入容器中
	 */
	MenuBar.prototype.render = function() {
		var _that = this;
		var _config = _that.config;
		var container = $(_config.elem);
		$.ajax({
			url : _config.url,
			async : true,
			dataType : 'json',
			success : function(result, status, xhr) {
				var html = getHtml(result);
				container.html(html);
				element.init();
				onBandTad();
				
			},
			error : function(xhr, status, error) {
				common.msgError('菜单加载失败:' + error);
			},
			complete : function(xhr, status) {
			}
		});
	};
	
	/**
	 * 菜单点击绑定到tab
	 */
    function onBandTad(){
	  $('.site-demo-active').on('click', function () {
	        var dataid = $(this);
	        //这时会判断右侧.layui-tab-title属性下的有lay-id属性的li的数目，即已经打开的tab项数目
	        if ($(".layui-tab-title li[lay-id]").length <= 0) {
	            //如果比零小，则直接打开新的tab项
	            active.tabAdd(dataid.attr("data-url"), dataid.attr("data-id"), dataid.attr("data-title"));
	        } else {
	            //否则判断该tab项是否以及存在
	            var isData = false; //初始化一个标志，为false说明未打开该tab项 为true则说明已有
//	            $('.layadmin-tabsbody-item').removeClass("layui-show");
	            $.each($(".layui-tab-title li[lay-id]"), function () {
	                //如果点击左侧菜单栏所传入的id 在右侧tab项中的lay-id属性可以找到，则说明该tab项已经打开
	                if ($(this).attr("lay-id") == dataid.attr("data-id")) {
	                    isData = true;
	                }
	            });
	            /*$.each($(".layadmin-tabsbody-item iframe[data-frameid]"), function () {
	            	//如果点击左侧菜单栏所传入的id 在右侧tab项中的lay-id属性可以找到，则说明该tab项已经打开
	            	console.log($(this).attr("data-frameid"));
	            	if ($(this).attr("data-frameid") == dataid.attr("data-id")) {
	            		console.log($(this).parent().html());
	            		$(this).parent().addClass("layui-show");
	            	}
	            });*/
	            if (isData == false) {
	                //标志为false 新增一个tab项
	                active.tabAdd(dataid.attr("data-url"), dataid.attr("data-id"), dataid.attr("data-title"));
	            }
	        }
	        //最后不管是否新增tab，最后都转到要打开的选项页面上
	        active.tabChange(dataid.attr("data-id"), dataid.attr("data-title"));
	        //重写监听事件
	        $('.layui-tab-close').unbind();
	        $('.layui-tab-close').on('click', function(){
	        	var id = $(this).parent().attr("lay-id");
	        	element.tabDelete("domain", id);//删除tab
	        	//删除iframe
	        	$('#LAY_app_body .layadmin-tabsbody-item').each(function(index){
	        		if($(this).children("iframe").attr("data-frameid") == id){
	        			$(this).remove();
	        			return false;
	        		}
	        	});
	        });
	    });
	}
	
    /**
     * tab页签操作函数
     */
    var active = {
        //在这里给active绑定几项事件，后面可通过active调用这些事件
        tabAdd: function (url, id, name) {
            //新增一个Tab项 传入三个参数，分别对应其标题，tab页面的地址，还有一个规定的id，是标签中data-id的属性值
            //关于tabAdd的方法所传入的参数可看layui的开发文档中基础方法部分
        	tabsPage.index = $("#LAY_app_tabsheader>li").length;
//        	console.log("index: " + tabsPage.index);
            element.tabAdd('domain', {
                title: name,
                id: id //规定好的id
            });
            $("#LAY_app_body").append(appendIframe(id, url));
            FrameWH();  
        },
        tabChange: function (id, title) {
            //切换到指定Tab项
        	$.each($(".layui-tab-title li[lay-id]"), function (index) {
                //如果点击左侧菜单栏所传入的id 在右侧tab项中的lay-id属性可以找到，则说明该tab项已经打开
                if ($(this).attr("lay-id") == id) {
                	tabsPage.index = index;
                	return false;
                }
            });
            element.tabChange('domain', id); //根据传入的id传入到指定的tab项
            admin.tabsBodyChange(tabsPage.index, {
	            url: id,
	            text: title
            });
            
        },
        tabDelete: function (id) {
            element.tabDelete("domain", id);//删除
        }
    };
    
    /**
     * iframe标签
     */
    function appendIframe(id, url) {
    	var iframe = ['<div class="layadmin-tabsbody-item layui-show">'
    		, '<iframe data-frameid="' + id + '" scrolling="auto"  frameborder="0" src="' + url + '" class="layadmin-iframe"></iframe>'
    		, "</div>"];
    	return iframe.join("");
    }
    
    /**
     *计算ifram层的大小
     */
    function FrameWH() {
        var h = $(window).height();
        $("iframe").css("height",h+"px");
    }

	/**
	 * 配置Menu
	 * 
	 * @param {Object}
	 *            options
	 */
	MenuBar.prototype.set = function(options) {
		var that = this;
		$.extend(true, that.config, options);
		return that;
	};
	
	var menuBar = new MenuBar();
	
	exports('menuBar', function(options) {
		return menuBar.set(options);
	});
	
	//点击选项卡时刷新
	element.on('tab(domain)', function(data){
		$('#LAY_app_body').children().each(function(index){
			$(this).removeClass("layui-show");
		});
		$('#LAY_app_body').children().eq(data.index).addClass("layui-show");
	});
});