var $, layer, flow;
layui.use(['jquery', 'layer', 'flow'], function(){
	$ = layui.jquery
	,layer = layui.layer
	,flow = layui.flow;
	
	if($('body').width() < 960){
		tabPageSize = 4;
		scrollElem = '#activityList';
		$('.class').css("min-height", document.documentElement.clientHeight - $('.footer').height() - 30 + "px");
		$('#activityList').height($('.class').height() - $('#actCategory').height() - 30);
	}
	
	//获取活动类型
	$.ajax({ 
		url: "/ruleVerify/getActivityCategory", 
		data: {},
		dataType: 'json',
		success: function(result){
			if(result.code == "000000"){
				var data = result.data;
				if(data != null){
					category = data;
					for(var i = 0;i < category.length;i++){
						activityImg["log" + category[i].id] = category[i].activityImg;
					}
					getNewAct();
				}
			}
		}
	});
	
	//后退按钮监听
	$('.back').on('click', function(){
		$('#actBox1').show();
		$('#actBox2').hide();
	});
});

var category = [];
var activityImg = [];
var tabPageSize = 6;
var tabPageNo = 1;

//显示活动类型列表
function showCategory(){
	if(category != null){
		var page = tabPageSize * (tabPageNo - 1);
		if(page >= category.length){
			return ;
		}
		var content = '<tr>';
		content += '<th class="border_right tabPage" onclick="tabLeft()">';
		content += "<<";
		content += '</th>';
		for(var i = page;i < (page + tabPageSize);i++){
			if(i < category.length){
				content += '<th class="border_right" id="tab_' + category[i].id + '" onclick="getActivity(' + category[i].id + ')">' 
				content += category[i].actName;
				content += '</th>';
			}

		}
		content += '<th class="tabPage" onclick="tabRight()">';
		content += ">>";
		content += '</th>';
		content += '</tr>';
		$('#actCategory').html(content);
		
	}
}

//活动列表上一页
function tabLeft(){
	if(tabPageNo > 1){
		tabPageNo--;
		showCategory();
	}
}

//活动列表下一页
function tabRight(){
	var page = tabPageSize * tabPageNo;
	if(page < category.length){
		tabPageNo++;
		showCategory();
	}
}

var pageNo = 1;
var pageSize = 10;
var categoryId = 0;
var flag = true;
var scrollElem = 'document';
//获取活动
function getActivity(actId){
	$('.border_right').removeClass("selected");
	$('#tab_' + actId).addClass("selected");
	if(categoryId != actId){
		categoryId = actId;
		$('#activityList').html("");
	}	
	var url = '/ruleVerify/getActivityList?actId=' + actId + '&pageSize=' + pageSize;
	flow.load({
		elem: '#activityList' //指定列表容器
		,scrollElem: scrollElem
	    ,done: function(page, next){ //到达临界点（默认滚动触发），触发下一页
	    	
	    	var lis = [];
	    	//以jQuery的Ajax请求为例，请求下一页数据（注意：page是从1开始返回）
	    	$.get(url + '&pageNo=' + page, function(res){
				//假设你的列表返回在data集合中
				layui.each(res.data, function(index, item){
					var content = '';
					content += '<a href="/ruleVerify/toActivityPage?actId=' + item.id + '">';
					content += '<div class="col-md-4 class-left animated wow fadeInLeft animated" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-name: fadeInLeft;">';
					content += '<figure>';
					content += '<img src="' + activityImg["log" + actId] + '" alt="" class="img_responsive">';
					content += '</figure>';
					content += '<h6>' + item.beginTime.substring(0, 10) + ' - ' + item.endTime.substring(0, 10) + '</h6>';
					content += '<div class="c-btm">';
					content += '<h4>' + item.actName + '</h4>';
					content += '</div>';
					content += '</div>';
					content += '</a>';
					lis.push(content);
				}); 
				
				//执行下一页渲染，第二参数为：满足“加载更多”的条件，即后面仍有分页
				//pages为Ajax返回的总页数，只有当前页小于总页数的情况下，才会继续出现加载更多
				/*if(page == 1){
					$('#activityList').html("");
				}*/
				next(lis.join(''), pageSize <= res.data.length);    
	    	});
	    }
	});
}

//获取最新活动
function getNewAct(){
	$.ajax({
		url: '/ruleVerify/getNewActivity',
		type: 'POST',
		dataType: 'json',
		success: function(result){
			if(result.code == '000000'){
				var data = result.data;
				if(data != null){
					var content = '';
					console.log(data);
					for(var i = 0;i < data.length;i++){
						var item = data[i];
						content += '<a onclick="toActivity(\'/ruleVerify/toActivityPage?actId=' + item.id + '\')" >';
						content += '<div class="col-md-4 class-left animated wow fadeInLeft animated" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-name: fadeInLeft;">';
						content += '<figure>';
						content += '<img src="' + activityImg["log" + item.actId] + '" alt="" class="img_responsive">';
						content += '</figure>';
//						content += '<h6>' + item.beginTime.substring(0, 10) + ' - ' + item.endTime.substring(0, 10) + '</h6>';
						content += '<div class="c-btm">';
						content += '<h4>' + item.actName + '</h4>';
						content += '</div>';
						content += '<div class="checkBtm" onclick="checkAct(' + item.actId + ', event)">';
						content += '<span>查看更多</span>';
						content += '</div>';
						content += '</div>';
						content += '</a>';
					}
					$('#activityList').html(content);
				}
			}else{
				layer.msg("获取最新活动失败");
			}
		}
	});
}

//跳转到活动页
function toActivity(url){
	window.location.href = url;
}

//查看更多活动
function checkAct(actId, event){
	$('#actBox1').hide();
	$('#actBox2').show();
	$('#actList').html('');
	var url = '/ruleVerify/getActivityList?actId=' + actId + '&pageSize=' + pageSize;
	flow.load({
		elem: '#actList' //指定列表容器
		,scrollElem: scrollElem
	    ,done: function(page, next){ //到达临界点（默认滚动触发），触发下一页
	    	
	    	var lis = [];
	    	//以jQuery的Ajax请求为例，请求下一页数据（注意：page是从1开始返回）
	    	$.get(url + '&pageNo=' + page, function(res){
				//假设你的列表返回在data集合中
				layui.each(res.data, function(index, item){
					var content = '';
					content += '<a onclick="toActivity(\'/ruleVerify/toActivityPage?actId=' + item.id + '\')" >';
					content += '<div class="col-md-4 class-left animated wow fadeInLeft animated" data-wow-duration="1000ms" style="visibility: visible; animation-duration: 1000ms; animation-name: fadeInLeft;">';
					content += '<figure>';
					content += '<img src="' + activityImg["log" + actId] + '" alt="" class="img_responsive">';
					content += '</figure>';
//					content += '<h6>' + item.beginTime.substring(0, 10) + ' - ' + item.endTime.substring(0, 10) + '</h6>';
					content += '<div class="c-btm">';
					content += '<h4>' + item.actName + '</h4>';
					content += '</div>';
					content += '</div>';
					content += '</a>';
					lis.push(content);
				}); 
				
				//执行下一页渲染，第二参数为：满足“加载更多”的条件，即后面仍有分页
				//pages为Ajax返回的总页数，只有当前页小于总页数的情况下，才会继续出现加载更多
				/*if(page == 1){
					$('#activityList').html("");
				}*/
				next(lis.join(''), pageSize <= res.data.length);    
	    	});
	    }
	});
	event.stopPropagation();
}

var img_show;
//图片预览
function showImg(rewardImg, id){
	var img = new Image();
	img.src = rewardImg;
	var width = img.width;
	if(width > document.documentElement.clientWidth * 0.3){
		width = document.documentElement.clientWidth * 0.3
	}
	var img = "<img class='img_msg' src='"+ rewardImg +"' style='max-width: " + width + "px' />";
    img_show = layer.tips(img, '.actImg_' + id,{
        tips:2
        ,area: [width + 30 + 'px']
    });
}

//隐藏图片
function hiddenImg(){
	layer.close(img_show);
}
