layui.use(['jquery', 'layer', 'element', 'flow'], function(){
	var $ = layui.jquery
	,layer = layui.layer
	,element = layui.element
	,flow = layui.flow;
	
//	alert(document.documentElement.clientWidth + ", " + document.documentElement.clientHeight);
	
	$('.bg').height(document.documentElement.clientHeight);
	
	//获取站点域名配置
	$('.menu').html(getDomainKv('sign'));
	
	//活动规格按钮监听
	$('.ruleBtn').on('click', function(){
		$('#shade').show();
		$('.ruleBox').show();
	});
	
	//登录按钮监听
	$('#signLogin').on('click', function(){
		signLogin();
	});
	
	//我的奖品按钮监听
	$('#myReward').on('click', function(){
		if($('#memberId').val() == null || $('#memberId').val() == ''){
			$('#shade').show();
			$('.login').show();
		}else{
			$('#shade').show();
			$('.recordBox').show();
			var content = getMyRewardByPage();
			$('#record').html(content);
		}
	});
	
	var rewards = [];
	//查看奖品监听
	$('.checkReward').on('click', function(){
		page = 0;
		if(rewards == null || rewards.length == 0){
			getSignRule();
		}
		$('#shade').show();
		$('.rewardBox').show();
	});
	
	//补签按钮监听
	$(".supplementSignBtn").on('click', function(){
		if($('#memberId').val() == null || $('#memberId').val() == ''){
			$('#shade').show();
			$('.login').show();
		}else{
			getAllowSignDay();
			$('#shade').show();
			$('.supplementSignBox').show();
		}
	});
	
	//点击关闭弹窗按钮隐藏弹窗
	var closeValue = ['.login', '.gitReward', '.ruleBox', '.rewardBox', '.recordBox', '.supplementSignBox', '#shade'];
	closePopup(closeValue);
	$('.closeBtn').on('click', function(){
		$('.gitReward').hide();
		$('#shade').hide();
		if($('#deputyActId').val() != null && $('#deputyActId').val() != "" && $('#lotteryTimes').text() != null && $('#lotteryTimes').text() > 0){
			window.location.href = "/ruleVerify/toActivityPage?actId=" + $('#deputyActId').val()
									+ "&mainActId=" + $('#actId').val();
		}
	});
	
	//抽奖按钮监听
	$('#toActivity').on("click", function(){
		if($('#deputyActId').val() != null && $('#deputyActId').val() != ""){
			window.location.href = "/ruleVerify/toActivityPage?actId=" + $('#deputyActId').val()
									+ "&mainActId=" + $('#actId').val();
		}
	});
	
	//签到监听
	$('#signBtn').on('click', function(){
		//判断是否登录
		if($('#memberId').val() == null || $('#memberId').val() == ''){
			$('#shade').show();
			$('.login').show();
		}else{
			//签到
			signRuleVerify();
		}
	});
	
	//立即补签按钮监听
	$('#supplementBtn').on('click', function(){
		supplementSignApply();
	});
	
	var myDate = new Date();
	var year = myDate.getFullYear();    //获取完整的年份(4位,1970-????)
	var month = myDate.getMonth();       //获取当前月份(0-11,0代表1月)
	var day = myDate.getDate();
	var myDate=new Date();
	myDate.setFullYear(year, month, 1);
	var week = myDate.getDay();
	var daysCount = getCountDays();
	//签到记录
	var signRecords = [];
	//连续签到天数
	var seriesDay = 0;
	//可补签天数
	var allowSign = 0;
	var allowSignDay = [];
	//获取签到纪录
	signRecord();
	function signRecord(){
		$('.month').text((month + 1) + "");
		if($("#memberId").val() == null || $("#memberId").val() == ""){
			$('#shade').show();
			$('.login').show();
		}
		//计算签到天数
		supplementDay();
		
		var content = '';
		var dayNum = 1;
		//显示日历
		$("#calendar").find("tbody").html("");
		for(var i = 0;i < daysCount;i++){
			if((i + week) % 7 == 0 || i + 1 == daysCount){
				if(content != ""){
					content += '</tr>';
					$("#calendar").find("tbody").append(content);
				}
				content = '<tr>';
			}
			if(content == ''){
				content = '<tr>';
			}
			if(i == 0 && week > 0){
				for(var j = 0;j < week;j++){
					content += '<td></td>';
				}
			}
			content += '<td id="day_' + dayNum + '">';
			if(signRecords != null && signRecords.length > 0 && i + 1 <= day){
				var isSign = 0;
				for(var j = 0;j < signRecords.length;j++){
					var day1 = i + 1;
					var date = new Date(Date.parse(createTime(signRecords[j].signTime).replace(/-/g,"/")));
					var day2 = date.getDate();
					if(day1 == day2){
						if(signRecords[j].signType == 0){
							//已签到
							content += '<img src="/activityPage/sign/image/sign4.png">';
						}else{
							//补签到
							content += '<img src="/activityPage/sign/image/sign3.png">';
						}
						isSign = 1;
						break;
					}
				}
				if(isSign == 0){
					if(i + 1 >= day){
						//未签到
						content += '<img src="/activityPage/sign/image/sign1.png">';
					}else{
						//没签到
						content += '<img src="/activityPage/sign/image/sign2.png">';
					}
				}
			}else{
				/*if(i + 1 >= day){
					content += '<img src="/activityPage/sign/image/sign1.png">';
				}else{
					content += '<img src="/activityPage/sign/image/sign2.png">';
				}*/
				content += '<img src="/activityPage/sign/image/sign1.png">';
			}
			content += '<br><span>' + dayNum + '</span>'
			content += '</td>';
			dayNum++;
		}
	}
	
	//获取当月天数
	function getCountDays() {
		var curDate = new Date();
        /* 获取当前月份 */
          var curMonth = curDate.getMonth();
        /*  生成实际的月份: 由于curMonth会比实际月份小1, 故需加1 */
        curDate.setMonth(curMonth + 1);
        /* 将日期设置为0, 这里为什么要这样设置, 我不知道原因, 这是从网上学来的 */
        curDate.setDate(0);
        /* 返回当月的天数 */
        return curDate.getDate();
	}
	
	//登录
	function signLogin(){
		$.ajax({ 
			url: "/ruleVerify/memberLogin", 
			data: {actId: $('#actId').val(), memberName: $('#memberName').val()},
			dataType: 'json',
			success: function(result){
				if(result.code == "000000"){
					$('#memberId').val(result.data.id);
					getMemberAct();
					signRecord();
					$('.close').click();
					//签到
					signRuleVerify();
				}else{
					layer.msg(result.msg);
				}
			}
		});
	}

	//签到
	if($('#memberId').val() != null && $('#memberId').val() != ''){
		signRuleVerify();
	}
	function signRuleVerify(){
		$.ajax({ 
			url: "/ruleVerify/signRuleVerify", 
			data: {actId: $('#actId').val(), centralActId: $('#mainActId').val()},
			dataType: 'json',
			success: function(result){
				if(result.code == '000000'){
					if(result.data != null && result.data != ''){
						var reward = result.data;
						var content = '';
						if(reward.actId != null && reward.actId != "" ){
							if(reward.lotteryTimes != null && reward.lotteryTimes > 0){
								content += '<div class="joinTimes">';
								content += '<img src="/activityPage/sign/image/gift3.png">';
								content += '<div>获得' + reward.lotteryTimes + '次抽奖</div>';
								content += '</div>';
								$('.closeBtn').text("去抽奖");
							}
							$('#deputyActId').val(reward.actId);
							$('#lotteryTimes').text(reward.lotteryTimes);
						}
						if(reward.reward != null && reward.reward != ''){
							content += '<div class="reward">';
							if(reward.rewardTimes != null && reward.rewardTimes != ''){
								content += '<div class="rewardTimes" id="rewardTimes">X' + reward.rewardTimes + '</div>';
							}
							content += '<img src="/activityPage/sign/image/gift1.png">';
							content += '<div class="rewardName" id="rewardName">' + reward.reward + '</div>';
							content += '</div>';
						}
						if(content == ''){
							$('#rewardTitle').text("谢谢参与");
						}
						$('#reward').html(content);
					}
					$('#shade').show();
					$('.gitReward').show();
					signRecord();
				}else{
					if(result.data != null){
						$('#deputyActId').val(result.data.actId);
						$('#lotteryTimes').text(result.data.lotteryTimes);
					}
					layer.msg(result.msg);
				}
			}
		});
	}
	
    //获取该签到活动规则
    function getSignRule(){
    	$.ajax({ 
			url: "/ruleVerify/getSignRule", 
			data: {actId: $('#actId').val()},
			dataType: 'json',
			async: false,
			success: function(result){
				if(result.code == '000000'){
					if(result.data != null){
						var content = '';
						var data = result.data;
						for(var i = 0;i < data.length;i++){
							content += '<div class="layui-colla-item">';
							if(data[i].signType == 0){
								content += '<h2 class="layui-colla-title colla-title">每日签到</h2>';
							}else if(data[i].signType == 1){
								content += '<h2 class="layui-colla-title colla-title">连续' + data[i].statisticsTimes + '天</h2>';
							}else if(data[i].signType == 2){
								content += '<h2 class="layui-colla-title colla-title">累计' + data[i].statisticsTimes + '天</h2>';
							}
							
							content += '<div class="layui-colla-content colla-content">';
							if(data[i].deputyActId != null && data[i].deputyActId != '' && data[i].lotteryTimes != null && data[i].lotteryTimes > 0){
								content += '<div class="rewardInfo">';
								content += '<img class="rewardImg" src="/activityPage/sign/image/gift2.png">';
								content += '<span class="text">抽奖' + data[i].lotteryTimes + '次</span>';
								content += '</div>';
							}
							if(data[i].rewards != null && data[i].rewards.length > 0){
								for(var j = 0;j < data[i].rewards.length;j++){
									content += '<div class="rewardInfo">';
									if(data[i].rewards[j].rewardImg != null && data[i].rewards[j].rewardImg != ''){
										content += '<img class="rewardImg" src="' + data[i].rewards[j].rewardImg + '">';
									}else{
										content += '<img class="rewardImg" src="/activityPage/sign/image/gift1.png">';
									}
									content += '<span class="text">' + data[i].rewards[j].rewardOption + '</span><br>';
//									content += '<span class="text">' + data[i].rewards[j].rewardCon + '</span>';
									content += '</div>';
								}
							}
							content += '</div>';
							content += '</div>';
						}
						$('.collapse').html(content);
						element.render('collapse');
						
						//添加监听
						element.on('collapse(rewardList)', function(data){
						    if(data.show){
						    	
						    }
						});
					}
				}
			}
    	});
    }
    
    //获取我的中奖纪录
    var pageSize = 20;
    function getMyRewardByPage(){
    	var url = '/ruleVerify/getRewardRecord?actId=' + $('#actId').val()
    				+ '&memberId=' + $('#memberId').val()
    				+ '&centralActId=' + $('#mainActId').val()
    				+ '&pageSize=' + pageSize;
    	flow.load({
    		elem: '#record' //指定列表容器
    		,scrollElem: '#record'
		    ,done: function(page, next){ //到达临界点（默认滚动触发），触发下一页
		      var lis = [];
		      //以jQuery的Ajax请求为例，请求下一页数据（注意：page是从1开始返回）
		      $.get(url + '&pageNo='+page, function(res){
		        //假设你的列表返回在data集合中
		        layui.each(res.data, function(index, item){
		        	var content = '';
		        	content += '<div class="row">';
					content += '<div class="time">' + createTime(item.createTime) + '</div>';
					if(item.isGit != 1){
						content += '<div class="reward">谢谢参与</div>';
					}else{
						content += '<div class="reward">' + item.rewardCon + '</div>';
					}
					if(item.isSend == 1){
						content += '<div class="isSend">已派送</div>';
					}else{
						content += '<div class="isSend">未派送</div>';
					}
					content += '</div>';
					lis.push(content);
		        }); 
		        
		        //执行下一页渲染，第二参数为：满足“加载更多”的条件，即后面仍有分页
		        //pages为Ajax返回的总页数，只有当前页小于总页数的情况下，才会继续出现加载更多
		        next(lis.join(''), pageSize <= res.data.length);    
		      });
		    }
    	});
    }
	
    function supplementDay(){
    	$.ajax({ 
			url: "/ruleVerify/getSignRecord", 
			data: {actId: $('#actId').val()},
			dataType: 'json',
			async: false,
			success: function(result){
				if(result.code == "000000"){
					signRecords = result.data.signRecord;
					$('#countSign').text(result.data.countSign);
					if(signRecords != null && signRecords.length > 0){
						//计算连续签到
						var newDay = 0;
						for(var i = signRecords.length - 1;i >= 0;i--){
							var date = new Date(Date.parse(createTime(signRecords[i].signTime).replace(/-/g,"/")));
							if(newDay == 0){
								newDay = date.getDate();
								if(newDay == day){
									seriesDay = 1;
								}
							}else{
								if(newDay - 1 == date.getDate()){
									seriesDay++;
									newDay--;
								}else{
									break;
								}
							}
						}
						//计算可补签天数
						var j = 0;
						allowSignDay = [];
						for(var i = 1;i < day;i++){
							if(j < signRecords.length){
//								alert(signRecords[j].signTime.replace(/-/g,"/"));
								var date = new Date(Date.parse(createTime(signRecords[j].signTime).replace(/-/g,"/")));
								var day1 = date.getDate();
								if(i == day1){
									j++;
								}else{
									allowSignDay.push(i);
									allowSign++;
								}
							}
						}
					}
				}
				$('#seriesDay').text(seriesDay);
				$('#allowSign').text(allowSign);
			}
		});
    }
    
    //显示可补签日期
    var applys = [];
    function getAllowSignDay(){
    	//获取补签申请
    	getApply();
    	
    	//重新获取签到天数
    	signRecord();
    	
    	if(allowSignDay != null && allowSignDay.length > 0){
    		var content = '';
    		var isApply = 0;
    		for(var i = 0;i < allowSignDay.length;i++){
    			isApply = 0;
    			content += '<div class="dayBox">';
    			for(var j = 0;j < applys.length;j++){
    				if(allowSignDay[i] == applys[j]){
    					isApply = 1
    					break;
    				}
    			}
    			if(isApply == 0){
    				content += '<input id="day' + allowSignDay[i] + '" class="day" type="checkbox" value="' + allowSignDay[i] + '" />';
        			content += '<span id="forDay_' + allowSignDay[i] + '" class="forDay">' + allowSignDay[i] + '</span>';
//    				content += '<label id="dayLabel_' + allowSignDay[i] + '" for="day' + allowSignDay[i] + '">' + allowSignDay[i] + '</label>';
    			}else{
    				content += '<span class="apply">' + allowSignDay[i] + '</span>';
    			}
    			content += '</div>';
    		}
    		$('#allowSignDay').html(content);
    		//补签日期选择监听
    		$(".forDay").unbind();
    		$('.forDay').on("click", function(){
    			var id = $(this).attr("id").substring(7);
    			if(!$("#day" + id).prop("checked")){
    				$('#day' + id).prop("checked", true);
    				$('#forDay_' + id).css("background-color", "#fcbf41");
    				$('#forDay_' + id).css("color", "#fff");
    			}else{
    				$('#day' + id).prop("checked", false);
    				$('#forDay_' + id).css("background-color", "#e9e9e9");
    				$('#forDay_' + id).css("color", "#000");
    			}
    		});
    		//已提交补签申请按钮监听
    		$(".apply").unbind();
    		$('.apply').on('click', function(){
    			layer.msg("已经发起补签申请");
    		});
    	}
    }
    
    //获取已有的补签申请
    function getApply(){
    	$.ajax({ 
			url: "/ruleVerify/getSupplementSignApply", 
			data: {actId: $('#actId').val()},
			dataType: 'json',
			async: false,
			success: function(result){
				if(result.code == '000000'){
					if(result.data != null){
						var data = result.data;
						applys = [];
						for(var i = 0;i < data.length;i++){
							applys.push(parseInt(data[i].signTime.substring(8, 10).trim()));
						}
					}
				}
			}
    	});
    }
    
    //提交补签申请
    function supplementSignApply(){
    	var dates = '';
    	$(".day").each(function(){
    		if($(this).prop("checked")){
    			if(dates == ''){
    				dates += $(this).val();
    			}else{
    				dates += "," + $(this).val();
    			}
    		}
    	});
    	if(dates != ''){
    		$.ajax({ 
    			url: "/ruleVerify/supplementSignApply", 
    			data: {actId: $('#actId').val(), dates: dates},
    			dataType: 'json',
    			async: false,
    			success: function(result){
    				if(result.code == '000000'){
    					applys = [];
    					layer.msg("您选择的日期已提交补签申请");
    					getAllowSignDay();
    				}else{
    					layer.msg(result.msg);
    				}
    			}
        	});
    	}else{
    		layer.msg("请选择补签日期");
    	}
    }
    
});

