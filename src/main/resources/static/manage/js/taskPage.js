
var devices = [];
layui.use(['layer','jquery','form'], function(){
	var layer = layui.layer //弹层
	,$ = layui.jquery
	,form = layui.form;
	
	$.ajax({ 
		url: "/device/getAll", 
		data: {},
		dataType: 'json',
		success: function(result){
			if(result.code != "000000"){
				layer.msg("设备获取失败");
			}else{
				devices = result.data;
				if(devices != null){
					var html = '';
					for(var i = 0;i < devices.length;i++){
						html += '<input type="checkbox" name="device" value="' + devices[i].id + '" lay-skin="primary" title="' + devices[i].mark + '">';
					}
					$('#devices').html(html);
					form.render();
				}
			}
		}
	});
});